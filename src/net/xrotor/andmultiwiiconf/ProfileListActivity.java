package net.xrotor.andmultiwiiconf;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * This Activity appears as a dialog. It lists any found profiles
 * stored in the default profile folder.
 */
public class ProfileListActivity extends Activity {
    // Debugging
    private static final String TAG = "AndMultiWiiConf.ProfileListActivity";
    private static final boolean D = false;

    // Return Intent extra
    public static String SELECTED_PROFILE_NAME = "selected_profile_name";
    public static String SELECTED_PROFILE_VERSION = "selected_profile_version";
    public static String SAVE_PROFILE_NAME = "save_profile_name";
    
    // Incoming Intent extra
    public static final int PROFILE_ACTION_LOAD = 0;
	public static final int PROFILE_ACTION_SAVE = 1;
	public static final int PROFILE_FILENAME = 2;
	
    // Member fields
    private ArrayAdapter<String> mProfilesArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		if (D) Log.d(TAG, "--- ON CREATE --- ");
    	
		int intentRequestType = getIntent().getExtras().getInt("ACTION",-1);
		
		// Determine the type of Profile Action requested
		switch (intentRequestType) {
		
		// LOAD Profile
		case ProfileListActivity.PROFILE_ACTION_LOAD:
	        
			// Setup the window
	        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	        setContentView(R.layout.profile_load);

	        // Set result CANCELED in case the user backs out
	        setResult(Activity.RESULT_CANCELED);

	        // Initialize the button to perform close of windows
	        Button closeButton = (Button) findViewById(R.id.button_close);
	        closeButton.setOnClickListener(new OnClickListener() {
	            @Override
                public void onClick(View v) {
	               finish();
	            }
	        });

	        // Initialize array list array adapter
	        mProfilesArrayList = new ArrayAdapter<String>(this, R.layout.profile_name);

	        // Find and set up the ListView for found profiles
	        ListView profilesListView = (ListView) findViewById(R.id.found_profiles);
	        profilesListView.setAdapter(mProfilesArrayList);
	        profilesListView.setOnItemClickListener(mProfileClickListener);
	        profilesListView.setOnItemLongClickListener(mProfileLongClickListener);

	        // Scan profiles folder to check for files
	        ArrayList <Profile> profilesFound = MwcProfiles.findProfiles();
	        
	        // If any profile files were found, add each one to the ArrayAdapter
	        if (profilesFound != null) {
	            
	        	findViewById(R.id.title_found_profiles).setVisibility(View.VISIBLE);
	            
	            for (Profile profile : profilesFound) {
	            	mProfilesArrayList.add(profile.getName() + "\n" + "Ver: " 
	            			+ ((float) profile.getVersion() / 10 )); // Nasty hack to show the proper version format
	            }
	        
	        // no profile was found load the no-profile text    
	        } else {
	            String noProfiles = getResources().getText(R.string.no_profile_found).toString();
	            mProfilesArrayList.add(noProfiles);
	        }
			
			break;
			
		// SAVE Profile
		case ProfileListActivity.PROFILE_ACTION_SAVE:

			// Setup the window
	        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	        setContentView(R.layout.profile_save);

	        // Set result CANCELED in case the user backs out
	        setResult(Activity.RESULT_CANCELED);
			
	        // Initialize the button to perform close of windows
	        Button saveProfileButton = (Button) findViewById(R.id.button_profile_save);
	        saveProfileButton.setOnClickListener(new OnClickListener() {
	            @Override
                public void onClick(View v) {
	            	// Get the name for the new profile
	                EditText editTextSaveProfileName = (EditText) findViewById(R.id.save_profile_name);
	                
	                // Create the result Intent and include the MAC address
	                Intent intent = new Intent();
	                intent.putExtra(SAVE_PROFILE_NAME, editTextSaveProfileName.getText().toString());

	                // Set result and finish this Activity
	                setResult(AndMultiWiiConf.RESULT_SAVE_PROFILE, intent);
	                finish();
	            }
	        });
	        
			break;
		}
			
    }

    // The on-click listener for all profile entries in the ListViews
    private OnItemClickListener mProfileClickListener = new OnItemClickListener() {
    	@Override
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
        	
        	// Get the profile name, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String profileName = info.split("\\n")[0];
            
            // Make sure we're not picking the NO PROFILE
            if (profileName.equals(getResources().getText(R.string.no_profile_found).toString())) {
                return;
            }
            // Create the result Intent and include the MAC address
            Intent intent = new Intent();
            // Pass full file path to the Intent extras
            intent.putExtra(SELECTED_PROFILE_NAME, Environment.getExternalStorageDirectory() 
            		+ MwcConstants.PROFILES_PATH + profileName);

            // Set result and finish this Activity
            setResult(AndMultiWiiConf.RESULT_LOAD_PROFILE, intent);
            finish();
        }
    };
    
    // The long-on-click listener for all profiles entries in the ListViews
    private OnItemLongClickListener mProfileLongClickListener = new OnItemLongClickListener() {
		@Override
        public boolean onItemLongClick(AdapterView<?> av, View v,
				int arg2, long arg3) {
			
			// Get the profile name
            String info = ((TextView) v).getText().toString();
            String profileName = info.split("\\n")[0];
            
            // Make sure we're not picking the NO PROFILE
            if (profileName.equals(getResources().getText(R.string.no_profile_found).toString())) {
                return false;
            }
            //String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            
            // Call the SEND_FILE intent for the select item file
            Intent i = new Intent(Intent.ACTION_SEND); 
            i.setType("application/mwc"); 
            //i.putExtra(Intent.EXTRA_STREAM, Uri.parse(extStorageDirectory + MwcConstants.PROFILES_PATH
			//		+ profileName));
            i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Environment.getExternalStorageDirectory() 
            		+ MwcConstants.PROFILES_PATH, profileName)));
            startActivity(Intent.createChooser(i, "Share MWC Profile"));
			return false;
		}
    };

}