package net.xrotor.andmultiwiiconf;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataLogger {

    private static final String TAG = "AndMultiWiiConf.DataLogger";
    private static final boolean D = false;

    private String fileName;
    private BufferedWriter logBuffer;

    // Class constructor
    public DataLogger(String filename){
	fileName = filename;
    }

    // Start function
    public void startLogger(){

	try {
	    File root = Environment.getExternalStorageDirectory();

	    if (root.canWrite()){

		File logfile = new File(root, fileName);
		FileWriter logwriter = new FileWriter(logfile);

		logBuffer = new BufferedWriter(logwriter);
		if (D) Log.d(TAG, "Logging data");
	    }

	} catch (IOException e) {
	    Log.e(TAG, "Could not write file " + e.getMessage());
	}

    }

    // Stop function
    public void stop(){

	try {

	    logBuffer.close();

	} catch (IOException e) {
	    Log.e(TAG, "Could not write file " + e.getMessage());
	}
    }
    public void writeData(String data){
	try {

	    logBuffer.write(data);

	} catch (IOException e) {
	    Log.e(TAG, "Could not write file " + e.getMessage());
	}
    }

}
