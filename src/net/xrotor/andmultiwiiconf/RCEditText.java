package net.xrotor.andmultiwiiconf;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import java.text.DecimalFormat;

public class RCEditText extends EditText {
    //public class RCEditText extends TextView implements OnLongClickListener{

    private static final String TAG = "AndMultiWiiConf.RCEditText";
    private static final boolean D = false;

    public static final int PRECISION_NONE   = 0;
    public static final int PRECISION_SINGLE = 1;
    public static final int PRECISION_DOUBLE = 2;
    public static final int PRECISION_TRIPLE = 3;

    private int sFormatType;
    private int minValue;
    private int maxValue;

    //private static boolean mFormatting;

    public RCEditText(Context context) {
        super(context);
        //this.addTextChangedListener(this);
    }

    public RCEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Grab precision format from the layout attribute or default to none
        sFormatType = attrs.getAttributeIntValue(
                "http://schemas.android.com/apk/res/net.xrotor.andmultiwiiconf",
                "decimals",
                0);

        // Get the minimum values from the layout attribute or defaults to 0
        minValue = attrs.getAttributeIntValue(
                "http://schemas.android.com/apk/res/net.xrotor.andmultiwiiconf",
                "minValue",
                0);

        // Get the maximum value from the layout attribute or defaults to 100
        maxValue = attrs.getAttributeIntValue(
                "http://schemas.android.com/apk/res/net.xrotor.andmultiwiiconf",
                "maxValue",
                100);

    }

    public RCEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        //this.addTextChangedListener(this);
    }
/*
    @Override
    protected void onFocusChanged(boolean focused, int direction,
            Rect previouslyFocusedRect) {

        //super.onFocusChanged(focused, direction, previouslyFocusedRect);
        //if(this.getText().equals("")) {
        //	this.setText(this.getResources().getText(id));
        //}

        // Did we loosed focus???
        if(!hasFocus()){
            String userInput = this.getText().toString();




            int dotPos = -1;

            for (int i = 0; i < userInput.length(); i++){
                char c = userInput.charAt(i);
                if(c == '.'){
                    dotPos = i;
                }
            }

            if (dotPos == -1) {
                this.setText(userInput + ".00");
            } else {
                if (userInput.length() - dotPos == 1) {
                    this.setText(userInput + "00");
                } else if (userInput.length() - dotPos == 2) {
                    this.setText(userInput + "0");
                }
            }

            //textFormat();

        }

        super.onFocusChanged(focused, direction, previouslyFocusedRect);
    }
*/

    @Override
    public boolean onCheckIsTextEditor() {
        AndMultiWiiConf mainActivity = (AndMultiWiiConf) getContext();
        return !mainActivity.getUseSliderState();
    }

    /*
	public synchronized void afterTextChanged(Editable text) {
		if(D) { Log.d(TAG, "New char " + text + " triggered afterTextChanged...."); };

        // Make sure to ignore calls to afterTextChanged caused by the work done below
        if (!mFormatting) {
            mFormatting = true;

            // If deleting the hyphen, also delete the char before or after that
            if (mDeletingDot && mDotStart > 0) {
                if (mDeletingBackward) {
                    if (mDotStart - 1 < text.length()) {
                        text.delete(mDotStart - 1, mDotStart);
                    }
                } else if (mDotStart < text.length()) {
                    text.delete(mDotStart, mDotStart + 1);
                }
            }

            //for (int i = 0; i < text.length(); i++){
            //	if(text.charAt(i) == '.' || text.charAt(i) == '-') {
            //		text.replace(i, i + 1, "", 0, 0); // remove dot, later formatNumber will place it again
            //	}
            //}

            if(D) { Log.d(TAG, "Sending char " + text + " to be formated...."); };
            // Format the new text unless it's empty
            if (text.length() > 0) {RCEditText.formatNumber(text, this.sFormatType); };

            //mFormatting = false;
        }
	}
     */
    /*
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		if(D) { Log.d(TAG, "I " + s + " triggered beforeTextChanged...."); };

        // Check if the user is deleting a hyphen
        //if (!mFormatting) {
            // Make sure user is deleting one char, without a selection
            final int selStart = Selection.getSelectionStart(s);
            final int selEnd = Selection.getSelectionEnd(s);
            if (s.length() > 1 // Can delete another character
                    && count == 1 // Deleting only one character
                    && after == 0 // Deleting
                    && s.charAt(start) == '.' // a hyphen
                    && selStart == selEnd) { // no selection
                mDeletingDot = true;
                mDotStart = start;
                // Check if the user is deleting forward or backward
                if (selStart == start + 1) {
                	if(D) { Log.d(TAG, "Deleting backwards"); };
                    mDeletingBackward = true;
                } else {
                	if(D) { Log.d(TAG, "Deleting forwards"); };
                    mDeletingBackward = false;
                }
            } else {
            	mDeletingDot = false;
            }

        //}

	}
     */
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(s.toString().matches(""))
        {
            // Workaround to fix localization settings bug: (http://code.google.com/p/android/issues/detail?id=2626)
            // this is very ugly and I know it. :(
            //Double cleanValue = Double.parseDouble( (Double.toString(value)).replace(',', '.'));


            //DecimalFormat decimalFormat = new DecimalFormat(getFormatString());
            //this.setText(decimalFormat.format(0));
            // Move cursor to last position on the right
            //this.setSelection(this.getText().length());
        }
    }

    /* Custom Methods */

    /*
	public void setRange(double min, double max) {
		minValue = min;
		// Make sure the max value is not smaller than the min
		//  if so use the min as max.
		if (max < min ) {
			maxValue = min;
		} else
			maxValue = max;
	}
     */

    /*
    private static void formatNumber(Editable text, int defaultFormattingType) {
        if(D) Log.d(TAG, "Formating text: " + text + " with precision: " + defaultFormattingType);

        switch(defaultFormattingType){
            case PRECISION_NONE:
                // Check if new char is a dot
                if(text.length() > 0 && text.charAt(text.length() - 1) == '.' ) {
                    text.delete(text.length(), text.length()); // if so remove it
                }
                break;
            case PRECISION_SINGLE:
                // Check if the dot is placed either on index 1 or 2 ( #.# or ##.# )
                if(text.length() >= 3 && (text.charAt(1) != '.' || text.charAt(2) != '.') ) {
                    //text.replace(2, 2, ".");
                    text.insert(2, ".");
                    text.insert(2, ".");
                    if(D) { Log.d(TAG, "Formating text the dot" ); };
                }
                break;
            case PRECISION_DOUBLE:
                if(text.length() >= 2 && text.charAt(1) != '.'){ // Check if the dot is placed
                    text.insert(1, ".");
                }
                break;
            case PRECISION_TRIPLE:
                if(text.length() >= 2 && text.charAt(1) != '.'){ // Check if the dot is placed
                    text.insert(1, ".");
                }
                break;
        }
        // Additional formating
        // If the initial char is a dot '.' add a 0 to the start
        if (text.charAt(0) == '.'){
            text.insert(0, "0");
        }


        if(D) { Log.d(TAG, "Text lenght " + text.length() + " precision "+ defaultFormattingType + " result: " + text); };
        // Delete excessive chars
        // check if the precision is not single to prevent truncate the text
        if (defaultFormattingType != PRECISION_SINGLE && text.length() > defaultFormattingType + 2){
            text.delete(defaultFormattingType + 2, text.length());
            // hack'ish workaround to allow PRECISION_SINGLE to have either #.# or ##.# format
        } else if (defaultFormattingType == PRECISION_SINGLE && text.length() > 4) {
            text.delete(4, text.length());
        }
        setmFormatting(false);
    }
    */
/*
    private void textFormat() {

        // Workaround to fix localization settings bug: (http://code.google.com/p/android/issues/detail?id=2626)
        // this is very ugly and I know it. :(
        //Double cleanValue = Double.parseDouble( (Double.toString(value)).replace(',', '.'));

        //String formattedValue = new DecimalFormat(formatPattern).format(cleanValue);
        //super.setText(formattedValue);
        DecimalFormat decimalFormat = new DecimalFormat(getFormatString());
        this.setText(decimalFormat.format(this.getText()));
    }
*/
    private String getFormatString() {
        String formatPattern = "";

        switch (sFormatType) {
            case PRECISION_NONE:
                formatPattern = "0";
                break;

            case PRECISION_SINGLE:
                formatPattern = "0.0";
                break;

            case PRECISION_DOUBLE:
                formatPattern = "0.00";
                break;

            case PRECISION_TRIPLE:
                formatPattern = "0.000";
                break;

            default:
                formatPattern = "0";
        }

        return formatPattern;
    }

    public Double getRCData(){
        String value = super.getText().toString();
        Double fomatedValue = Double.parseDouble( value.replace(',', '.') );
        return fomatedValue;
    }

    /**
     * @param value
     * @param precision  
     */
    public void setRCData(double value, int precision){

        // Workaround to fix localization settings bug: (http://code.google.com/p/android/issues/detail?id=2626)
        // this is very ugly and I know it. :(
        //Double cleanValue = Double.parseDouble( (Double.toString(value)).replace(',', '.'));

        //String formattedValue = new DecimalFormat(formatPattern).format(cleanValue);
        //super.setText(formattedValue);
        DecimalFormat decimalFormat = new DecimalFormat(getFormatString());
        super.setText(decimalFormat.format(value));

        if (D) {
            Log.d(TAG, "Value set: "+ decimalFormat.format(value));
        }
    }

    public int getsFormatType() {
        return sFormatType;
    }

    //public void setsFormatType(int sFormatType) {
    //    this.sFormatType = sFormatType;
    //}

    public int getMinValue() {
        return minValue;
    }

    //public void setMinValue(int minValue) {
    //    this.minValue = minValue;
    //}

    public int getMaxValue() {
        return maxValue;
    }

    //public void setMaxValue(int maxValue) {
    //    this.maxValue = maxValue;
    //}

    //public static void setmFormatting(boolean mFormatting) {
    //    RCEditText.mFormatting = mFormatting;
    //}

    //public static boolean ismFormatting() {
    //    return mFormatting;
    //}

}
