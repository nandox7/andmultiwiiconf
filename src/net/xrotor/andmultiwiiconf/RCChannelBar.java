package net.xrotor.andmultiwiiconf;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class RCChannelBar extends ProgressBar{
  private String text;  
  private Paint textPaint;
  private Rect bounds = new Rect();

  public RCChannelBar(Context context) {
    super(context);
    text = "1500";  
    textPaint = new Paint();  
    textPaint.setColor(Color.WHITE);
    //super.setMax(2000); // Max RC PWM signal
  }

  public RCChannelBar(Context context, AttributeSet attrs) {  
    super(context, attrs);  
    text = "1500";  
    textPaint = new Paint();  
    textPaint.setColor(Color.WHITE);  
  }  

  public RCChannelBar(Context context, AttributeSet attrs, int defStyle) {  
    super(context, attrs, defStyle);  
    text = "1500";  
    textPaint = new Paint();  
    textPaint.setColor(Color.WHITE);  
  } 

  // Custom method to handle RC data values and that substitutes .setProgress(int)
  public void SetRCData(int value){
      text = String.valueOf(value);
      super.setProgress((value - 1000) / 10);
  }

  public synchronized void setText(String text) {  
    this.text = text;  
    drawableStateChanged();  
  }  

  public void setTextColor(int color) {  
    textPaint.setColor(color);  
    drawableStateChanged();  
  }  

  @Override  
  protected synchronized void onDraw(Canvas canvas) {  
    // First draw the regular progress bar, then custom draw our text  
    super.onDraw(canvas);
    textPaint.getTextBounds(text, 0, text.length(), bounds);  
    int x = getWidth() / 2 - bounds.centerX();  
    int y = getHeight() / 2 - bounds.centerY();  
    canvas.drawText(text, x, y, textPaint);  
  }
}