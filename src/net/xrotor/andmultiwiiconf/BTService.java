package net.xrotor.andmultiwiiconf;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.UUID;

public class BTService {
    // Debugging
    private static final String TAG = "AndMultiWiiConf.BTService";
    private static final boolean D = false;

    // Name for the SDP record when creating server socket
    private static final String NAME = "BTAndroid";

    // Unique UUID for this application to use SPP
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;
    private int cState;
    private static boolean versionChecked = false;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    // Constants that indicate the connection operation status
    public static final int CONN_NONE = 0;       // we're doing nothing
    public static final int CONN_SENDING = 1;    // now we're sending data
    public static final int CONN_RECEIVING = 2;  // now we're receiving data

    // constants that indicate the operation type
    public static final int ACTION_READ = 0;
    public static final int ACTION_WRITE = 1;
    public static final int ACTION_CALIBRATE_ACC = 2;    
    public static final int ACTION_CALIBRATE_MAG = 3;

    // Constants for the actions message content
    public static final String MSG_READ = "A";
    public static final String MSG_WRITE = "C";
    public static final String MSG_CALIBRATE = "D";

    // Serial frame size
    public static final int[] SERIAL_FRAME_SIZE = {
        84,  // version 1.7
        116, // version 1.8 patch2
        125, // version 1.9
        155, // version 2.0
        256  // version 2.1
    };
    
    // Temp Byte Data Buffer for incoming data
    private ByteBuffer mInByteBuffer = ByteBuffer.allocate(256);
 
    // States for Mwc Protocol 2.1+
    public final int
        IDLE = 0,
        HEADER_START = 1,
        HEADER_M = 2,
        HEADER_ARROW = 3,
        HEADER_SIZE = 4,
        HEADER_CMD = 5,
        HEADER_ERR = 6;
    
    // Data frame size for Mwc Protocol 2.1
    private int dataSize = -1;
    
    /**
     * Constructor. Prepares a new BluetoothChat session.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     */
    public BTService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        cState = CONN_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of the chat connection
     * @param state  An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        if (D) Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Return the current connection state. */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Return the current operation state. */
    public synchronized int getOperation() {
        return cState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume() */
    public synchronized void start() {
        if (D) Log.d(TAG, "BTService starting");

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        // Start the thread to listen on a BluetoothServerSocket
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
        setState(STATE_LISTEN);
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (D) Log.d(TAG, "BTService stopped");
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}
        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;} 
        setState(STATE_NONE);
        versionChecked = false;
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * @param device  The BluetoothDevice to connect
     */
    public synchronized void connect(BluetoothDevice device) {
        if (D) Log.d(TAG, "BTService trying to connect to: " + device);

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);

    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     * @param socket  The BluetoothSocket on which the connection was made
     * @param device  The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        if (D) Log.d(TAG, "BTService Connected to:" + device);

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        // Cancel the accept thread because we only want to connect to one device
        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(AndMultiWiiConf.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    /**
     * Write to the ConnectedThread in an not synchronized manner
     * @param msg 
     * @see ConnectedThread#write(byte[])
     */
    //public void write(int action, byte[] msg) {
    public void write(byte[] msg) {
        cState = CONN_SENDING;
        // Create temporary object
        ConnectedThread r;

        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }

        // Clear the incoming buffer
        //mInByteBuffer.clear(); // This way resetting 

        // Perform the write  not synchronized
        if (msg != null) {
            r.write(msg);
        }
        cState = CONN_NONE;
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        setState(STATE_LISTEN);

        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        //bundle.putString(AndMultiWiiConf.TOAST, "Unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        setState(STATE_LISTEN);

        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(AndMultiWiiConf.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
        versionChecked = false;
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
            } catch (IOException e) {
                if (D) Log.e(TAG, "listen() failed", e);
            }
            mmServerSocket = tmp;
        }

        @Override
        public void run() {
            if (D) Log.d(TAG, "BEGIN mAcceptThread" + this);

            setName("AcceptThread");
            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                //try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    //socket = mmServerSocket.accept(500);

                //} catch (IOException e) {
                //    if (D) Log.e(TAG, "BTService accept() failed", e);
                //    break;
                //}

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BTService.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                                //TODO: do something here if needed
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice());
                                break;
                            case STATE_NONE:
                                //TODO: do something here if needed
                            case STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    if (D) Log.e(TAG, "BTService Could not close unwanted socket", e);
                                }
                                break;
                        }
                    }
                }
            }
            if (D) Log.i(TAG, "END mAcceptThread");
        }

        public void cancel() {
            if (D) Log.d(TAG, "cancel " + this);
            if(mmServerSocket != null) { // Make sure it exists
                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    if (D) Log.e(TAG, "close() of server failed", e);
                }
            } else {
                Log.i(TAG, "Nothing to close.");
            }
        }
    }


    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                if (D) Log.e(TAG, "BTService create() failed", e);
            }
            mmSocket = tmp;
        }

        /**
         * Run the BTService
         */
        @Override
        public void run() {
            if (D) Log.i(TAG, "BTService BEGIN mConnectThread");
            setName("ConnectThread");

            // Always cancel discovery because it will slow down a connection
            mAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            if (mmSocket != null) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    mmSocket.connect();
                } catch (IOException e) {
                    connectionFailed();
                    // Close the socket
                    try {
                        mmSocket.close();
                    } catch (IOException e2) {
                        Log.e(TAG, "BTService unable to close() socket during connection failure", e2);
                    }
                    // Start the service over to restart listening mode
                    //BTService.this.start();
                    return;
                }

                // Reset the ConnectThread because we're done
                synchronized (BTService.this) {
                    mConnectThread = null;
                }

                // Start the connected thread
                connected(mmSocket, mmDevice);
                
            } else {
                Log.i(TAG, "Socket doesn't exist");
            }

        }
        
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                if (D) Log.e(TAG, "BTService close() of connect socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            if (D) Log.d(TAG, "BTService create ConnectedThread");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {

                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();

            } catch (IOException e) {
                if (D) Log.e(TAG, "Temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        /**
         * Start the listening service and passes it to a instance
         * of DataProcessor when receiving data in the
         * buffer.
         */
        @Override
        public void run() {
            if (D) Log.i(TAG, "BEGIN mConnectedThread");

            byte[] inBuffer = new byte[ SERIAL_FRAME_SIZE[0] ];
            int bytes = 0;
            
            // Keep listening to the InputStream while connected
            while (true) {
                try {                	              	 

                    bytes = mmInStream.read(inBuffer);

                    // Make sure we received something
                    if(bytes > 0) {
                        if (D) Log.d(TAG, "Bytes received: " + bytes);

                        //if ( mDataProcessor.setDataInBuffer(inBuffer, bytes) ) {
                        if (D) Log.d(TAG, "Temp buffer position before setting data: " + mInByteBuffer.position());
                        setDataInBuffer(inBuffer, bytes);
                        //if ( setDataInBuffer(inBuffer, bytes) ) {
                        //    if (D) Log.d(TAG, "Full stream received reporting back to UI");
                            //Send the processed data back to the UI Activity
                            //mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_READ, mInByteBuffer.limit(), -1, mInByteBuffer.array()).sendToTarget();
                            //mInByteBuffer.clear();
                        //}
                    }

                } catch (IOException e) {
                    if (D) Log.e(TAG, "disconnected", e);
                    connectionLost();
                    break;
                }
            }
        }

        /**
         * Write to the connected OutStream.
         * @param outBuffer  The bytes to write
         */
        public void write(byte[] outBuffer) {
            cState = CONN_SENDING;
            try {

                if (D) {
                    for (int i = 0; i < outBuffer.length; i++) {
                        Log.d(TAG, "Write() : Pos[" + i + "] -> " + outBuffer[i]);
                    }
                }

                mmOutStream.write(outBuffer);
                mmOutStream.flush();

                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_WRITE, -1, -1, outBuffer)
                .sendToTarget();
                if (D) Log.i(TAG, "BTServicewrite() sent message");

            } catch (IOException e) {
                if (D) Log.e(TAG, "Exception during write", e);
            }
            cState = CONN_NONE;
        }

        /**
         * Closes the open socket.
         */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                if (D) Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    /**
     * Inserts data in a temp Byte buffer and checks when the
     * data received is complete and ready to report to the UI.
     * 
     * @param buffer byte array containing the data
     * @param bytes number of bytes received
     * @return true if the data is ready to be processed or false if not.
     */
    private boolean setDataInBuffer(byte[] buffer, int bytes) {

        int activeMwcVersion = AndMultiWiiConf.getActiveVersion();

        if (D) {Log.i(TAG,"Received a " 
                + buffer.length + " sized buffer and " 
                + bytes + " bytes to process for version " 
                + activeMwcVersion); 
        }

        if (bytes > 0) {
                
                if (D) Log.i(TAG, "Received buffer: " + DataTools.getHex(buffer, bytes));
    
                // Pass the data to a temp buffer in case we didnt' received the full string.
                if (D) Log.i(TAG, "Temp buffer size before is: " + mInByteBuffer.position() );
    
                // Make sure that the incoming buffer data will not overload the temp buffer.
                if (mInByteBuffer.position() + bytes > mInByteBuffer.capacity()) {
                    if (D) Log.i(TAG, "Temp buffer size might be exceeded with new data clearing it to prevent that" );
                    mInByteBuffer.clear();
                }
               
                // Let's try to determine the version if it wasn't checked already
                // The buffer must be empty as it';s the first data to be received and
                // we need at least 2 bytes to get the version
                if (!versionChecked) {
                    if (bytes >= 2 && mInByteBuffer.position() == 0) {
                        char firstChar = (char) (buffer[0]&0xff);
                        char secondChar = (char) (buffer[1]&0xff);
                        if (firstChar == 'M') {
                            Log.i(TAG, "Old Protocol - Vesion:" + (int) secondChar);
                            if (secondChar == MwcConstants.MWC_VERSIONS_LIST[activeMwcVersion]) {
                                versionChecked = true;
                            } else {
                                Log.e(TAG, "Protocol Mismatch!");
                                versionChecked = false;
                                // Report this to the UI
                                int versionIndex = -1;
                                int iter = 0;
                                for(int versions: MwcConstants.MWC_VERSIONS_LIST) {
                                    if (secondChar == versions) {
                                        versionIndex = iter;
                                    }
                                    iter++;
                                }
                                String textMessage = "";
                                if (versionIndex >= 0) {
                                    textMessage = "Different Multiwii version detected: " + MwcConstants.MWC_VERSIONS_LIST_NAME[versionIndex];
                                } else {
                                    textMessage = "Different Multiwii version detected!";
                                }
                                Message msg = mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_TOAST);
                                Bundle bundle = new Bundle();
                                bundle.putString(AndMultiWiiConf.TOAST, textMessage);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                                return false;
                            }
                            
                        }
                        if (firstChar == '$' && secondChar == 'M') {
                            Log.i(TAG, "New Protocol detected");
                            if (activeMwcVersion == MwcConstants.MWC_VERSION_21 || activeMwcVersion == MwcConstants.MWC_VERSION_22) {
                            versionChecked = true;
                            } else {
                                int versionIndex = 0;
                                Message msg = mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_TOAST);
                                Bundle bundle = new Bundle();
                                bundle.putString(AndMultiWiiConf.TOAST, "Different Multiwii version detected: "
                                        + MwcConstants.MWC_VERSIONS_LIST_NAME[versionIndex]);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                                return false;
                            }
                        }
                        if (D) Log.d(TAG, "Checking Version: " + firstChar);
                    }    
                }
                
               // Old protocol v2.0 or lower
               if (activeMwcVersion <= MwcConstants.MWC_VERSION_20) {
    
                   if (D) Log.d(TAG, "Using old protocol!");
                   
                   // In case the temp buffer is empty make sure the data stream been received is not garbage...
                   if (mInByteBuffer.position() == 0 && 
                           (char) (buffer[0]&0xff) == MwcConstants.DATA_IDENTIFICATION_CHAR[activeMwcVersion] ) {
                       
                        // Insert the incoming buffer data into the temp buffer to process
                        mInByteBuffer.put(buffer, 0, bytes);
                    } else if (mInByteBuffer.position() > 0) {
                        // Append the incoming buffer data into the temp buffer to process
                        mInByteBuffer.put(buffer, 0, bytes);
                    }
        
                    if (D) Log.i(TAG, "Temp buffer size after is: " + mInByteBuffer.position());
                    if (D) {
                        if (mInByteBuffer.position() > 0 ) {
                            Log.i(TAG, "Last char in buffer is: " + ((DataTools.getHex(mInByteBuffer.get(mInByteBuffer.position() - 1))) ));
                        }
                    }
        
                    if ( mInByteBuffer.position() == MwcConstants.SERIAL_FRAME_SIZE[activeMwcVersion] && 
                            (char) (mInByteBuffer.getChar(mInByteBuffer.position() - 2)&0xff) == MwcConstants.DATA_IDENTIFICATION_CHAR[activeMwcVersion] ) {
        
                        if (D) Log.d(TAG, "Buffer is full lets process it.");
        
                        mInByteBuffer.flip();
        
                        if (D) { Log.d(TAG, "Buffer capacity:" + mInByteBuffer.capacity() + " limit: " + mInByteBuffer.limit() + 
                                " position: " + mInByteBuffer.position()); }
                        
                        //if (D) Log.d(TAG, "A enviar: " + DataTools.getHex(mInByteBuffer.array(), mInByteBuffer.limit()));
                        byte[] queueArray = new byte[mInByteBuffer.limit() + 1];
                        System.arraycopy(mInByteBuffer.array(), 0, queueArray, 0, mInByteBuffer.limit() + 1);
                    
                        // All received correctly and check let's send the message back to the main Activity
                        mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_READ,  queueArray.length , -1, queueArray.clone()).sendToTarget();
                        
                        mInByteBuffer.clear();
                        
                        return true;
                    }
    
                } else {
                    // New protocol 2.1+
                    if (D) Log.d(TAG, "Using new protocol!");
                    int bytesToProcess = bytes;
                    int bufferPointer = 0;
                    
                    while(bytesToProcess > 0) {
                        if (mInByteBuffer.position() == 0) {
                            if ( (char) (buffer[bufferPointer]&0xff) == '$') {
                                if (D) Log.d(TAG, "Proper data detected...");
                                // Check if we have enough to determine the datasize
                                if(bytesToProcess >= 4) {
                                    if ((char) (buffer[bufferPointer + 1]&0xff) == 'M' && (char) (buffer[bufferPointer + 2]&0xFF) == '>'){
                                        dataSize = (char) (buffer[bufferPointer + 3]&0xFF);
                                        if (D) Log.d(TAG, "1.Datasize detected: 6 + " + dataSize + " =  " + (dataSize + 6));
                                        // Check if we already received the total stream = datasize + 6 (Header Size + checksum)
                                        if (bytesToProcess >= (dataSize + 6)) {
                                            mInByteBuffer.put(buffer, bufferPointer, (dataSize + 6));
                                            bytesToProcess = bytesToProcess - (dataSize + 6);
                                            bufferPointer = bufferPointer + (dataSize + 6);
                                        } else {
                                            mInByteBuffer.put(buffer, bufferPointer, bytesToProcess);
                                            bytesToProcess = 0;
                                            bufferPointer = bufferPointer + bytesToProcess;
                                        }
                                    }
                                // Not enough bytes lets just fill in the temp buffer
                                } else {
                                    if (D) Log.d(TAG, "Not enought bytes to get data size this time only " + bytesToProcess + " bytes received.");
                                    mInByteBuffer.put(buffer, bufferPointer, bytesToProcess);
                                    bytesToProcess = 0;
                                    bufferPointer = bufferPointer + bytesToProcess;
                                    return true;
                                }
                            // Data received is no good...    
                            } else {
                                Log.i(TAG, "Bad data received...");
                                return false;
                            }
                        }
                        
                        if (D) Log.d(TAG, "Step 2 datasize: " + (dataSize + 6) + " Position: " + mInByteBuffer.position());
                        if (dataSize < 0) {
                          if (mInByteBuffer.position() >= 4) {
                            if ((char) (mInByteBuffer.get(1)&0xff) == 'M' && (char) (mInByteBuffer.get(2)&0xFF) == '>') {
                                dataSize = (char) (mInByteBuffer.get(3)&0xFF);
                                if (D) Log.d(TAG, "2.Datasize detected: 6 + " + dataSize);
                            }
                          }
                          // Check if the received data will allow to read the dataSize byte
                          if (mInByteBuffer.position() < 4 && mInByteBuffer.position() + bytesToProcess >= 4) {
                            // Substract the existing bytes in the temp buffer to match POS 4 of the
                            // data size byte in the incoming data.
                            dataSize = (char) (buffer[bufferPointer + 2 - (mInByteBuffer.position() - 1)]&0xFF);
                            //if (D)Log.d(TAG, DataTools.getHex(buffer[4 - (mInByteBuffer.position() - 1)]));
                            if (D) Log.d(TAG, "3.Datasize detected: " + dataSize);
                          }
                        }       
        
                        if (D) Log.d(TAG, "Step 3 datasize: " + (dataSize + 6) + " Buffer Position: " + mInByteBuffer.position() + " Bytes To Process: " + bytesToProcess);
                        if (dataSize >= 0 && mInByteBuffer.position() + bytesToProcess >= (dataSize + 6)) {
                            
                            // Copy only the ones that fit the remaining bytes of the dataSize
                            int missingBytes = (dataSize + 6) - mInByteBuffer.position();
                            mInByteBuffer.put(buffer, bufferPointer,  missingBytes);
                            bytesToProcess = bytesToProcess - missingBytes;
                            bufferPointer = bufferPointer + missingBytes;
                            
                            if (D) Log.d(TAG, "3. Datasize: " + (dataSize + 6) + " Buffer Position: " + mInByteBuffer.position());
                            
                            if (mInByteBuffer.position() == (dataSize + 6)  && dataSize >= 1) {
                            //if (mInByteBuffer.position() == (dataSize + 6)) {
                                if (checkSum(dataSize)) {
                                    if (D) Log.d(TAG, "3.Full stream received and checked reporting back to UI");
                                    mInByteBuffer.flip();
                                
                                    //if (D) Log.d(TAG, "A enviar: " + DataTools.getHex(mInByteBuffer.array(), mInByteBuffer.limit()));
                                    byte[] queueArray = new byte[dataSize + 6];
                                    System.arraycopy(mInByteBuffer.array(), 0, queueArray, 0, dataSize + 6);
                                
                                    // All received correctly and check let's send the message back to the main Activity
                                    mHandler.obtainMessage(AndMultiWiiConf.MESSAGE_READ,  queueArray.length , -1, queueArray.clone()).sendToTarget();
                                } else {
                                    if (D) Log.d(TAG, "Checksum failed!");
                                }
                            }
                            
                            // Reset the position on the temp buffer
                            mInByteBuffer.clear();
                            // Clear the data size var
                            dataSize = -1;
                            if (D) Log.d(TAG, "3.Still bytes to process: " + bytesToProcess + " Buffer Position: " + mInByteBuffer.position());
                            
                        } else if (bytesToProcess > 0 ){
                            if (D) Log.d(TAG, "3.Processing remaining bytes: " + bytesToProcess);
                            mInByteBuffer.put(buffer, bufferPointer, bytesToProcess);
                            bytesToProcess = 0;
                            bufferPointer = bufferPointer + bytesToProcess;
                            return true;
                        }
                    } 
            }
        } 
        if (D) Log.i(TAG, "Temp buffer position after is: " + mInByteBuffer.position());
                
        // There was nothing to process
        return false;
    }

    /**
     * Verify the checksum
     * 
     * @param size
     * @return state as boolean
     */
    private boolean checkSum(int size) {
        byte checkSum = 0;
        
        for(int i = 0; i < size + 2; i++) {
            checkSum ^= mInByteBuffer.get(i + 3)&0xFF;
            //if (D) Log.d(TAG, "Checking byte: " + DataTools.getHex(mInByteBuffer.get(i + 3))  + " Current Checksum " +
            //       DataTools.getHex(checkSum));
        }

        if(checkSum == mInByteBuffer.get(size + 5)) {
            if (D) { Log.d(TAG, "Checksum check passed!"); }
            return true;
        } else {
            if (D) { Log.d(TAG, "Checksum check failed! Checksum: " + (mInByteBuffer.get(mInByteBuffer.position() - 1)) + " Calc Checksum: " + checkSum); }
            return false;
        }
    }
    
    /**
     * Called when the class instance is destroyed 
     */
    @Override
    public void finalize() {
        if (D) Log.d(TAG, "Destroying class BTService");
    }
}