package net.xrotor.andmultiwiiconf;

import android.util.Log;

import net.xrotor.andmultiwiiconf.protocol.Mwc2_1;
import net.xrotor.andmultiwiiconf.protocol.Mwc2_2;

import java.nio.ByteBuffer;

public class DataProcessor {

    // Debugging
    private static final String TAG = "AndMultiWiiConf.DataProcessor";
    private static final boolean D = true;

    // Temp Byte Data Buffer for incoming data
    ByteBuffer mInByteBuffer = ByteBuffer.allocate(180);

    // Byte array with that to process
    public byte[] dataBuffer = new byte[ MwcConstants.SERIAL_FRAME_SIZE[ AndMultiWiiConf.getActiveVersion() ] ];

    //String tempString = "";

    // BOXNAMES
    private String[] BOXNAMES;

    int bufPointer; // Buffer frame pointer

    // MWC data params vars
    int multiType;  // 1 for tricopter, 2 for quad+, 3 for quadX

    int version;

    float gx,gy,gz = 0;
    float ax,ay,az = 0;
    float baro = 0;
    float mag = 0;
    float angx,angy = 0;
    float r;
    int init_com = 0;
    int graph_on = 0;

    float[] motorArray = new float[8];
    float[] servoArray = new float[8];

    float rcThrottle = 0,
	  rcRoll     = 0,
	  rcPitch    = 0,
	  rcYaw      = 0,
	  rcAUX1     = 0,
	  rcAUX2     = 0,
	  rcAUX3     = 0,
	  rcAUX4     = 0;

    int nunchukPresent,
        i2cAccPresent,
        i2cBaroPresent,
        i2cMagnetoPresent,
        evelMode;

    int byteP_ROLL,
        byteI_ROLL,
        byteD_ROLL,
        byteP_PITCH,
        byteI_PITCH,
        byteD_PITCH,
        byteP_YAW,
        byteI_YAW,
        byteD_YAW,
        byteP_LEVEL,
        byteI_LEVEL,
        byteD_LEVEL,
        byteRC_RATE,
        byteRC_EXPO,
        byteACC_STRENGTH,
        byteRollPitchRate,
        byteYawRate,
        byteDynThrPID;

    int[] activationState = new int[40];

    boolean[] sensorsPresent = new boolean[] {false, false, false, false, false};
    boolean[] sensorsState = new boolean[] {false, false, false, false, false};

    int cycleTime;

    // New vars for version 1.8
    int magx,
        magy,
        magz;

/*
    int mot6,
        mot7,
        mot8;
*/

    int byteP_ALT,
        byteI_ALT,
        byteD_ALT,
        byteP_VEL,
        byteI_VEL,
        byteD_VEL,
        byteP_MAG;

    int pMeterSum,
        intPowerTrigger,
        bytevbat;

    int debug1,
        debug2,
        debug3,
        debug4;
    // -------------------------

    // New vars for version 1.9
    int GPS_distanceToHome,
        GPS_directionToHome,
        GPS_numSat,
        GPS_fix,
        GPS_update;

    int gpsPresent;

    // New vars for version 2.0
    int i2cError;

    int byteP_GPS,
        byteI_GPS,
        byteD_GPS;

/*
    float servo4 = 0,
	  servo5 = 0,
	  servo6 = 0,
	  servo7 = 0;
*/

    // New vars for version 2.1
    int byteP_POS,
        byteI_POS,
        byteD_POS,
        byteP_POSR,
        byteI_POSR,
        byteD_POSR,
        byteP_NAVR,
        byteI_NAVR,
        byteD_NAVR,
        byteThr_MID,
        byteThr_EXPO;

    int GPS_latitude,
        GPS_longitude,
        GPS_altitude,
        GPS_speed,
        head,
        alt;

    int sonarPresent;

    // **
    // Buffer processing methods
    // -------------------------
    public final int read32() {
        return (dataBuffer[bufPointer++]&0xff) + ((dataBuffer[bufPointer++]&0xff)<<8) + ((dataBuffer[bufPointer++]&0xff)<<16) + ((dataBuffer[bufPointer++]&0xff)<<24);
    }

    public final int read16() {
        return (dataBuffer[bufPointer++]&0xff) + (dataBuffer[bufPointer++]<<8);
    }

    public final int read8() {
        return dataBuffer[bufPointer++]&0xff;
    }
    // -------------------------

    public boolean processData() {


    	switch (AndMultiWiiConf.getActiveVersion() ) {
    	case MwcConstants.MWC_VERSION_17:
    	    if (D) Log.d(TAG, "Active version 1.7 processing data");
    	    if ( processData_17() ) return true;
    	    if (D) Log.d(TAG, "Data processing failed.");
    	    break;
    	case MwcConstants.MWC_VERSION_18:
    	    if (D) Log.d(TAG, "Active version 1.8 processing data");
    	    if ( processData_18() ) return true;
    	    if (D) Log.d(TAG, "Data processing failed.");
    	    break;
    	case MwcConstants.MWC_VERSION_19:
    	    if (D) Log.d(TAG, "Active version 1.9 processing data");
    	    if ( processData_19() ) return true;
    	    if (D) Log.d(TAG, "Data processing failed.");
    	    break;
    	case MwcConstants.MWC_VERSION_20:
    	    if (D) Log.d(TAG, "Active version 2.0 processing data");
    	    if ( processData_20() ) return true;
    	    if (D) Log.d(TAG, "Data processing failed.");
    	    break;
    	case MwcConstants.MWC_VERSION_21:
            if (D) Log.d(TAG, "Active version 2.1 processing data");
            if ( processData_21() ) return true;
            if (D) Log.d(TAG, "Data processing failed.");
            break;
        case MwcConstants.MWC_VERSION_22:
            if (D) Log.d(TAG, "Active version 2.2 processing data");
            if ( processData_22() ) return true;
            if (D) Log.d(TAG, "Data processing failed.");
            break;
    	}
    	return false;
    }

    // Old process data for ver 1.7
    public boolean processData_17() {

	int present = 0;
	int mode = 0;

	if (D) {
	    Log.d(TAG, "Processing data for v1.7" );
	    Log.d(TAG, "Data String size:" + dataBuffer.length );
	    Log.d(TAG, "Last byte: " + ((char)(dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_17] - 1]&0xff)));
	    Log.d(TAG, DataTools.getHex(dataBuffer) );
	}

	if ( ((char) (dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_17] - 1]&0xff)) == 'A' ) {

	    if (D) Log.d(TAG, "Processing data for v1.7..");

	    //bufPointer = 0; // only when using the fake MWC service
	    bufPointer = 1; //starting with 1 to ingnore pos=0 that is the A

	    ax = read16();
	    ay = read16();
	    az = read16();

	    gx = read16();
	    gy = read16();
	    gz = read16();

	    baro = read16();
	    mag = read16();

	    int lenght = servoArray.length;
	    for (int i = 0; i < lenght - 4; i++) {
	        servoArray[i] = read16();
	    }

	    lenght = motorArray.length;
        for (int i = 0; i < lenght - 2; i++) {
            motorArray[i] = read16();
        }

	    rcRoll = read16();
	    rcPitch = read16();
	    rcYaw = read16();
	    rcThrottle = read16();

	    rcAUX1 = read16();
	    rcAUX2 = read16();
	    rcAUX3 = read16();
	    rcAUX4 = read16(); //52

	    present = read8();
	    mode = read8();
	    cycleTime = read16();

	    angx = read16();
	    angy = read16();

	    multiType = read8(); //61

	    byteP_ROLL = read8();
	    byteI_ROLL = read8();
	    byteD_ROLL = read8();

	    byteP_PITCH = read8();
	    byteI_PITCH = read8();
	    byteD_PITCH = read8();

	    byteP_YAW = read8();
	    byteI_YAW = read8();
	    byteD_YAW = read8(); // 70


	    byteP_LEVEL = read8();
	    byteI_LEVEL = read8();

	    byteRC_RATE = read8();
	    byteRC_EXPO = read8();

	    byteRollPitchRate = read8();
	    byteYawRate = read8();
	    byteDynThrPID = read8();

	    activationState[0] = read8(); // Level
	    activationState[1] = read8(); // Baro
	    activationState[2] = read8(); // Mag
	    activationState[3] = read8(); // CamStab
	    activationState[4] = read8(); // CamTrig

	    // Processing present - Sensors Presence
        if ((present&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // NK
        if ((present&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // ACC
        if ((present&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // BARO
        if ((present&8) >0) sensorsState[3] = true; else sensorsState[3] = false ; // MAG

	    // Processing Mode - Sensors State
	    if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
	    if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
	    if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG

	    return true;
	}

	return false;
    }

    // New process data for ver 1.8
    public boolean processData_18() {

	int present = 0;
	int mode = 0;

	if (D) {
	    Log.d(TAG, "Processing data for v1.8" );
	    Log.d(TAG, "Data String size:" + dataBuffer.length );
	    Log.d(TAG, "Last byte: " + ((char)(dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_18] - 1]&0xff)));
	    Log.d(TAG, DataTools.getHex(dataBuffer) );
	}

	if ( ((char) (dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_18] - 1]&0xff)) == 'M' ) {

	    if (D) Log.d(TAG, "Processing data for v1.8...");

	    //bufPointer = 0; // only when using the fake MWC service
	    bufPointer = 1; //starting with 1 to ignore pos=0 that is the M

	    version = read8();

	    ax = read16();
	    ay = read16();
	    az = read16();

	    gx = read16();
	    gy = read16();
	    gz = read16();

	    magx = read16();
	    magy = read16();
	    magz = read16(); //19

	    baro = read16();
	    mag = read16();

        for (int i = 0; i < servoArray.length - 4; i++) {
            servoArray[i] = read16();
        }

        for (int i = 0; i < motorArray.length; i++) {
            motorArray[i] = read16();
        }

	    rcRoll = read16();
	    rcPitch = read16();
	    rcYaw = read16();
	    rcThrottle = read16();

	    rcAUX1 = read16();
	    rcAUX2 = read16();
	    rcAUX3 = read16();
	    rcAUX4 = read16(); //63

	    present = read8();
	    mode = read8();
	    cycleTime = read16();

	    angx = read16();
	    angy = read16();

	    multiType = read8(); //72

	    byteP_ROLL = read8();
	    byteI_ROLL = read8();
	    byteD_ROLL = read8();

	    byteP_PITCH = read8();
	    byteI_PITCH = read8();
	    byteD_PITCH = read8();

	    byteP_YAW = read8();
	    byteI_YAW = read8();
	    byteD_YAW = read8(); // 70

	    byteP_ALT = read8();
	    byteI_ALT = read8();
	    byteD_ALT = read8();

	    byteP_VEL = read8();
	    byteI_VEL = read8();
	    byteD_VEL = read8(); // 70

	    byteP_LEVEL = read8();
	    byteI_LEVEL = read8(); //89
	    byteP_MAG = read8();

	    byteRC_RATE = read8();
	    byteRC_EXPO = read8();

	    byteRollPitchRate = read8();
	    byteYawRate = read8();
	    byteDynThrPID = read8(); //95

	    activationState[0] = read8(); // Level
	    activationState[1] = read8(); // Baro
	    activationState[2] = read8(); // Mag
	    activationState[3] = read8(); // CamStab
	    activationState[4] = read8(); // CamTrig
	    activationState[5] = read8(); // Arm

	    pMeterSum = read8();
	    intPowerTrigger = read16();
	    bytevbat = read8();

	    debug1 = read16();
	    debug2 = read16();
	    debug3 = read16();
	    debug4 = read16();

	    // Process multi-mode bytes
	    if ((present&1) >0) nunchukPresent = 1; else  nunchukPresent = 0;
	    if ((present&2) >0) i2cAccPresent = 1; else  i2cAccPresent = 0;
	    if ((present&4) >0) i2cBaroPresent = 1; else  i2cBaroPresent = 0;
	    if ((present&8) >0) i2cMagnetoPresent = 1; else  i2cMagnetoPresent = 0;

	    // Processing Mode - Sensors State
	    if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
	    if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
	    if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG

	    return true;
	}

	return false;
    }

    // New process data for ver 1.9
    public boolean processData_19() {

	int present = 0;
	int mode = 0;

	if (D) {
	    Log.d(TAG, "Processing data for v1.9" );
	    Log.d(TAG, "Data String size:" + dataBuffer.length );
	    Log.d(TAG, "Last byte: " + ((char)(dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_19] - 1]&0xff)));
	    Log.d(TAG, DataTools.getHex(dataBuffer) );
	}

	if ( ((char) (dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_19] - 1]&0xff)) == 'M' ) {

	    if (D) Log.d(TAG, "Processing data for v1.9...");

	    //bufPointer = 0; // only when using the fake MWC service
	    bufPointer = 1; //starting with 1 to ingnore pos=0 that is the M

	    version = read8();

	    ax = read16();
	    ay = read16();
	    az = read16();

	    gx = read16();
	    gy = read16();
	    gz = read16();

	    magx = read16();
	    magy = read16();
	    magz = read16(); //19

	    baro = read16();
	    mag = read16();

	    for (int i = 0; i < servoArray.length - 4; i++) {
            servoArray[i] = read16();
        }

        for (int i = 0; i < motorArray.length; i++) {
            motorArray[i] = read16();
        }

	    rcRoll = read16();
	    rcPitch = read16();
	    rcYaw = read16();
	    rcThrottle = read16();

	    rcAUX1 = read16();
	    rcAUX2 = read16();
	    rcAUX3 = read16();
	    rcAUX4 = read16(); //63

	    present = read8();
	    mode = read8();
	    cycleTime = read16();

	    angx = read16();
	    angy = read16();

	    multiType = read8(); //72

	    byteP_ROLL = read8();
	    byteI_ROLL = read8();
	    byteD_ROLL = read8();

	    byteP_PITCH = read8();
	    byteI_PITCH = read8();
	    byteD_PITCH = read8();

	    byteP_YAW = read8();
	    byteI_YAW = read8();
	    byteD_YAW = read8(); // 70

	    byteP_ALT = read8();
	    byteI_ALT = read8();
	    byteD_ALT = read8();

	    byteP_VEL = read8();
	    byteI_VEL = read8();
	    byteD_VEL = read8(); // 70

	    byteP_LEVEL = read8();
	    byteI_LEVEL = read8(); //89
	    byteP_MAG = read8();

	    byteRC_RATE = read8();
	    byteRC_EXPO = read8();

	    byteRollPitchRate = read8();
	    byteYawRate = read8();
	    byteDynThrPID = read8(); //95

	    activationState[0] = read8(); // Level
	    activationState[1] = read8(); // Baro
	    activationState[2] = read8(); // Mag
	    activationState[3] = read8(); // CamStab
	    activationState[4] = read8(); // CamTrig
	    activationState[5] = read8(); // Arm
	    activationState[6] = read8(); // GPSHome
	    activationState[7] = read8(); // GPSHold


	    pMeterSum = read8();
	    intPowerTrigger = read16();
	    bytevbat = read8();

	    debug1 = read16();
	    debug2 = read16();
	    debug3 = read16();
	    debug4 = read16();

	    // Process multi-mode bytes
	    if ((present&1) >0) nunchukPresent = 1; else  nunchukPresent = 0;
	    if ((present&2) >0) i2cAccPresent = 1; else  i2cAccPresent = 0;
	    if ((present&4) >0) i2cBaroPresent = 1; else  i2cBaroPresent = 0;
	    if ((present&8) >0) i2cMagnetoPresent = 1; else  i2cMagnetoPresent = 0;
	    if ((present&16) >0) gpsPresent = 1; else  gpsPresent = 0;

	    // Processing Mode - Sensors State
	    if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
	    if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
	    if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG
	    if ((mode&8) >0) sensorsState[3] = true; else sensorsState[3] = false ; // GPS

	    return true;
	}

	return false;
    }

    // New process data for ver 2.0
    public boolean processData_20() {

	int present = 0;
	int mode = 0;

	if (D) {
	    Log.d(TAG, "Processing data for v2.0" );
	    Log.d(TAG, "Data String size:" + dataBuffer.length );
	    Log.d(TAG, "Last byte: " + ((char)(dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_20] - 1]&0xff)));
	    Log.d(TAG, DataTools.getHex(dataBuffer) );
	}

	if ( ((char) (dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_20] - 1]&0xff)) == 'M' ) {

	    if (D) Log.d(TAG, "Setting vars with data...");

	    //bufPointer = 0; // only when using the fake MWC service
	    bufPointer = 1; //starting with 1 to ingnore pos=0 that is the M

	    version = read8();

	    ax = read16();
	    ay = read16();
	    az = read16();

	    gx = read16() / 8;
	    gy = read16() / 8;
	    gz = read16() / 8;

	    magx = read16() / 3;
	    magy = read16() / 3;
	    magz = read16() / 3; //19

	    baro = read16();
	    mag = read16(); //23

	    for (int i = 0; i < servoArray.length; i++) {
            servoArray[i] = read16();
        }

	    for (int i = 0; i < motorArray.length; i++) {
            motorArray[i] = read16();
        }

	    rcRoll = read16();
	    rcPitch = read16();
	    rcYaw = read16();
	    rcThrottle = read16();

	    rcAUX1 = read16();
	    rcAUX2 = read16();
	    rcAUX3 = read16();
	    rcAUX4 = read16(); //63

	    present = read8();
	    mode = read8();

	    cycleTime = read16();
	    i2cError = read16();

	    angx = read16() / 10;
	    angy = read16() / 10;

	    multiType = read8(); //72

	    byteP_ROLL = read8();
	    byteI_ROLL = read8();
	    byteD_ROLL = read8();

	    byteP_PITCH = read8();
	    byteI_PITCH = read8();
	    byteD_PITCH = read8();

	    byteP_YAW = read8();
	    byteI_YAW = read8();
	    byteD_YAW = read8(); // 70

	    if (D) Log.i(TAG, "Value of byteP_YAW: " + byteP_YAW);

	    byteP_ALT = read8();
	    byteI_ALT = read8();
	    byteD_ALT = read8();

	    byteP_VEL = read8();
	    byteI_VEL = read8();
	    byteD_VEL = read8();

	    byteP_GPS = read8();
	    byteI_GPS = read8(); //89
	    byteD_GPS = read8();

	    byteP_LEVEL = read8();
	    byteI_LEVEL = read8(); //89
	    byteD_LEVEL = read8();

	    byteP_MAG = read8();
	    read8(); // null PID
	    read8(); // null PID

	    byteRC_RATE = read8();
	    byteRC_EXPO = read8();

	    byteRollPitchRate = read8();
	    byteYawRate = read8();
	    byteDynThrPID = read8(); //95

	    //int teste =0;
	    for (int iter = 0; iter < MwcConstants.MWC_20_BOX_LIST.size(); iter++) {
	        activationState[iter] = read8();
	        //if(D) Log.d(TAG, "1. Activation " + iter + " :" + activationState[iter]);

	        //teste = read8() & 63;
	        //if(D) Log.d(TAG, "2. Activation " + iter + " :" + teste);

            //activationState[iter] += (teste << 6);
            //if(D) Log.d(TAG, "3. Result " + iter + " :" + activationState[iter]);
            activationState[iter] += ((read8() & 63) << 6);
	    }

	    GPS_distanceToHome = read16();
	    GPS_directionToHome = read16();
	    GPS_numSat = read16();
	    GPS_fix = read8();
	    GPS_update = read8();

	    pMeterSum = read8();
	    intPowerTrigger = read16();
	    bytevbat = read8();

	    debug1 = read16();
	    debug2 = read16();
	    debug3 = read16();
	    debug4 = read16();

	    // Process multi-mode bytes
	    if ((present&1) >0) nunchukPresent = 1; else  nunchukPresent = 0;
	    if ((present&2) >0) i2cAccPresent = 1; else  i2cAccPresent = 0;
	    if ((present&4) >0) i2cBaroPresent = 1; else  i2cBaroPresent = 0;
	    if ((present&8) >0) i2cMagnetoPresent = 1; else  i2cMagnetoPresent = 0;
	    if ((present&16) >0) gpsPresent = 1; else  gpsPresent = 0;

	    // Processing Mode - Sensors State
	    if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
	    if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
	    if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG
	    if ((mode&8) >0) sensorsState[3] = true; else sensorsState[3] = false ; // GPS

	    return true;
	}

	return false;
    }

    // New process data for ver 2.1
    public boolean processData_21() {
        bufPointer = 5;

        int present = 0;
        int mode = 0;

        if (D) Log.d(TAG, "Message type detected: " + dataBuffer[4]);
        switch (dataBuffer[4]) {

            case Mwc2_1.MSP_IDENT:
                version = read8();
                multiType = read8();
                read8();  // MSP version
                read32(); // capability
                if (D) Log.d(TAG, "Mwc Version read: " + version + " Platform: " + multiType);
                break;

            case Mwc2_1.MSP_STATUS:
                cycleTime = read16();
                i2cError = read16();
                present = read16();
                mode = read32();

                // Process multi-mode bytes
                if ((present&1) >0) i2cAccPresent = 1; else  i2cAccPresent = 0;
                if ((present&2) >0) i2cBaroPresent = 1; else  i2cBaroPresent = 0;
                if ((present&4) >0) i2cMagnetoPresent = 1; else  i2cMagnetoPresent = 0;
                if ((present&8) >0) gpsPresent = 1; else  gpsPresent = 0;
                if ((present&16) >0) sonarPresent = 1; else  sonarPresent = 0;

                // Processing Mode - Sensors State
                if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
                if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
                if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG
                if ((mode&8) >0) sensorsState[3] = true; else sensorsState[3] = false ; // GPS
                if ((mode&16) >0) sensorsState[4] = true; else sensorsState[4] = false ; // SONAR
                break;

            case Mwc2_1.MSP_RAW_IMU:
                ax = read16();
                ay = read16();
                az = read16();
                gx = read16()/8;
                gy = read16()/8;
                gz = read16()/8;
                magx = read16()/3;
                magy = read16()/3;
                magz = read16()/3;
                break;

            case Mwc2_1.MSP_SERVO:
                for (int i = 0; i < servoArray.length; i++) {
                    servoArray[i] = read16();
                }
                break;

            case Mwc2_1.MSP_MOTOR:
                for (int i = 0; i < motorArray.length; i++) {
                    motorArray[i] = read16();
                }
                break;

            case Mwc2_1.MSP_RC:
                rcRoll = read16();
                rcPitch = read16();
                rcYaw = read16();
                rcThrottle = read16();
                rcAUX1 = read16();
                rcAUX2 = read16();
                rcAUX3 = read16();
                rcAUX4 = read16();
                break;

            case Mwc2_1.MSP_RAW_GPS:
                GPS_fix = read8();
                GPS_numSat = read8();
                GPS_latitude = read32();
                GPS_longitude = read32();
                GPS_altitude = read16();
                GPS_speed = read16();
                break;

            case Mwc2_1.MSP_COMP_GPS:
                GPS_distanceToHome = read16();
                GPS_directionToHome = read16();
                GPS_update = read8();
                break;

            case Mwc2_1.MSP_ATTITUDE:
                angx = read16()/10;
                angy = read16()/10;
                head = read16();
                break;

            case Mwc2_1.MSP_ALTITUDE:
                alt = read32();
                break;

            case Mwc2_1.MSP_BAT:
                bytevbat = read8();
                pMeterSum = read16();
                break;
            case Mwc2_1.MSP_MISC:
                intPowerTrigger = read16();
                break;

            case Mwc2_1.MSP_RC_TUNING:
                byteRC_RATE = read8();
                byteRC_EXPO = read8();
                byteRollPitchRate = read8();
                byteYawRate = read8();
                byteDynThrPID = read8();
                byteThr_MID = read8();
                byteThr_EXPO = read8();
                break;

            case Mwc2_1.MSP_PID:
                byteP_ROLL = read8();
                byteI_ROLL = read8();
                byteD_ROLL = read8();

                byteP_PITCH = read8();
                byteI_PITCH = read8();
                byteD_PITCH = read8();

                byteP_YAW = read8();
                byteI_YAW = read8();
                byteD_YAW = read8();

                if (D) Log.i(TAG, "Value of byteP_YAW: " + byteP_YAW);

                byteP_ALT = read8();
                byteI_ALT = read8();
                byteD_ALT = read8();

                byteP_POS = read8();
                byteI_POS = read8();
                byteD_POS = read8();

                byteP_POSR = read8();
                byteI_POSR = read8();
                byteD_POSR = read8();

                byteP_NAVR = read8();
                byteI_NAVR = read8();
                byteD_NAVR = read8();

                byteP_LEVEL = read8();
                byteI_LEVEL = read8(); //89
                byteD_LEVEL = read8();

                byteP_MAG = read8();
                read8(); // null PID
                read8(); // null PID
                break;

            case Mwc2_1.MSP_BOX:
                for(int iter = 0; iter < MwcConstants.MWC_21_BOX_LIST.size(); iter++) {
                    activationState[iter] = read16();
                }
            default:
                break;
        }
        return true;
    }

    // New process data for ver 2.2
    public boolean processData_22() {
        bufPointer = 5;

        int present = 0;
        int mode = 0;

        if (D) Log.d(TAG, "Message type detected: " + dataBuffer[4]);
        switch (dataBuffer[4]) {

            case Mwc2_2.MSP_IDENT:
                if (D) Log.d(TAG, "Parsing MSP_IDENT");
                version = read8();
                multiType = read8();
                read8();  // MSP version
                read32(); // capability
                if (D) Log.d(TAG, "Mwc Version read: " + version + " Platform: " + multiType);
                break;

            case Mwc2_2.MSP_STATUS:
                if (D) Log.d(TAG, "Parsing MSP_STATUST");
                cycleTime = read16();
                i2cError = read16();
                present = read16();
                mode = read32();

                // Process multi-mode bytes
                if ((present&1) >0) i2cAccPresent = 1; else  i2cAccPresent = 0;
                if ((present&2) >0) i2cBaroPresent = 1; else  i2cBaroPresent = 0;
                if ((present&4) >0) i2cMagnetoPresent = 1; else  i2cMagnetoPresent = 0;
                if ((present&8) >0) gpsPresent = 1; else  gpsPresent = 0;
                if ((present&16) >0) sonarPresent = 1; else  sonarPresent = 0;

                // Processing Mode - Sensors State
                if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
                if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
                if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG
                if ((mode&8) >0) sensorsState[3] = true; else sensorsState[3] = false ; // GPS
                if ((mode&16) >0) sensorsState[4] = true; else sensorsState[4] = false ; // SONAR
                break;

            case Mwc2_2.MSP_RAW_IMU:
                if (D) Log.d(TAG, "Parsing MSP_RAW_IMU");
                ax = read16();
                ay = read16();
                az = read16();
                gx = read16()/8;
                gy = read16()/8;
                gz = read16()/8;
                magx = read16()/3;
                magy = read16()/3;
                magz = read16()/3;
                break;

            case Mwc2_2.MSP_SERVO:
                if (D) Log.d(TAG, "Parsing MSP_SERVO");
                for (int i = 0; i < servoArray.length; i++) {
                    servoArray[i] = read16();
                }
                break;

            case Mwc2_2.MSP_MOTOR:
                if (D) Log.d(TAG, "Parsing MSP_MOTOR");
                for (int i = 0; i < motorArray.length; i++) {
                    motorArray[i] = read16();
                }
                break;

            case Mwc2_2.MSP_RC:
                if (D) Log.d(TAG, "Parsing MSP_RC");
                rcRoll = read16();
                rcPitch = read16();
                rcYaw = read16();
                rcThrottle = read16();
                rcAUX1 = read16();
                rcAUX2 = read16();
                rcAUX3 = read16();
                rcAUX4 = read16();
                break;

            case Mwc2_2.MSP_RAW_GPS:
                if (D) Log.d(TAG, "Parsing MSP_GPS");
                GPS_fix = read8();
                GPS_numSat = read8();
                GPS_latitude = read32();
                GPS_longitude = read32();
                GPS_altitude = read16();
                GPS_speed = read16();
                break;

            case Mwc2_2.MSP_COMP_GPS:
                if (D) Log.d(TAG, "Parsing MSP_COMP_GPS");
                GPS_distanceToHome = read16();
                GPS_directionToHome = read16();
                GPS_update = read8();
                break;

            case Mwc2_2.MSP_ATTITUDE:
                if (D) Log.d(TAG, "Parsing MSP_ATTITUDE");
                angx = read16()/10;
                angy = read16()/10;
                head = read16();
                break;

            case Mwc2_2.MSP_ALTITUDE:
                if (D) Log.d(TAG, "Parsing MSP_ALTITUDE");
                alt = read32();
                break;

            case Mwc2_2.MSP_BAT:
                if (D) Log.d(TAG, "Parsing MSP_BAT");
                bytevbat = read8();
                pMeterSum = read16();
                break;

            case Mwc2_2.MSP_MISC:
                if (D) Log.d(TAG, "Parsing MSP_MISC");
                intPowerTrigger = read16();
                break;

            case Mwc2_2.MSP_RC_TUNING:
                if (D) Log.d(TAG, "Parsing MSP_RC_TUNING");
                byteRC_RATE = read8();
                byteRC_EXPO = read8();
                byteRollPitchRate = read8();
                byteYawRate = read8();
                byteDynThrPID = read8();
                byteThr_MID = read8();
                byteThr_EXPO = read8();
                break;

            case Mwc2_2.MSP_PID:
                if (D) Log.d(TAG, "Parsing MSP_PID");
                byteP_ROLL = read8();
                byteI_ROLL = read8();
                byteD_ROLL = read8();

                byteP_PITCH = read8();
                byteI_PITCH = read8();
                byteD_PITCH = read8();

                byteP_YAW = read8();
                byteI_YAW = read8();
                byteD_YAW = read8();

                if (D) Log.i(TAG, "Value of byteP_YAW: " + byteP_YAW);

                byteP_ALT = read8();
                byteI_ALT = read8();
                byteD_ALT = read8();

                byteP_POS = read8();
                byteI_POS = read8();
                byteD_POS = read8();

                byteP_POSR = read8();
                byteI_POSR = read8();
                byteD_POSR = read8();

                byteP_NAVR = read8();
                byteI_NAVR = read8();
                byteD_NAVR = read8();

                byteP_LEVEL = read8();
                byteI_LEVEL = read8(); //89
                byteD_LEVEL = read8();

                byteP_MAG = read8();
                read8(); // null PID
                read8(); // null PID
                break;

            case Mwc2_2.MSP_BOXNAMES:
                BOXNAMES = new String(dataBuffer, 0, dataBuffer.length).split(";");
                Log.d(TAG, "Received BOXNAMES: " + new String(dataBuffer, 0, dataBuffer.length));
                // Create required activationState and Update UI
                //activationState = new int[BOXNAMES.length - 1];
                break;

            case Mwc2_2.MSP_BOX:
                if (D) {
                    Log.d(TAG, "Parsing MSP_BOX");
                    Log.d(TAG, "BOXNAMES Size: " + BOXNAMES.length);
                }
                //for(int iter = 0; iter < MwcConstants.MWC_22_BOX_LIST.size(); iter++) {
                for(int iter = 0; iter < BOXNAMES.length - 1; iter++) {
                    activationState[iter] = read16();
                }
                break;

            default:
                break;
        }
        return true;
    }

    /**
     * @Log.i some data
     */
    public void outputData() {

    	Log.i(TAG, "P_ROLL is :" + String.valueOf(byteP_ROLL / 10) );
    	Log.i(TAG, "I_ROLL is :" + byteI_ROLL);
    	Log.i(TAG, "D_ROLL is :" + byteD_ROLL);
    	Log.i(TAG, "ACC_X is :" + ax);
    	Log.i(TAG, "ACC_Y is :" + ay);
    	Log.i(TAG, "ACC_Z is :" + az);
    	Log.i(TAG, "RCThrotle is :" + rcThrottle);
    	Log.i(TAG, "RCRoll is :" + rcRoll);
    	Log.i(TAG, "RCPitch is :" + rcPitch);
    	Log.i(TAG, "RCYaw is :" + rcYaw);
    	Log.i(TAG, "P_Yaw is :" + byteP_YAW);
    	Log.i(TAG, "P_Level is :" + byteP_LEVEL);
    	Log.i(TAG, "P_Alt is :" + byteP_ALT);
    	Log.i(TAG, "D_Alt is :" + byteD_ALT);
    	Log.i(TAG, "MAG is :" + byteP_MAG);
    	Log.i(TAG, "Baro is :" + i2cBaroPresent);

    }

    public int[] getParameters(int mwcVersion){
	int[] paramsString = null;
	int iterA = 0;

	switch(mwcVersion){
	case MwcConstants.MWC_VERSION_17:
	    paramsString = new int[32];

	    paramsString[iterA++] = 'C'; // control char
        paramsString[iterA++] = byteP_ROLL;
        paramsString[iterA++] = byteI_ROLL;
        paramsString[iterA++] = byteD_ROLL;
        paramsString[iterA++] = byteP_PITCH;
        paramsString[iterA++] = byteI_PITCH;
        paramsString[iterA++] = byteD_PITCH;
        paramsString[iterA++] = byteP_YAW;
        paramsString[iterA++] = byteI_YAW;
        paramsString[iterA++] = byteD_YAW;
        paramsString[iterA++] = byteP_LEVEL;
        paramsString[iterA++] = byteI_LEVEL;
        paramsString[iterA++] = byteRC_RATE;
        paramsString[iterA++] = byteRC_EXPO;
        paramsString[iterA++] = byteRollPitchRate;
        paramsString[iterA++] = byteYawRate;
        paramsString[iterA++] = byteDynThrPID;

        for(int iterB = 0; iterB <= 5; iterB++) {
            paramsString[iterA++] = activationState[iterB];
        }
	    break;

	case MwcConstants.MWC_VERSION_18:
	    paramsString = new int[32];

        paramsString[iterA++] = 'W'; // control char
        paramsString[iterA++] = byteP_ROLL;
        paramsString[iterA++] = byteI_ROLL;
        paramsString[iterA++] = byteD_ROLL;
        paramsString[iterA++] = byteP_PITCH;
        paramsString[iterA++] = byteI_PITCH;
        paramsString[iterA++] = byteD_PITCH;
        paramsString[iterA++] = byteP_YAW;
        paramsString[iterA++] = byteI_YAW;
        paramsString[iterA++] = byteD_YAW;
        paramsString[iterA++] = byteP_ALT;
        paramsString[iterA++] = byteI_ALT;
        paramsString[iterA++] = byteD_ALT;
        paramsString[iterA++] = byteP_VEL;
        paramsString[iterA++] = byteI_VEL;
        paramsString[iterA++] = byteD_VEL;
        paramsString[iterA++] = byteP_LEVEL;
        paramsString[iterA++] = byteI_LEVEL;
        paramsString[iterA++] = byteP_MAG;
        paramsString[iterA++] = byteRC_RATE;
        paramsString[iterA++] = byteRC_EXPO;
        paramsString[iterA++] = byteRollPitchRate;
        paramsString[iterA++] = byteYawRate;
        paramsString[iterA++] = byteDynThrPID;

        for(int iterB = 0; iterB < 6; iterB++) {
            paramsString[iterA++] = activationState[iterB];
        }

	    paramsString[30] = intPowerTrigger;
	    break;

	case MwcConstants.MWC_VERSION_19:
	    paramsString = new int[33];

        paramsString[iterA++] = 'W'; // control char
        paramsString[iterA++] = byteP_ROLL;
        paramsString[iterA++] = byteI_ROLL;
        paramsString[iterA++] = byteD_ROLL;
        paramsString[iterA++] = byteP_PITCH;
        paramsString[iterA++] = byteI_PITCH;
        paramsString[iterA++] = byteD_PITCH;
        paramsString[iterA++] = byteP_YAW;
        paramsString[iterA++] = byteI_YAW;
        paramsString[iterA++] = byteD_YAW;
        paramsString[iterA++] = byteP_ALT;
        paramsString[iterA++] = byteI_ALT;
        paramsString[iterA++] = byteD_ALT;
        paramsString[iterA++] = byteP_VEL;
        paramsString[iterA++] = byteI_VEL;
        paramsString[iterA++] = byteD_VEL;
        paramsString[iterA++] = byteP_LEVEL;
        paramsString[iterA++] = byteI_LEVEL;
        paramsString[iterA++] = byteP_MAG;
        paramsString[iterA++] = byteRC_RATE;
        paramsString[iterA++] = byteRC_EXPO;
        paramsString[iterA++] = byteRollPitchRate;
        paramsString[iterA++] = byteYawRate;
        paramsString[iterA++] = byteDynThrPID;

        for(int iterB = 0; iterB < 8; iterB++) {
            paramsString[iterA++] = activationState[iterB];
        }

	    paramsString[iterA++] = intPowerTrigger;
	    break;

	case MwcConstants.MWC_VERSION_20:
	    paramsString = new int[54];

	    paramsString[iterA++] = 'W'; // control char
	    paramsString[iterA++] = byteP_ROLL;
	    paramsString[iterA++] = byteI_ROLL;
	    paramsString[iterA++] = byteD_ROLL;
	    paramsString[iterA++] = byteP_PITCH;
	    paramsString[iterA++] = byteI_PITCH;
	    paramsString[iterA++] = byteD_PITCH;
	    paramsString[iterA++] = byteP_YAW;
	    paramsString[iterA++] = byteI_YAW;
	    paramsString[iterA++] = byteD_YAW;
	    paramsString[iterA++] = byteP_ALT;
	    paramsString[iterA++] = byteI_ALT;
	    paramsString[iterA++] = byteD_ALT;
	    paramsString[iterA++] = byteP_VEL;
	    paramsString[iterA++] = byteI_VEL;
	    paramsString[iterA++] = byteD_VEL;
	    paramsString[iterA++] = byteP_GPS;
	    paramsString[iterA++] = byteI_GPS;
	    paramsString[iterA++] = byteD_GPS;
	    paramsString[iterA++] = byteP_LEVEL;
	    paramsString[iterA++] = byteI_LEVEL;
	    paramsString[iterA++] = byteD_LEVEL;
	    paramsString[iterA++] = byteP_MAG;
	    paramsString[iterA++] = byteP_MAG;
	    paramsString[iterA++] = byteP_MAG;
	    paramsString[iterA++] = byteRC_RATE;
	    paramsString[iterA++] = byteRC_EXPO;
	    paramsString[iterA++] = byteRollPitchRate;
	    paramsString[iterA++] = byteYawRate;
	    paramsString[iterA++] = byteDynThrPID;

	    for(int iterB = 0; iterB < 24; iterB++) {
	        paramsString[iterA++] = activationState[iterB];
	    }

	    break;

	case MwcConstants.MWC_VERSION_21:
        paramsString = new int[65];

        paramsString[iterA++] = Mwc2_1.MSP_SET_PID;
        // PID
        paramsString[iterA++] = byteP_ROLL;
        paramsString[iterA++] = byteI_ROLL;
        paramsString[iterA++] = byteD_ROLL;
        paramsString[iterA++] = byteP_PITCH;
        paramsString[iterA++] = byteI_PITCH;
        paramsString[iterA++] = byteD_PITCH;
        paramsString[iterA++] = byteP_YAW;
        paramsString[iterA++] = byteI_YAW;
        paramsString[iterA++] = byteD_YAW;
        paramsString[iterA++] = byteP_ALT;
        paramsString[iterA++] = byteI_ALT;
        paramsString[iterA++] = byteD_ALT;
        paramsString[iterA++] = byteP_POS;
        paramsString[iterA++] = byteI_POS;
        paramsString[iterA++] = 0;
        paramsString[iterA++] = byteP_POSR;
        paramsString[iterA++] = byteI_POSR;
        paramsString[iterA++] = byteD_POSR;
        paramsString[iterA++] = byteP_NAVR;
        paramsString[iterA++] = byteI_NAVR;
        paramsString[iterA++] = byteD_NAVR;
        paramsString[iterA++] = byteP_LEVEL;
        paramsString[iterA++] = byteI_LEVEL;
        paramsString[iterA++] = byteD_LEVEL;
        paramsString[iterA++] = byteP_MAG;
        paramsString[iterA++] = 0;
        paramsString[iterA++] = 0;
        paramsString[iterA++] = byteRC_RATE;
        paramsString[iterA++] = byteRC_EXPO;
        paramsString[iterA++] = byteRollPitchRate;
        paramsString[iterA++] = byteYawRate;
        paramsString[iterA++] = byteDynThrPID;
        paramsString[iterA++] = byteThr_MID;
        paramsString[iterA++] = byteThr_EXPO;

        for(int iterB = 0; iterB < 28; iterB++) {
            paramsString[iterA++] = activationState[iterB];
        }

        paramsString[iterA++] = intPowerTrigger;
        paramsString[iterA++] = intPowerTrigger;
        break;

    case MwcConstants.MWC_VERSION_22:
        paramsString = new int[77];

        paramsString[iterA++] = Mwc2_2.MSP_SET_PID;
        // PID
        paramsString[iterA++] = byteP_ROLL;
        paramsString[iterA++] = byteI_ROLL;
        paramsString[iterA++] = byteD_ROLL;
        paramsString[iterA++] = byteP_PITCH;
        paramsString[iterA++] = byteI_PITCH;
        paramsString[iterA++] = byteD_PITCH;
        paramsString[iterA++] = byteP_YAW;
        paramsString[iterA++] = byteI_YAW;
        paramsString[iterA++] = byteD_YAW;
        paramsString[iterA++] = byteP_ALT;
        paramsString[iterA++] = byteI_ALT;
        paramsString[iterA++] = byteD_ALT;
        paramsString[iterA++] = byteP_POS;
        paramsString[iterA++] = byteI_POS;
        paramsString[iterA++] = 0;
        paramsString[iterA++] = byteP_POSR;
        paramsString[iterA++] = byteI_POSR;
        paramsString[iterA++] = byteD_POSR;
        paramsString[iterA++] = byteP_NAVR;
        paramsString[iterA++] = byteI_NAVR;
        paramsString[iterA++] = byteD_NAVR;
        paramsString[iterA++] = byteP_LEVEL;
        paramsString[iterA++] = byteI_LEVEL;
        paramsString[iterA++] = byteD_LEVEL;
        paramsString[iterA++] = byteP_MAG;
        paramsString[iterA++] = 0;
        paramsString[iterA++] = 0;
        paramsString[iterA++] = byteRC_RATE;
        paramsString[iterA++] = byteRC_EXPO;
        paramsString[iterA++] = byteRollPitchRate;
        paramsString[iterA++] = byteYawRate;
        paramsString[iterA++] = byteDynThrPID;
        paramsString[iterA++] = byteThr_MID;
        paramsString[iterA++] = byteThr_EXPO;

        for(int iterB = 0; iterB < 40; iterB++) {
            paramsString[iterA++] = activationState[iterB];
        }

        paramsString[iterA++] = intPowerTrigger;
        paramsString[iterA++] = intPowerTrigger;
        break;
	}
	return paramsString;
    }

    /**
     * Return a list containing the box names
     * @return boxnames as String[]
     */
    public String[] getBoxNames() {
        return BOXNAMES;
    }

    /**
     * Return the values readings for the motors outputs and
     * RC channels inputs.
     * @param mwcVersion int representing the Mwc version
     * @return float[] containing the rc channels values
     */
    public float[] getChannels(int mwcVersion) {
    	float[] paramsString = new float[16];

    	// Motor readings
        for (int i = 0; i < motorArray.length; i++) {
            paramsString[i] = motorArray[i];
        }

    	// Channels readings
    	paramsString[8] = rcRoll;
    	paramsString[9] = rcPitch;
    	paramsString[10] = rcYaw;
    	paramsString[11] = rcThrottle;
    	paramsString[12] = rcAUX1;
    	paramsString[13] = rcAUX2;
    	paramsString[14] = rcAUX3;
    	paramsString[15] = rcAUX4;

    	if (D) {
    	    for (int i = 0; i < paramsString.length; i++) {
    		Log.d(TAG, "getChannels() : Pos[" + i + "] -> "
    			+ paramsString[i]);
    	    }
    	}
    	return paramsString;
    }

    /**
     * Return the values readings for the sensors
     * @param mwcVersion int representing the Mwc current version
     * @return float[] containing the sensor readings
     */
    public float[] getSensors(int mwcVersion) {
    	float[] sensorStream = new float[6];

    	// Gyro readings
    	sensorStream[0] = gx;
    	sensorStream[1] = gy;
    	sensorStream[2] = gz;

    	// Acc readings
    	sensorStream[3] = ax;
    	sensorStream[4] = ay;
    	sensorStream[5] = az;

    	if (D) {
    	    for (int i = 0; i < sensorStream.length; i++) {
    		Log.d(TAG, "getSensors() : Pos[" + i + "] -> "
    			+ sensorStream[i]);
    	    }
    	}
    	return sensorStream;
    }

    /**
     * Return the values readings for the sensors
     * @param mwcVersion int representing the Mwc current version
     * @return float[] containing the sensor readings
     */
    public boolean[] getSensorState(int mwcVersion) {
        return sensorsState;
    }

    /**
     *
     * @param buffer
     * @param bytes
     * @return state
     */
    public boolean setData(byte[] buffer, int bytes) {
    	try {
    	    if (D) Log.i(TAG, "Received buffer: " + DataTools.getHex(buffer));

    	    this.dataBuffer = buffer;
    	    if (processData()) {
    	        return true;
    	    }
    	} catch (Exception e) {
    	    if (D) { Log.e(TAG, "Exception eccurred: " + e.toString());}
    	}
    	return false;
    }

    public byte[] getData() {
        return dataBuffer;
    }

    public int getLenght() {
        return dataBuffer.length;
    }

    @Override
    public void finalize() {
      if (D) Log.d(TAG, "Destroying class DataProcessor");
    }

}