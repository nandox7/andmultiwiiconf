package net.xrotor.andmultiwiiconf;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;
import com.bugsense.trace.BugSenseHandler;
import net.xrotor.andmultiwiiconf.ValueChangeDialog.OnMyDialogResult;
import net.xrotor.andmultiwiiconf.protocol.Mwc2_2;
import net.xrotor.andmultiwiiconf.protocol.MwcProtocol;
import net.xrotor.andmultiwiiconf.protocol.Mwc2_1;
import java.util.Timer;
import java.util.TimerTask;

public class AndMultiWiiConf extends Activity implements OnTouchListener {

    // Debugging
    private static final String TAG = "AndMultiWiiConf.Main";
    private static final boolean D = false;

    private static final boolean isReleaseVersion = true;

    // Message types sent from the BTService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Message types sent from the MwcParameters Handler
    public static final int PARAMS_WRITE = 6;

    // Key names received from the BTService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Layout views
    private TextView mStatusBar;

    // Intent request & result codes
    public static final int REQUEST_CONNECT_DEVICE = 1;
    public static final int REQUEST_ENABLE_BT = 2;
    public static final int RESULT_DEVICE_SELECTED = 3;
    public static final int RESULT_ENABLE_BT = 4;
    public static final int REQUEST_PROFILE_ACTION = 5;
    public static final int RESULT_LOAD_PROFILE = 6;
    public static final int RESULT_SAVE_PROFILE = 7;
    public static final int REQUEST_VERSION_CHANGE = 8;
    public static final int RESULT_VERSION_CHANGED = 9;
    public static final int REQUEST_OPEN_MONITOR = 10;

    // Name of the connected device
    //private String mConnectedDeviceName = null;

    // UI update thread
    //private Handler uiHandler = new Handler();
    boolean mMonitorState = false;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;

    // Will hold the Bluetooth device MAC address
    private static String btMacAddress = null;
    private static String btName = null;

    // Will hold the local Bluetooth device state
    private int mBTState;

    // Member object for the SPP services
    private BTService mBTService = null;

    // Member object for storing the parameters
    private MwcParameters mMwcParameters = new MwcParameters();

    // Member object for processing the incoming stream data
    private DataProcessor mDataProcessor = new DataProcessor();

    // Member object that provides methods to interact with the UI
    private MwcUIHelper mMwcUIHelper;

    // Member object that provides methods to interact with the GraphView
    private MwcGraph mMwcGraph;

    // Member object that provides methods to interact with the profiles
    MwcProfiles mMwcProfiles = new MwcProfiles(mMwcParameters);

    // Store the ID for the currently active MWC version
    private static int mActiveMwcVersion;

    private MwcProtocol mMwcProtocol;

    // Store the constant values for the MWC version
    private MwcConstantValues mMwccontantValues;

    // States if the Slider should be used instead of the keyboard
    private static boolean useSliderDialog = false;

    // Vars for viewflipper action
    private ViewFlipper mViewFlipper;
    float downXValue; // Stores X position for touch down event
    float downYValue; // Stores Y position for touch down event

    // Handler to deal with the UI auto update
    //private Handler mHandler = new Handler();
    private Timer mTimer = null;
    private TimerTask mTimerTask = null;
    //private static int delay = 1000;  //1s
    private static int frequency = 1000;  //1s
    // State var to detect read actions while auto-updating
    private boolean readSent = false;

    // Round Progress BAR
    //final ProgressButton progressButton1 = (ProgressButton) findViewById(R.id.motor_progress_1);

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Editor editor = getPreferences(Context.MODE_PRIVATE).edit();

        // Handle item selection
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.connect:
                if (D)
                    Log.d(TAG, "connect button pressed");
                // Connect to the define MAC address device
                setupBTService();
                return true;

            case R.id.disconnect:
                if (D)
                    Log.d(TAG, "disconnect button pressed");
                // Connect to the define MAC address device
                mBTService.stop();
                return true;

            case R.id.calibrateACC:
                if (D)
                    Log.d(TAG, "calibrate button pressed");
                // Send a calibrate request to the MWC board
                // if (mBTState == BTService.STATE_CONNECTED) {
                // mBTService.write(BTService.ACTION_CALIBRATE, null,
                // mActiveMwcVersion); };
                if (mBTState == BTService.STATE_CONNECTED) {
                    //mBTService.write(MwcCommands.calibrateACC(mActiveMwcVersion));
                    mBTService.write(mMwcProtocol.calibrateACC(mActiveMwcVersion));
                }
                showMessage("   Calibration executed\nWait for process to finish");
                //Toast.makeText(this,
                //    "   Calibration executed\nWait for process to finish",
                //    Toast.LENGTH_LONG).show();
                return true;

            case R.id.calibrateMAG:
                if (D)
                    Log.d(TAG, "calibrate button pressed");
                // Send a calibrate request to the MWC board
                // if (mBTState == BTService.STATE_CONNECTED) {
                // mBTService.write(BTService.ACTION_CALIBRATE, null,
                // mActiveMwcVersion); };
                if (mBTState == BTService.STATE_CONNECTED) {
                    //mBTService.write(MwcCommands.calibrateMAG(mActiveMwcVersion));
                    mBTService.write(mMwcProtocol.calibrateMAG(mActiveMwcVersion));
                }
                showMessage("   Calibration executed\nWait for process to finish");
                //Toast.makeText(this,
                //    "   Calibration executed\nWait for process to finish",
                //    Toast.LENGTH_LONG).show();
                return true;

            case R.id.saveScreenshot:
                if (D)
                    Log.d(TAG, "saving screenshot");
                // Save a screenshot of the current screen
                View myCurrentView = ((ViewGroup) findViewById(android.R.id.content))
                        .getChildAt(0);
                mMwcUIHelper.saveScreenshot(myCurrentView);
                return true;

            case R.id.resetToDefaults:
                if (D)
                    Log.d(TAG, "resetToDefaults button pressed");
                // Reset the mMwcParameters to default values and update the UI
                if (mActiveMwcVersion <= MwcConstants.MWC_VERSION_20) {
                    mMwcParameters.resetToDefaults(mActiveMwcVersion);
                    mMwcUIHelper.setUIParams(mMwcParameters, mActiveMwcVersion);
                } else {
                    mBTService.write(mMwcProtocol.resetCONF(mActiveMwcVersion));
                    mBTService.write(mMwcProtocol.readPARAMS(mActiveMwcVersion));
                }
                return true;

            case R.id.findMAC:
                if (D)
                    Log.d(TAG, "find device button pressed");
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getApplicationContext(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                return true;

            case R.id.mwcVersion:
                if (D)
                    Log.d(TAG, "Change Mwc Version button pressed");
                Intent mwcversionIntent = new Intent(getApplicationContext(),
                        VersionChooseActivity.class);
                mwcversionIntent.putExtra("ACTIVE_VERSION",
                        MwcConstants.MWC_VERSIONS_LIST[mActiveMwcVersion]);
                startActivityForResult(mwcversionIntent, REQUEST_VERSION_CHANGE);
                return true;

            case R.id.mnuKeepSreenOn:
                if (D)
                    Log.d(TAG, "Keep Screen On option pressed");
                // Check current state and reverse it.
                if(item.isChecked()) {
                    MwcUIHelper.setKeepScreenOn(false);
                    editor.putBoolean("KeepScreenOn", false);
                } else {
                    MwcUIHelper.setKeepScreenOn(true);
                    editor.putBoolean("KeepScreenOn", true);
                }
                // Save to preferences to the "Keep Screen On" State
                editor.commit();
                return true;

            case R.id.mnuUseSlider:
                if (D)
                    Log.d(TAG, "Use the slider option pressed");
                // Check current state and reverse it.
                if(item.isChecked()) {
                    useSliderDialog = false;
                    editor.putBoolean("UseSlider", false);
                } else {
                    useSliderDialog = true;
                    editor.putBoolean("UseSlider", true);
                }
                // Save to preferences to the "Use Slider" State
                editor.commit();
                return true;

            case R.id.readProfile:
                if (D)
                    Log.d(TAG, "profile been read");
                Intent loadProfileIntent = new Intent(getApplicationContext(),
                        ProfileListActivity.class);
                loadProfileIntent.putExtra("ACTION",
                        ProfileListActivity.PROFILE_ACTION_LOAD);
                startActivityForResult(loadProfileIntent, REQUEST_PROFILE_ACTION);
                return true;

            case R.id.saveProfile:
                if (D)
                    Log.d(TAG, "profile been saved");
                Intent saveProfiletIntent = new Intent(getApplicationContext(),
                        ProfileListActivity.class);
                saveProfiletIntent.putExtra("ACTION",
                        ProfileListActivity.PROFILE_ACTION_SAVE);
                startActivityForResult(saveProfiletIntent, REQUEST_PROFILE_ACTION);
                return true;

            case R.id.aboutApp:
                if (D)
                    Log.e(TAG, "find device buttin pressed");
                // Launch the DeviceListActivity to see devices and do scan
                mMwcUIHelper.showAboutApp(this);
                return true;

            case R.id.exitApp:
                // Check if the connections is active and close it
                if (mBTState != BTService.STATE_NONE) {
                    mBTService.stop();
                }
                // End application
                System.exit(RESULT_OK);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /** Called when the menu button is pressed in the activity **/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        // Change enabled menu options according to
        // BT connection state
        MenuItem connectItem = menu.findItem(R.id.connect);
        MenuItem disconnectItem = menu.findItem(R.id.disconnect);
        MenuItem calibrateItem = menu.findItem(R.id.calibrate);
        MenuItem calibrateACCItem = menu.findItem(R.id.calibrateACC);
        MenuItem calibrateMAGItem = menu.findItem(R.id.calibrateMAG);
        MenuItem resetToDefaultsItem = menu.findItem(R.id.resetToDefaults);
        MenuItem keepScreeOn = menu.findItem(R.id.mnuKeepSreenOn);
        MenuItem useSlider = menu.findItem(R.id.mnuUseSlider);

        if (mBTState == BTService.STATE_CONNECTED) {
            connectItem.setVisible(false);
            disconnectItem.setVisible(true);
            calibrateItem.setEnabled(true);
            calibrateACCItem.setEnabled(true);
            calibrateMAGItem.setVisible(mActiveMwcVersion >= MwcConstants.MWC_VERSION_18); // Only required for v1.8+
            resetToDefaultsItem.setEnabled(true);
        } else {
            connectItem.setVisible(true);
            disconnectItem.setVisible(false);
            calibrateItem.setEnabled(false);
            calibrateACCItem.setEnabled(false);
            resetToDefaultsItem.setEnabled(false);
        }

        // Change enabled state for menu connect option
        // only when there is a defined MAC address
        if (btMacAddress != null) {
            connectItem.setEnabled(true);
        } else {
            connectItem.setEnabled(false);
        }

        // Get the stored settings for keeping the Screen On and
        // set the menuitem checked state accordingly.
        keepScreeOn.setChecked(getPreferences(Context.MODE_PRIVATE).getBoolean("KeepScreenOn", false));

        // Get the stored settings for "Use the Slider" and
        // set the menuitem checked state accordingly.
        useSlider.setChecked(getPreferences(Context.MODE_PRIVATE).getBoolean("UseSlider", false));

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (D) Log.e(TAG, "requestCode " + requestCode + " with onActivityResult "
                    + resultCode);

        // Select depending from what requestCode the activity returned
        switch (requestCode) {

            case REQUEST_CONNECT_DEVICE:
                if (D) Log.e(TAG, "REQUEST_CONNECT_DEVICE returned");

                // Activity returned from REQUEST_CONNECT_DEVICE select depending on
                // the resultCode
                switch (resultCode) {
                    case RESULT_DEVICE_SELECTED:
                        if (D) Log.e(TAG, "REQUEST_CONNECT_SELECTED returned");

                        // Get the selected device MAC address and Name
                        AndMultiWiiConf.btMacAddress = data.getExtras().getString(
                                DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                        AndMultiWiiConf.btName = data.getExtras().getString(
                                DeviceListActivity.EXTRA_DEVICE_NAME);

                        if(AndMultiWiiConf.btName != null) {
                            mStatusBar.setText(getString(R.string.selected_bt_device)
                                    + AndMultiWiiConf.btName);
                        } else {
                            if (AndMultiWiiConf.btMacAddress != null ) {
                                mStatusBar.setText(getString(R.string.selected_bt_mac)
                                        + AndMultiWiiConf.btMacAddress);
                            } else {
                                mStatusBar.setText(getString(R.string.no_device_selected));
                            }
                        }
                        break;
                    case RESULT_ENABLE_BT:
                        if (D) Log.e(TAG, "REQUEST_ENABLE_BT returned");

                        // When the DeviceListActivity returns a request to enable Bluetooth
                        Intent enableIntent = new Intent(
                                BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                        break;
                    default:
                        break;
                }

            case REQUEST_PROFILE_ACTION:
                switch (resultCode) {
                    case RESULT_LOAD_PROFILE:
                        // Load the prfile data
                        if (mMwcProfiles.loadProfile(data.getExtras().getString(
                                ProfileListActivity.SELECTED_PROFILE_NAME))) {
                            // Update the UI with the values
                            mMwcUIHelper.setUIParams(mMwcParameters,
                                    mActiveMwcVersion);
                            Log.d(TAG, "profile read successefully!");
                        } else {
                            Log.d(TAG, "profile read failed!");
                        }
                        break;
                    case RESULT_SAVE_PROFILE:
                        // Get values from the UI and pass them to the MwcParameters
                        // instance
                        mMwcUIHelper.getUIParams(mMwcParameters, mActiveMwcVersion);
                        // Save the data to the profile
                        mMwcProfiles.saveProfile(
                                data.getExtras().getString(
                                        ProfileListActivity.SAVE_PROFILE_NAME),
                                        mMwcParameters.getParamsData(),
                                        // Pass the version number not the ID
                                        MwcConstants.MWC_VERSIONS_LIST[mActiveMwcVersion]
                                );
                    default:
                        break;
                }
            case REQUEST_VERSION_CHANGE:
                switch (resultCode) {
                    case RESULT_VERSION_CHANGED:
                        // Get the new selected version and assign to the member var
                        int mwcVersionNumber = data.getExtras().getInt(
                                VersionChooseActivity.SELECTED_VERSION);
                        mActiveMwcVersion = MwcProfiles
                                .getVersionIndex(mwcVersionNumber);
                        // Save to preferences the default Mwc Version
                        Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                        editor.putInt("DefaultMwcVersion", mActiveMwcVersion);
                        editor.commit();
                        // Reload the UI layout to reflect the active Mwc Version
                        mMwcUIHelper.reloadLayoutToVersion(mActiveMwcVersion);
                        // Define the Protocol to use
                        setProtocol(mActiveMwcVersion);
                        if (D) Log.d(TAG, "Layout reload requested for version "
                                    + mActiveMwcVersion);
                }
            default:
                break;

        }
    }

    //public static AndMultiWiiConf getInstance() {
    //    return sInstance;
    //}

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (D) Log.e(TAG, "--- ON CREATE ---");

        // Make sure we only set the trace reporting on a release version
        if (isReleaseVersion) BugSenseHandler.initAndStartSession(this, "7ce8275e");

        // Hide the Status Bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Hide keyboard :
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.activity_params);

        // Set the local BT device
        mBTState = BTService.STATE_NONE;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adaptor is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            showMessage(getString(R.string.no_bluetooth));
            //Toast.makeText(this, R.string.no_bluetooth, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        // Get saved data from preferences
        // Set the state for "Use Slider" from the setting stored in preferences
        useSliderDialog = getPreferences(Context.MODE_PRIVATE).getBoolean("UseSlider", false);

        // Get MAC address of last connected device
        String lastConnectedMAC = getPreferences(Context.MODE_PRIVATE)
                .getString("lastConnectedMAC", "");
        // Get Name address of last connected device
        String lastConnectedName = getPreferences(Context.MODE_PRIVATE)
                .getString("lastConnectedName", "");
        // Get default Mwc version
        mActiveMwcVersion = getPreferences(Context.MODE_PRIVATE).getInt(
                "DefaultMwcVersion", MwcConstants.MWC_DEFAULT_VERSION);

        if (D)
            Log.d(TAG, "Read from preferences { LastMAC: " + lastConnectedMAC
                    + "LastName: " + lastConnectedName + "ActiveVersion: "
                    + mActiveMwcVersion);

        // Set the Protocol to use
        setProtocol(mActiveMwcVersion);

        // Set our data handling objects
        mMwccontantValues = new MwcConstantValues(mActiveMwcVersion);

        // Initialize the MwcUIControls instance
        LinearLayout layMain = (LinearLayout) findViewById(R.id.layout_parameters_main);
        if (layMain != null) {
            mMwcUIHelper = new MwcUIHelper(layMain);
            mMwcUIHelper.reloadLayoutToVersion(mActiveMwcVersion);

            // Set the "Keep Screen On" from the setting stored in preferences
            MwcUIHelper.setKeepScreenOn(getPreferences(Context.MODE_PRIVATE).getBoolean("KeepScreenOn", false));

            // OnTouchListener added to the main view
            // needed to detect the finger swipe in the main layout
            layMain.setOnTouchListener(this);
        }

        // Define ViewFlipper
        // Create new OnTouchListener to allow siding within a scrollview
        OnTouchListener myOnTouchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                long downTime = 0;
                int currentChildViewIndex = mViewFlipper.getDisplayedChild();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        downXValue = event.getX();
                        downTime = event.getEventTime();
                        break;
                    }

                    case MotionEvent.ACTION_UP: {
                        float currentX = event.getX();
                        long currentTime = event.getEventTime();
                        float difference = Math.abs(downXValue - currentX);
                        long time = currentTime - downTime;

                        if (D)
                            Log.i(TAG, "Touch Event Distance: " + difference
                                    + "px Time: " + time + "ms CurrentViewIndex: "
                                    + mViewFlipper.getDisplayedChild());

                        if ((downXValue < currentX) && (time > 150)
                                && (difference > 150)) {
                            if (D)
                                Log.i(TAG, "flip to Previous");
                            // Check if the current view of the ViewFlipper is not
                            // the leftmost one
                            if (mViewFlipper.getDisplayedChild() > 0) {
                                // Set the animation
                                mViewFlipper.setInAnimation(mMwcUIHelper.inFromLeftAnimation());
                                mViewFlipper.setOutAnimation(mMwcUIHelper.outToRightAnimation());
                                // Flip!
                                mViewFlipper.showPrevious();
                            }
                        }

                        if ((downXValue > currentX) && (time > 150)
                                && (difference > 150)) {
                            if (D)
                                Log.i(TAG, "flip to Next");
                            // Check if the current view of the ViewFlipper is not
                            // the rightmost one
                            if (mViewFlipper.getDisplayedChild() < MwcConstants.VIEW_COUNT) {
                                // Set the animation
                                mViewFlipper.setInAnimation(mMwcUIHelper.inFromRightAnimation());
                                mViewFlipper.setOutAnimation(mMwcUIHelper.outToLeftAnimation());
                                // Flip!
                                mViewFlipper.showNext();
                            }
                        }
                        break;
                    }
                }
                // Update the style of the flipperDot's to show current panel
                mMwcUIHelper.dotViewChange(mViewFlipper.getDisplayedChild(), currentChildViewIndex, mMonitorState);
                return false;
            }
        };

        mViewFlipper = (ViewFlipper) findViewById(R.id.viewFlipperMain);

        /*
         *  Assign the custom OnTouchListener to the Scrollviews
         *  to allow side scroll
         */
        ScrollView mScrollView = (ScrollView) findViewById(R.id.scrollView1);
        mScrollView.setOnTouchListener(myOnTouchListener);

        mScrollView = (ScrollView) findViewById(R.id.scrollView2);
        mScrollView.setOnTouchListener(myOnTouchListener);

        mScrollView = (ScrollView) findViewById(R.id.scrollView3);
        mScrollView.setOnTouchListener(myOnTouchListener);

        mScrollView = (ScrollView) findViewById(R.id.scrollView4);
        mScrollView.setOnTouchListener(myOnTouchListener);

        // On create update the StatusBar with the last device
        // that was connected (get's it's MAC from the settings)
        mStatusBar = (TextView) findViewById(R.id.status_bar);
        if (lastConnectedName.equals("")) {
            mStatusBar.setText(getString(R.string.no_device_selected));
            btMacAddress = null;
        } else {
            mStatusBar.setText(getString(R.string.last_bt_device) + lastConnectedName);
            btMacAddress = lastConnectedMAC;
        }

        // Check if the Activity was started from a called intent passing a
        // Profile file
        Intent receivedIntent = getIntent();
        if (receivedIntent.getExtras() != null) {

            if (D)
                Log.d(TAG, "Received profile file from intent: "
                        + receivedIntent.getData().getEncodedPath());

            // Load the Profile received into the UI and get version
            if (mMwcProfiles.loadProfile(receivedIntent.getData()
                    .getEncodedPath())) {
                // Check the profile version and change UI layout to match
                // version if required
                int version = MwcProfiles
                        .getVersionIndex(MwcProfiles.readVersion(receivedIntent
                                .getData().getEncodedPath()));

                if (mMwcUIHelper != null) {
                    if (version != mActiveMwcVersion) {
                        mActiveMwcVersion = version;
                        mMwcUIHelper.reloadLayoutToVersion(mActiveMwcVersion);
                    }
                    mMwcUIHelper.setUIParams(mMwcParameters, mActiveMwcVersion);
                    Toast.makeText(getApplicationContext(), "Profile loaded", Toast.LENGTH_LONG)
                    .show();
                }
            }
        }

        //Init draw the sensors graph
        mMwcGraph = new MwcGraph(getApplicationContext(), MwcConstants.MWC_SENSOR_SERIES_SIZE,
            MwcConstants.MWC_SENSOR_SERIES_MIN_VALUE,
            MwcConstants.MWC_SENSOR_SERIES_MAX_VALUE);
        mMwcGraph.initSensorGraph();

        LinearLayout layout = (LinearLayout) findViewById(R.id.MwcSensorsGraph);
        layout.addView(mMwcGraph.getGraphView());

    }

    private String getProtocolVersion(int mActiveMwcVersion2) {
        switch (mActiveMwcVersion2) {
                case 4:
                    return "net.xrotor.andmultiwiiconf.protocol.Mwc2_1";
                case 5:
                    return "net.xrotor.andmultiwiiconf.protocol.Mwc2_2";
                default:
                    return "net.xrotor.andmultiwiiconf.protocol.Mwc_old";
        }
    }

    /**
     * Catches the onClick event triggered by button views on the layout
     * @param v View that called the action
     */
    public void clickDataAction(View v) {

        // Check if the we have an  active connection
        if (mBTState == BTService.STATE_CONNECTED) {
            switch (v.getId()) {

                /* Button READ pressed */
                case R.id.btnParamsRead:
                    if (D)  Log.i(TAG, "Button Read pressed.");
                    mBTService.write(mMwcProtocol.readPARAMS(mActiveMwcVersion));
                    // Used to detect read while auto-updating
                    if (mMonitorState) {
                        readSent = true;
                    }
                    showMessage("Reading parameters");
                    break;

                /* Button WRITE pressed */
                case R.id.btnParamsWrite:
                    if (D)  Log.i(TAG, "Button Write pressed.");

                    // Get values from the UI and lead them in the mMwcParameters
                    mMwcUIHelper.getUIParams(mMwcParameters, mActiveMwcVersion);

                    // Get values from the state of the Box's and pass them into the mMwcParameters
                    mMwcUIHelper.getChkBoxes(mMwcParameters, mActiveMwcVersion);

                    // Get the message bytes and tell the BTService to write
                    if (mActiveMwcVersion <= 3) {
                        /*mBTService.write(
                                MwcCommands.writePARAMS(
                                        mMwcParameters.getParamstoArray(mActiveMwcVersion),
                                        mActiveMwcVersion)
                                );
                        */
                        mBTService.write(mMwcParameters.getParamstoArray(mActiveMwcVersion));
                    } else {
                        // Read all data needed for the UI
                        mBTService.write(mMwcProtocol.setPID(mActiveMwcVersion, mMwcParameters.getValues(mActiveMwcVersion, Mwc2_1.MSP_SET_PID)));
                        mBTService.write(mMwcProtocol.setRCTUNING(mActiveMwcVersion, mMwcParameters.getValues(mActiveMwcVersion, Mwc2_1.MSP_SET_RC_TUNING)));
                        mBTService.write(mMwcProtocol.setBOX(mActiveMwcVersion, mMwcParameters.getValues(mActiveMwcVersion, Mwc2_1.MSP_SET_BOX)));
                        mBTService.write(mMwcProtocol.setMISC(mActiveMwcVersion, mMwcParameters.getValues(mActiveMwcVersion, Mwc2_1.MSP_SET_MISC)));
                        // Save settings to EEPROM
                        mBTService.write(mMwcProtocol.eepromWRITE(mActiveMwcVersion));
                    }
                    showMessage("Writing parameters");
                    break;

                /* Button Monitor ON/OFF pressed */
                case R.id.tglMonitor:
                    if (D)  Log.i(TAG, "ToggleMonitor pressed.");
                    ToggleButton toggleMonitor = (ToggleButton) v;
                    if (toggleMonitor.isChecked()) {
                        if (D)  Log.i(TAG, "ToggleMonitor is enabled.");
                        mMonitorState = true;
                        startAutoUpdate(MwcConstants.autoUpdateDelay);
                        //mBTService.write(mMwcProtocol.readSensorRaw(mActiveMwcVersion));
                    } else {
                        if (D) Log.i(TAG, "ToggleMonitor is disabled.");
                        mMonitorState = false;
                        stopAutoUpdate();
                    }
                    break;
            }
        } else {
            // Check if there is a selected device and auto-connect
            if (btMacAddress != null) {
                setupBTService();
            } else {
                showMessage("No device selected!");
                if (v.getId() == R.id.tglMonitor) {
                    ToggleButton toggleMonitor = (ToggleButton) v;
                    toggleMonitor.setChecked(false);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (D)
            Log.e(TAG, "--- ON START ---");
        /*
         * // If BT is not on, request that it be enabled. // setupService()
         * will then be called during onActivityResult if
         * (!mBluetoothAdapter.isEnabled()) { Intent enableIntent = new
         * Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
         * startActivityForResult(enableIntent, REQUEST_ENABLE_BT); // Otherwise
         * start the conf tool } else { if (mBTService == null)
         * setupBTService(); }
         */
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if (D)
            Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity
        // returns.
        if (mBTService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't
            // started already
            if (mBTService.getState() == BTService.STATE_NONE) {
                // Start the Bluetooth chat services
                mBTService.start();
            }
        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if (D)
            Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if (D)
            Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Stop the BTservice service
        //if (mBTService != null) mBTService.stop();
        if (D) Log.e(TAG, "--- ON DESTROY ---");

        unbindDrawables(findViewById(R.id.layout_parameters_main));
        System.gc();
    }

    // The Handler that gets information back from the BTService
    private final Handler btHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:

                    if (D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);

                    switch (msg.arg1) {
                        case BTService.STATE_CONNECTED:
                            if (D) Log.e(TAG, "STATE_CONNECTED received");

                            mBTState = BTService.STATE_CONNECTED;
                            mStatusBar.setText(getString(R.string.connected_to)
                                    + mBluetoothAdapter.getRemoteDevice(btMacAddress)
                                    .getName());

                            // Save to preferences the MAC address of the connected
                            // device to use later to populate on the StatusBar onCreate()
                            Editor e = getPreferences(Context.MODE_PRIVATE).edit();
                            // Save MAC address
                            e.putString("lastConnectedMAC", mBluetoothAdapter
                                    .getRemoteDevice(btMacAddress).getAddress());
                            // Save Name
                            e.putString("lastConnectedName", mBluetoothAdapter
                                    .getRemoteDevice(btMacAddress).getName());
                            e.commit();

                            // Toggle READ/Write buttons state
                            //mMwcUIHelper.toggleButtonsState(true);

                            // Perform a read to fill the UI
                            // If the version is > 2.2 we need to get the box names first
                            // to populate the UI.
                            if(mActiveMwcVersion >= MwcConstants.MWC_VERSION_22) {
                                mBTService.write(((Mwc2_2) mMwcProtocol)
                                        .readBOXNAMES(mActiveMwcVersion));
                            }
                            mBTService.write(mMwcProtocol.readPARAMS(mActiveMwcVersion));
                            break;

                        case BTService.STATE_CONNECTING:
                            if (D) Log.e(TAG, "STATE_CONNECTING received");

                            mBTState = BTService.STATE_CONNECTING;
                            mStatusBar.setText(getString(R.string.connecting_to)
                                    + mBluetoothAdapter.getRemoteDevice(btMacAddress)
                                    .getName());

                            // Toggle READ/Write buttons state
                            //mMwcUIHelper.toggleButtonsState(false);
                            break;

                        case BTService.STATE_LISTEN:
                            if (D) Log.e(TAG, "STATE_LISTEN received");

                            mBTState = BTService.STATE_LISTEN;
                            mStatusBar.setText(getString(R.string.waiting_for)
                                    + mBluetoothAdapter.getRemoteDevice(btMacAddress)
                                    .getName());

                            // Toggle READ/Write buttons state
                            //mMwcUIHelper.toggleButtonsState(false);
                            break;

                        case BTService.STATE_NONE:
                            if (D) Log.e(TAG, "STATE_NONE received");

                            mBTState = BTService.STATE_NONE;
                            mStatusBar.setText(getString(R.string.not_connected));

                            // Toggle READ/Write buttons state
                            //mMwcUIHelper.toggleButtonsState(false);
                            break;

                    }
                    break;

                case MESSAGE_WRITE:
                    if (D) Log.d(TAG, "MESSAGE_WRITE received");
                    break;

                case MESSAGE_READ:
                    if (D) Log.d(TAG, "MESSAGE_READ received: creating DataProcessor instance.");
                    if (D) Log.d(TAG, "Processing with DataProcessor instance: " + mDataProcessor.hashCode());

                    if (D) {
                        Log.i(TAG,
                                "Received " + ((byte[]) msg.obj).length
                                + " sized buffer: "
                                + DataTools.getHex((byte[]) msg.obj));
                    }

                    if (mDataProcessor.setData((byte[]) msg.obj, msg.arg1)) {
                        if(mMonitorState) {
                            mMwcUIHelper.updateChannelsBars(mDataProcessor
                                    .getChannels(mActiveMwcVersion), mActiveMwcVersion);
                            mMwcGraph.updateSensorGraph(mDataProcessor.getSensors(mActiveMwcVersion));
                            mMwcUIHelper.updateSensorState(mDataProcessor.getSensorState(mActiveMwcVersion));

                            if (readSent) {
                                Log.d(TAG, "readSent detected!");
                                mMwcParameters.setParamsData(mDataProcessor.getParameters(mActiveMwcVersion));
                                mMwcUIHelper.setUIParams(mMwcParameters, mActiveMwcVersion);
                                readSent = false;
                            }

                        } else {
                            mMwcParameters.setParamsData(mDataProcessor.getParameters(mActiveMwcVersion));
                            if (D) Log.d(TAG, "Going to update the UI");
                            mMwcUIHelper.setUIParams(mMwcParameters, mActiveMwcVersion);
                            //mDataProcessor.finalize();
                            mMwcUIHelper.updateBoxNames(mDataProcessor.getBoxNames());
                        }
                    } else {
                        Log.e(TAG, "Data processing failed!");
                    }
                    break;

                case MESSAGE_TOAST:
                    if (D) Log.d(TAG, "MESSAGE_TOAST received: "
                            + msg.getData().getString(TOAST));
                    if (msg.getData().getString(TOAST) != null) {
                        showMessage(msg.getData().getString(TOAST));
                    }
                    break;
            }
        }
    };

    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
        .show();
    }

    private void setupBTService() {
        Log.d(TAG, "setupBTService() - initializing BTService");

        // Get the local Bluetooth adapter
        // If the adaptor is null, then Bluetooth is not supported
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            if (D) Log.e(TAG, "No local BT device found. ");
            return;
        }

        // Bluetooth is present but not enable ask the user to enable it
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            if (D) Log.e(TAG, "BT is not enable ask to enable.");
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            return;
        }

        // Check if the BTservice is already initialized if not initialize it
        if (mBTService == null) {
            if (D)
                Log.e(TAG, "BTService not initialized, initializing now");

            // Initialize and start the BTService to perform bluetooth
            // connections
            mBTService = new BTService(getApplicationContext(), btHandler);
            mBTService.start();
        }
        // Obtain the MAC address of the bluetooth device we're going to connect
        // to and connect to it
        BluetoothDevice btDevice = mBluetoothAdapter.getRemoteDevice(btMacAddress);
        mBTService.connect(btDevice);
    }

    private void startAutoUpdate(final long delay){
        //Debug.startMethodTracing("traceFile");
        if (mTimer == null) {
            mTimer = new Timer();
        }
        if (mTimerTask == null) {
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    Log.i(TAG, "Starting auto update task.");
                    int count = 0;
                    do {
                        try {
                            if (D) Log.i(TAG, "Running auto update task...");
                            if (mActiveMwcVersion <= MwcConstants.MWC_VERSION_20) {
                                mBTService.write(mMwcProtocol.readSensorRaw(mActiveMwcVersion));
                            } else {
                                mBTService.write(mMwcProtocol.readSensorRaw(mActiveMwcVersion));
                                mBTService.write(mMwcProtocol.readChannels(mActiveMwcVersion));
                                if (count == 15) {
                                    mBTService.write(mMwcProtocol.readState(mActiveMwcVersion));
                                    count = 0;
                                    //Debug.stopMethodTracing();
                                }
                            }
                            count++;
                            Thread.sleep(delay);
                        } catch (InterruptedException e) {
                        }

                    } while (mMonitorState);
                }
            };
        }
        if(mTimer != null && mTimerTask != null ) {
            mTimer.schedule(mTimerTask, delay, frequency);
        }
    }

    /**
     * Stops the runnable thread performing the auto UI update
     */
    public void stopAutoUpdate() {
        Log.i(TAG, "Stopping auto update task...");
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask = null;
        }
        mMonitorState = false;
    }

    public void switchClicked(View v) {
        if(D) Log.d(TAG, "The view was clicked:" + v.getId());

        CheckBox checkBoxClicked = (CheckBox) this.findViewById(v.getId());
        // Get state in integer format
        //int state = checkBoxClicked.isChecked() ? 1 : 0;
        boolean stateB = checkBoxClicked.isChecked() ? true : false;

        // Let's get the Resource name from the View ID
        String name = getResources().getResourceName(v.getId());
        // Splitting the Resource name to grab only the android:id
        String id = name.split("/")[1];
        // Split
        String[] idTypeArray = id.split("_");

        if (D) {
            for (int i = 0; i < idTypeArray.length; i++) {
                Log.d(TAG, "type: " + idTypeArray[i] + " State: " + stateB);
            }
        }

        /*
        mMwcParameters.setBoxActivation(
                MwcConstants.MWC_21_BOX_LIST.get(idTypeArray[0]),
                MwcConstants.MWC_BOX_CHANNEL.get(idTypeArray[1]),
                MwcConstants.MWC_BOX_POSITION.get(idTypeArray[2]),
                stateB);
        */
        mMwcParameters.setBoxActivation(
                mMwccontantValues.getBoxIndex(idTypeArray[0]),
                mMwccontantValues.getChannelId(idTypeArray[1]),
                mMwccontantValues.getBoxPosition(idTypeArray[2]),
                stateB);
        /*
        if (D) Log.d(TAG, "BOX: " + MwcConstants.MWC_21_BOX_LIST.get(idTypeArray[0])
                + " Channel: " + MwcConstants.MWC_BOX_CHANNEL.get(idTypeArray[1])
                + " Position: " + MwcConstants.MWC_BOX_POSITION.get(idTypeArray[2]));
         */

        // Clean it
        checkBoxClicked = null;
        if (D) Log.d(TAG, "Control Name: " + id);
    }

    // Return the currently active version of Mwc
    public static int getActiveVersion() {
        return mActiveMwcVersion;
    }

    // @Override
    @Override
    public boolean onTouch(View view, MotionEvent event) {

        long downTime = 0;
        int currentChildViewIndex = mViewFlipper.getDisplayedChild();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                downXValue = event.getX();
                downTime = event.getEventTime();
                break;
            }

            case MotionEvent.ACTION_UP: {
                float currentX = event.getX();
                long currentTime = event.getEventTime();
                float difference = Math.abs(downXValue - currentX);
                long time = currentTime - downTime;

                if (D) Log.d(TAG,
                        "Distance: " + difference + "px Time: " + time
                        + "ms CurrentViewIndex: "
                        + mViewFlipper.getDisplayedChild());

                if ((downXValue < currentX) && (time > 150) && (difference > 150)) {
                    if (mViewFlipper.getDisplayedChild() > 0) {
                        // Set the animation
                        mViewFlipper.setInAnimation(mMwcUIHelper.inFromLeftAnimation());
                        mViewFlipper.setOutAnimation(mMwcUIHelper.outToRightAnimation());
                        // Flip!
                        mViewFlipper.showPrevious();
                    }
                }

                if ((downXValue > currentX) && (time > 150) && (difference > 150)) {
                    if (mViewFlipper.getDisplayedChild() < MwcConstants.VIEW_COUNT) {
                        // Set the animation
                        mViewFlipper.setInAnimation(mMwcUIHelper.inFromRightAnimation());
                        mViewFlipper.setOutAnimation(mMwcUIHelper.outToLeftAnimation());
                        // Flip!
                        mViewFlipper.showNext();
                    }
                }
                break;
            }

        }
        // Update the flipperDots style to reflect the current active pane
        mMwcUIHelper.dotViewChange(mViewFlipper.getDisplayedChild(), currentChildViewIndex, mMonitorState);
        return true;
    }

    // onClick action performed by the flipperDotText
    public void changeViewFlipper(View view) {
        int currentChildViewIndex = mViewFlipper.getDisplayedChild();

        if (mViewFlipper.getDisplayedChild() == 0
                && (view.getId() == findViewById(R.id.flipperDotRightText)
                .getId())) {
            mViewFlipper.setInAnimation(mMwcUIHelper.inFromRightAnimation());
            mViewFlipper.setOutAnimation(mMwcUIHelper.outToLeftAnimation());
            // Flip!
            mViewFlipper.showNext();
        } else if (mViewFlipper.getDisplayedChild() == 1
                && (view.getId() == findViewById(R.id.flipperDotLeftText)
                .getId())) {
            mViewFlipper.setInAnimation(mMwcUIHelper.inFromLeftAnimation());
            mViewFlipper.setOutAnimation(mMwcUIHelper.outToRightAnimation());
            // Flip!
            mViewFlipper.showPrevious();
        }

        // Update the flipperDots style to reflect the current active pane
        mMwcUIHelper.dotViewChange(mViewFlipper.getDisplayedChild(), currentChildViewIndex, mMonitorState);
    }

    public void changeValue(View v) {

        if (useSliderDialog) {
            final RCEditText rcEditView = (RCEditText) v;
            if (D) Log.d(TAG, "changeValue called from view of type: " + rcEditView.getsFormatType());
            ValueChangeDialog changeDialog = new ValueChangeDialog(
                    AndMultiWiiConf.this,
                    rcEditView,
                    getParamName(rcEditView)
                    );
            changeDialog.setDialogResult(new OnMyDialogResult(){
                @Override
                public void finish(String result){
                    rcEditView.setText(result);
                }
            });
            changeDialog.show();
        } else {
            Log.i(TAG, "Slider is disabled!");
        }
    }

    /**
     * Return the name of the View that was passed.
     * It can be used to obtain the name of a editext param.
     * @param v View to obtain the name from
     * @return String containing the formated name
     */
    public String getParamName(View v) {
        String formatedName = "n/a";

        // Get View name and extract unwanted string
        String viewName = getResources().getResourceName(v.getId()).split("/")[1];
        if(viewName.startsWith("val")) {
            // Get the string ignoring the "val"
            viewName = viewName.substring(3);
            // Swap the _ by spaces to properlly format it
            formatedName = viewName.replace("_", " ");
        }
        return formatedName;
    }

    public boolean getUseSliderState() {
        return useSliderDialog;
    }

    /*
     * This method explores the passed view recursively and removes the callbacks on all
     * the background drawables and also childs of every viewgroup.
     */
    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    private void setProtocol(int mActiveMwcVersion) {
        // Load the MwcProtocol class based on the active version
        String protocolClassName = getProtocolVersion(mActiveMwcVersion);

        try {
            mMwcProtocol = (MwcProtocol) Class.forName(protocolClassName).newInstance();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Set constant values based on version


    }

}