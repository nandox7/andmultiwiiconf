package net.xrotor.andmultiwiiconf;

public class DataTools {

    public static final byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }

	
	public static final int byteArrayToInt(byte[] b) {
		 	return (b[3] << 24)
		 			+ ((b[2] & 0xFF) << 16)
		 			+ ((b[1] & 0xFF) << 8)
		 			+ (b[0] & 0xFF);
	}

	public static String getHex( byte [] raw ) {
		final String HEXES = "0123456789ABCDEF";
		
		if ( raw == null ) {
	      return null;
	    }
	    final StringBuilder hex = new StringBuilder( 3 * raw.length );
	    for ( final byte b : raw ) {
	      hex.append(HEXES.charAt((b & 0xF0) >> 4))
	         .append(HEXES.charAt((b & 0x0F)));
	      hex.append(" ");
	    }
	    return hex.toString();
	}

	public static String getHex( byte [] raw, int count ) {
		final String HEXES = "0123456789ABCDEF";
		int counter = 1;
		
		if ( raw == null ) {
	      return null;
	    }
	    final StringBuilder hex = new StringBuilder( 3 * raw.length + 2 );
	    for ( final byte b : raw ) {
	      hex.append(HEXES.charAt((b & 0xF0) >> 4))
	         .append(HEXES.charAt((b & 0x0F)));
	      if (counter != count) { 
	    	  hex.append(" ");
	      } else {
	    	  hex.append(" | ");
	      }
	      counter = counter + 1;
	    }
	    return hex.toString();
	}

	public static String getHex( byte code) {
	    final String HEXES = "0123456789ABCDEF";

	    if ( code == 0 ) {
		return null;
	    }
	    final StringBuilder hex = new StringBuilder( 2 );
	    hex.append(HEXES.charAt((code & 0xF0) >> 4))
	    .append(HEXES.charAt((code & 0x0F)));

	    return hex.toString();
	}

}
