// Copyright 2012 Google Inc. All Rights Reserved.

package net.xrotor.andmultiwiiconf.protocol;

import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author nando@xrotor.net (Fernando Morais) *
 */
public class Mwc2_2 implements MwcProtocol{

    // Debugging
    private static final String TAG = "AndMultiWiiConf.ProtocolNew";
    private static final boolean D = false;

    public static final int BOX_LEVEL = 1;

    // Temp Byte Data Buffer for incoming data
    //ByteBuffer mInByteBuffer = ByteBuffer.allocate(180);

    // Byte array with that to process
    //byte[] dataBuffer = new byte[256];

    String tempString = "";

    int bufPointer; // Buffer frame pointer

    // Mwc 2.1+ Serial Protocol
    int version;

    // MWC data params vars
    int multiType;  // 1 for tricopter, 2 for quad+, 3 for quadX

    // Mwc 2.2 Box enum list
    public static enum MWC_22_BOX_NAME_INDEX {
        Level,
        Baro,
        Mag,
        CamStab,
        CamTrig,
        Arm,
        GpsHome,
        GpsHold,
        PassThru,
        HeadFree,
        Beeper,
        LedMax,
        LLights,
        HeadAdj
    }

    private final String MSP_HEADER = "$M<";

    public static final int
      MSP_IDENT                =100,  // multitype + multiwii version + protocol version + capability variable
      MSP_STATUS               =101,  // cycletime & errors_count & sensor present & box activation
      MSP_RAW_IMU              =102,  // 9 DOF
      MSP_SERVO                =103,  // 8 servos
      MSP_MOTOR                =104,  // 8 motors
      MSP_RC                   =105,  // 8 rc channels
      MSP_RAW_GPS              =106,  // fix, numsat, lat, lon, alt, speed
      MSP_COMP_GPS             =107,  // distance home, direction home
      MSP_ATTITUDE             =108,  // 2 angles, 1 heading
      MSP_ALTITUDE             =109,  // 1 altitude
      MSP_BAT                  =110,  // vbat, powermetersum
      MSP_RC_TUNING            =111,  // rc rate, rc expo,, rollpitch rate, yaw rate, dyn throttle PID
      MSP_PID                  =112,  //
      MSP_BOX                  =113,  // out - up to 16 checkbox (11 are used)
      MSP_MISC                 =114,  // powermeter trig + 8 free for future use
      MSP_MOTOR_PINS           =115,  // which pins are in use for motors & servos, for GUI
      MSP_BOXNAMES             =116,  // out - the aux switch names
      MSP_PIDNAMES             =117,  // out - the PID names
      MSP_WP                   =118,  // out - get a WP, WP# is in the payload, returns (WP#, lat, lon, alt, flags) WP#0-home, WP#16-poshold

      MSP_SET_RAW_RC           =200,
      MSP_SET_RAW_GPS          =201,
      MSP_SET_PID              =202,
      MSP_SET_BOX              =203,
      MSP_SET_RC_TUNING        =204,
      MSP_ACC_CALIBRATION      =205,
      MSP_MAG_CALIBRATION      =206,
      MSP_SET_MISC             =207,

      MSP_RESET_CONF           =208,

      MSP_EEPROM_WRITE         =250, // in - no param

      MSP_DEBUG                =254; // out - debug1,debug2,debug3,debug4

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.Protocol#getVersion()
     */
    @Override
    public List<String> getSupportedVersion() {
        List<String> supportedVersions = new ArrayList<String>();
        supportedVersions.add ("2.2");
        return supportedVersions;
    }

    // Generate msp without payload
    private List<Byte> requestMSP(int msp) {
        if (D) Log.d(TAG, "Single MSP detected.");
        return  requestMSP( msp, null);
    }

    // Generate multiple msp without payload
    private List<Byte> requestMSP (int[] msps) {
        if (D) Log.d(TAG, "Multiple MSP detected without payload.");
        List<Byte> s = new LinkedList<Byte>();
        for (int m : msps) {
            s.addAll(requestMSP(m, null));
        }
        return s;
    }

    // Generate msp with payload
    private List<Byte> requestMSP (int msp, byte[] payload) {
      if (D) Log.d(TAG, "Generate MSP with payload.");
      if(msp < 0) {
       return null;
      }

      List<Byte> bf = new LinkedList<Byte>();
      for (byte c : MSP_HEADER.getBytes()) {
        bf.add( c );
      }

      byte checksum=0;
      byte pl_size = (byte) ((payload != null ? (payload.length) : 0)&0xFF);
      bf.add(pl_size);
      checksum ^= (pl_size&0xFF);

      bf.add((byte)(msp & 0xFF));
      checksum ^= (msp&0xFF);

      if (payload != null) {
        for (byte c :payload){
          bf.add((byte) (c&0xFF));
          checksum ^= (c&0xFF);
        }
      }
      bf.add(checksum);
      return (bf);
    }

    /**
     * Generates the MSP message for a list of MSP's
     * @param msp List<Byte>
     * @return byte[]
     */
    private byte[] sendRequestMSP(List<Byte> msp) {
      byte[] arr = new byte[msp.size()];
      int i = 0;
      for (byte b: msp) {
        arr[i++] = b;
      }
      return arr;
    }

    /**
     * Public Methods to Generate the several message types
     */
    @Override
    public byte[] calibrateACC(int mActiveMwcVersion) {
        List<Byte> msg = requestMSP(MSP_ACC_CALIBRATION);
        return sendRequestMSP(msg);
    }

    /*
     * (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.MwcProtocol#calibrateMAG(int)
     */
    @Override
    public byte[] calibrateMAG(int mActiveMwcVersion) {
        List<Byte> msg = requestMSP(MSP_MAG_CALIBRATION);
        return sendRequestMSP(msg);
    }

    /*
     * (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.MwcProtocol#readPARAMS(int)
     */
    @Override
    public byte[] readPARAMS(int mActiveMwcVersion) {
        int[] requests = {MSP_PID, MSP_RC_TUNING, MSP_MISC, MSP_BOX};
        List<Byte> msg = requestMSP(requests);
        return sendRequestMSP(msg);
    }

    // New method for Mwc 2.2 that retrieves the existing box names
    public byte[] readBOXNAMES(int mActiveMwcVersion) {
      List<Byte> msg = requestMSP(MSP_BOXNAMES);
      return sendRequestMSP(msg);
    }

    @Override
    public byte[] readSensorRaw(int mActiveMwcVersion) {
      int[] requests = {MSP_RAW_IMU};
      List<Byte> msg = requestMSP(requests);
      return sendRequestMSP(msg);
    }

    @Override
    public byte[] readChannels(int mActiveMwcVersion) {
        int[] requests = {MSP_RC, MSP_MOTOR};
        List<Byte> msg = requestMSP(requests);
        return sendRequestMSP(msg);
    }

    @Override
    public byte[] readState(int mActiveMwcVersion) {
        List<Byte> msg = requestMSP(MSP_STATUS);
        return sendRequestMSP(msg);
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#readGPS(int)
     */
    @Override
    public byte[] readGPS(int mActiveMwcVersion) {
      List<Byte> msg = requestMSP(MSP_SET_RAW_GPS);
      return sendRequestMSP(msg);
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#readGPS(int)
     */
    @Override
    public byte[] resetCONF(int mActiveMwcVersion) {
      List<Byte> msg = requestMSP(MSP_RESET_CONF);
      return sendRequestMSP(msg);
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#readGPS(int)
     */
    @Override
    public byte[] setRCTUNING(int mActiveMwcVersion, byte[] payload) {
      List<Byte> msg = requestMSP(MSP_SET_RC_TUNING, payload);
      return sendRequestMSP(msg);
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#readGPS(int)
     */
    @Override
    public byte[] setPID(int mActiveMwcVersion, byte[] payload) {
      List<Byte> msg = requestMSP(MSP_SET_PID, payload);
      return sendRequestMSP(msg);
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#readGPS(int)
     */
    @Override
    public byte[] setBOX(int mActiveMwcVersion, byte[] payload) {
      List<Byte> msg = requestMSP(MSP_SET_BOX, payload);
      return sendRequestMSP(msg);
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#readGPS(int)
     */
    @Override
    public byte[] setMISC(int mActiveMwcVersion, byte[] payload) {
      List<Byte> msg = requestMSP(MSP_SET_MISC, payload);
      return sendRequestMSP(msg);
    }


    @Override
    public byte[] eepromWRITE(int mActiveMwcVersion) {
        List<Byte> msg = requestMSP(MSP_EEPROM_WRITE);
        return sendRequestMSP(msg);
    }

}
