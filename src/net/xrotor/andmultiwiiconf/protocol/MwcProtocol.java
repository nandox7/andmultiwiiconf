// Copyright 2012 Google Inc. All Rights Reserved.

package net.xrotor.andmultiwiiconf.protocol;

import java.util.List;

/**
 * @author nando@xrotor.net (Fernando Morais)
 * 
 */
public abstract interface MwcProtocol {

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public List<String> getSupportedVersion();

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] calibrateACC(int mActiveMwcVersion);

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] calibrateMAG(int mActiveMwcVersion);

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] readPARAMS(int mActiveMwcVersion);

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] readSensorRaw(int mActiveMwcVersion);

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] readChannels(int mActiveMwcVersion);

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] readState(int mActiveMwcVersion);

  
  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] readGPS(int mActiveMwcVersion);

  /**
   * @param mActiveMwcVersion
   * @return
   */
  public byte[] resetCONF(int mActiveMwcVersion);

  /**
   * @param mActiveMwcVersion
   * @param paramstoArray
   * @return
   */
  public byte[] setPID(int mActiveMwcVersion, byte[] payload);

  
  /**
   * @param mActiveMwcVersion
   * @param paramstoArray
   * @return
   */
  public byte[] setRCTUNING(int mActiveMwcVersion, byte[] paramstoArray);

  /**
   * 
   * @param mActiveMwcVersion
   * @param values
   * @return
   */
  public byte[] setBOX(int mActiveMwcVersion, byte[] values);

  public byte[] eepromWRITE(int mActiveMwcVersion);

  public byte[] setMISC(int mActiveMwcVersion, byte[] values);
  
}
