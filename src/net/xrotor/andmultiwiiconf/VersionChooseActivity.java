package net.xrotor.andmultiwiiconf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.Arrays;

public class VersionChooseActivity extends Activity implements 
		RadioGroup.OnCheckedChangeListener{
	
    // Debugging
    private static final String TAG = "AndMultiWiiConf.VersionChooseActivity";
    private static final boolean D = false;
    
    // Return Intent extra
	protected static final String SELECTED_VERSION = "selected_version";
	
    int mActiveMwcVersion = 0;
    	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		if (D) Log.e(TAG, "--- ON CREATE --- ");
	
        // Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.version_chooser);
        
        // Get the Active version passed on the intent extras
        mActiveMwcVersion = getIntent().getExtras().getInt("ACTIVE_VERSION", 0);
        
        // Get saved from preferences the active/default Mwc version
        //int activeMwcVersion = getPreferences(Context.MODE_PRIVATE).getInt("DefaultMwcVersion", MwcConstants.MWC_DEFAULT_VERSION);
        //if (D) Log.d(TAG, "Preferences read active/default MwcVersion: " + mActiveMwcVersion );
        RadioGroup mRadioGroup = (RadioGroup) findViewById(R.id.choose_version_radiogroup);
        mRadioGroup.setOnCheckedChangeListener(this);
        /*
        // Construct the radiobuttons based on the supports versions
        for(int version: MwcConstants.MWC_VERSIONS_LIST) {
        	String versionText = String.valueOf((float) version / 10);
        	RadioButton radioButton = new RadioButton(getBaseContext());
            radioButton.setText(versionText);
            mRadioGroup.addView(radioButton);
            Log.d(TAG, "Version: " + mActiveMwcVersion);
            if (version == mActiveMwcVersion) radioButton.setChecked(true); // Check if it's the active/default version
        }
        */
        
        for(int versionIndex = 0; versionIndex < MwcConstants.MWC_VERSIONS_LIST.length; ++versionIndex ) {
        	if (D) Log.d(TAG, "Version: " + mActiveMwcVersion + " " + versionIndex);
        	String versionName = MwcConstants.MWC_VERSIONS_LIST_NAME[versionIndex];
        	RadioButton radioButton = new RadioButton(getBaseContext());
            radioButton.setText(versionName);
            mRadioGroup.addView(radioButton);
            // Check if it's the active/default version            
            if (MwcConstants.MWC_VERSIONS_LIST[versionIndex] == mActiveMwcVersion) radioButton.setChecked(true);
        }
        
        
        // Initialize the button to close popup
        Button closeButton = (Button) findViewById(R.id.choose_version_close);
        closeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create the result Intent and include the MAC address
                Intent intent = new Intent();
                // Pass full file path to the Intent extras
                intent.putExtra(SELECTED_VERSION, mActiveMwcVersion);

                // Set result and finish this Activity
                setResult(AndMultiWiiConf.RESULT_VERSION_CHANGED, intent);
                finish();
            }
        });
        
        // Set result CANCELED incase the user backs out
        setResult(Activity.RESULT_CANCELED);
        
    }

	@Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton mRadioButton = (RadioButton) findViewById(checkedId);
        if (D) {Log.d(TAG, "Button selected " + mRadioButton.getText() ); }
    	// Save selected MultiWii version0
        //mActiveMwcVersion = (int) (Float.valueOf((String) mRadioButton.getText()) * 10);
        
        mActiveMwcVersion = MwcConstants.MWC_VERSIONS_LIST[Arrays.binarySearch(MwcConstants.MWC_VERSIONS_LIST_NAME, mRadioButton.getText())];
    	//saveDefaultMwcVersion( mwcVersion );
	}
    
}
