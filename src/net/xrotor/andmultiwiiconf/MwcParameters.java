package net.xrotor.andmultiwiiconf;

import android.util.Log;

import net.xrotor.andmultiwiiconf.protocol.Mwc2_1;
import net.xrotor.andmultiwiiconf.protocol.Mwc2_2;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class MwcParameters {
    // Debugging
    private static final String TAG = "AndMultiWiiConf.MwCParameters";
    private static final boolean D = true;

    // Private Settings vars
    private int byteP_ROLL,
    byteI_ROLL,
    byteD_ROLL,
    byteP_PITCH,
    byteI_PITCH,
    byteD_PITCH,
    byteP_YAW,
    byteI_YAW,
    byteD_YAW,
    byteP_ALT,
    byteI_ALT,
    byteD_ALT,
    byteP_VEL,
    byteI_VEL,
    byteD_VEL,
    byteP_GPS, // v2.0
    byteI_GPS, // v2.0
    byteD_GPS, // v2.0
    byteP_POS, // v2.1
    byteI_POS, // v2.1
    byteD_POS, // v2.1 unused...
    byteP_POSR, // v2.1
    byteI_POSR, // v2.1
    byteD_POSR, // v2.1
    byteP_NAVR, // v2.1
    byteI_NAVR, // v2.1
    byteD_NAVR, // v2.1
    byteP_ALARM, // v2.1
    byteTHR_MID,
    byteTHR_EXPO,
    byteP_LEVEL,
    byteI_LEVEL,
    byteD_LEVEL,
    byteP_MAG,
    byteRC_RATE,
    byteRC_EXPO,
    byteRollPitchRate,
    byteYawRate,
    byteDynThrPID;
    //intPowerTrigger;

    private int[] activationState;

    // Public constants
    //public static final int CHAN_AUX1 = 0;
    //public static final int CHAN_AUX2 = 0;

    public static final int POS_LOW  = 0;
    public static final int POS_MID  = 1;
    public static final int POS_HIGH = 2;

    //public static enum SwitchPosition { POS_LOW , POS_MID, POS_HIGH }
    //public static enum ChannelName { CHAN_AUX1, CHAN_AUX2, CHAN_AUX3, CHAN_AUX4 }

    public Map<String, Integer> boxNameMap = new HashMap<String, Integer>();

    //public Map<String, Integer> swithPositionMap = new HashMap<String, Integer>();
    //public Map<String, Integer> channelNameMap = new HashMap<String, Integer>();


    MwcParameters() {
/*
        for(MwcConstants.MWC_21_BOX_NAME_INDEX bn : MwcConstants.MWC_21_BOX_NAME_INDEX.values()) {
            boxNameMap.put(bn.toString(), bn.ordinal());
        }

        for(ChannelName c : ChannelName.values()) {
            channelNameMap.put(c.toString(), c.ordinal());
        }

        for(SwitchPosition s : SwitchPosition.values()) {
            swithPositionMap.put(s.toString(), s.ordinal());
        }
*/
        // Defines the activation array based on the size of the box of the version
        activationState = new int[40];

    }

    /**
     * @return the boxNameMap
     */
    public Map<String, Integer> getBoxNameMap() {
        return boxNameMap;
    }

    /**
     * @param boxNameMap the boxNameMap to set
     */
    public void setBoxNameMap(Map<String, Integer> boxNameMap) {
        this.boxNameMap = boxNameMap;
    }

    /**
     * @return the byteP_ROLL
     */
    public double getByteP_ROLL() {
        return byteP_ROLL / 10.0;
    }

    /**
     * @return the byteI_ROLL
     */
    public double getByteI_ROLL() {
        return byteI_ROLL / 1000.0;
    }

    /**
     * @return the byteD_ROLL
     */
    public double getByteD_ROLL() {
        return byteD_ROLL;
    }

    /**
     * @return the byteP_PITCH
     */
    public double getByteP_PITCH() {
        return byteP_PITCH / 10.0;
    }

    /**
     * @return the byteI_PITCH
     */
    public double getByteI_PITCH() {
        return byteI_PITCH / 1000.0;
    }

    /**
     * @return the byteD_PITCH
     */
    public double getByteD_PITCH() {
        return byteD_PITCH;
    }

    /**
     * @return the byteP_YAW
     */
    public double getByteP_YAW() {
        return byteP_YAW / 10.0;
    }

    /**
     * @return the byteI_YAW
     */
    public double getByteI_YAW() {
        return byteI_YAW / 1000.0;
    }

    /**
     * @return the byteD_YAW
     */
    public double getByteD_YAW() {
        return byteD_YAW;
    }

    /**
     * @return the byteP_ALT
     */
    public double getByteP_ALT() {
        return byteP_ALT / 10.0;
    }

    /**
     * @return the byteI_ALT
     */
    public double getByteI_ALT() {
        return byteI_ALT / 1000.0;
    }

    /**
     * @return the byteD_ALT
     */
    public double getByteD_ALT() {
        return byteD_ALT;
    }

    /**
     * @return the byteP_VEL
     */
    public double getByteP_VEL() {
        return byteP_VEL / 10.0;
    }

    /**
     * @return the byteI_VEL
     */
    public double getByteI_VEL() {
        return byteI_VEL / 1000.0;
    }

    /**
     * @return the byteD_VEL
     */
    public double getByteD_VEL() {
        return byteD_VEL;
    }

    /**
     * @return the byteP_GPS
     */
    public double getByteP_GPS() {
        return byteP_GPS / 10.0;
    }

    /**
     * @return the byteI_GPS
     */
    public double getByteI_GPS() {
        return byteI_GPS / 1000.0;
    }

    /**
     * @return the byteD_GPS
     */
    public double getByteD_GPS() {
        return byteD_GPS;
    }

    /**
     * @return the byteP_LEVEL
     */
    public double getByteP_LEVEL() {
        return byteP_LEVEL / 10.0;
    }

    /**
     * @return the byteI_LEVEL
     */
    public double getByteI_LEVEL() {
        return byteI_LEVEL / 1000.0;
    }

    /**
     * @return the byteD_LEVEL
     */
    public double getByteD_LEVEL() {
        return byteD_LEVEL;
    }

    /**
     * @return the byteRC_RATE
     */
    public double getRC_RATE() {
        if(AndMultiWiiConf.getActiveVersion() <= 3) {
            return byteRC_RATE / 50.0;
        } else {
            return byteRC_RATE / 100.0;
        }

    }

    /**
     * @return the byteRC_EXPO
     */
    public double getRC_EXPO() {
        return byteRC_EXPO / 100.0;
    }

    /**
     * @return the byteRollPitchRate
     */
    public double getRollPitchRate() {
        return byteRollPitchRate / 100.0;
    }

    /**
     * @return the byteYawRate
     */
    public double getYawRate() {
        return byteYawRate / 100.0;
    }

    /**
     * @return the byteDynThrPID
     */
    public double getDynThrPID() {
        return byteDynThrPID / 100.0;
    }

    /**
     * @return the getByteP_MAG
     */
    public double getByteP_MAG() {
        return byteP_MAG / 10.0;
    }

    /**
     * @return the intPowerTrigger
     */
    //public int getIntPowerTrigger() {
    //    return intPowerTrigger;
    //}

    /**
     * @return the byteP_POS
     */
    public double getByteP_POS() {
        return byteP_POS / 100.0;
    }

    /**
     * @return the byteI_POS
     */
    public double getByteI_POS() {
        return byteI_POS / 100.0;
    }

    /**
     * @return the byteD_POS
     */
    public double getByteD_POS() {
        return byteD_POS / 100.0;
    }

    /**
     * @return the byteP_POSR
     */
    public double getByteP_POSR() {
        return byteP_POSR / 10.0;
    }

    /**
     * @return the byteI_POSR
     */
    public double getByteI_POSR() {
        return byteI_POSR / 100.0;
    }

    /**
     * @return the byteD_POSR
     */
    public double getByteD_POSR() {
        return byteD_POSR / 1000.0;
    }

    /**
     * @return the byteP_NAVR
     */
    public double getByteP_NAVR() {
        return byteP_NAVR / 10.0;
    }

    /**
     * @return the byteI_NAVR
     */
    public double getByteI_NAVR() {
        return byteI_NAVR / 100.0;
    }

    /**
     * @return the byteD_NAVR
     */
    public double getByteD_NAVR() {
        return byteD_NAVR / 1000.0;
    }

    /**
     * @return the byteThrMid
     */
    public double getByteThr_MID() {
        return byteTHR_MID / 100.0;
    }

    /**
     * @return the byteThrExpo
     */
    public double getByteThr_EXPO() {
        return byteTHR_EXPO / 100.0;
    }

    /**
     * @return the byteD_PITCH
     */
    public double getByteP_ALARM() {
        return byteP_ALARM;
    }

    public void setByteP_ROLL(Double value) {

        byteP_ROLL = (int) (Math.floor(value * 10));

    }

    public void setByteI_ROLL(Double value) {

        byteI_ROLL = (int) (Math.floor(value * 1000));

    }

    public void setByteD_ROLL(Double value) {

        byteD_ROLL = (int) (Math.floor(value));

    }

    public void setByteP_PITCH(Double value) {

        byteP_PITCH = (int) (Math.floor(value * 10));

    }

    public void setByteI_PITCH(Double value) {

        byteI_PITCH = (int) (Math.floor(value * 1000));

    }

    public void setByteD_PITCH(Double value) {

        byteD_PITCH = (int) (Math.floor(value));

    }

    public void setByteP_YAW(Double value) {

        byteP_YAW = (int) (Math.floor(value * 10));

    }

    public void setByteI_YAW(Double value) {

        byteI_YAW = (int) (Math.floor(value * 1000));

    }

    public void setByteD_YAW(Double value) {

        byteD_YAW = (int) (Math.floor(value));

    }

    public void setByteP_ALT(Double value) {

        byteP_ALT = (int) (Math.floor(value * 10));

    }

    public void setByteI_ALT(Double value) {

        byteI_ALT = (int) (Math.floor(value * 1000));

    }

    public void setByteD_ALT(Double value) {

        byteD_ALT = (int) (Math.floor(value));

    }

    public void setByteP_VEL(Double value) {

        byteP_VEL = (int) (Math.floor(value * 10));

    }

    public void setByteI_VEL(Double value) {

        byteI_VEL = (int) (Math.floor(value * 1000));

    }

    public void setByteD_VEL(Double value) {

        byteD_VEL = (int) (Math.floor(value));

    }

    public void setByteP_GPS(Double value) {

        byteP_GPS = (int) (Math.floor(value * 10));

    }

    public void setByteI_GPS(Double value) {

        byteI_GPS = (int) (Math.floor(value * 1000));

    }

    public void setByteD_GPS(Double value) {

        byteD_GPS = (int) (Math.floor(value));

    }

    public void setByteP_LEVEL(Double value) {

        byteP_LEVEL = (int) (Math.floor(value * 10));

    }

    public void setByteI_LEVEL(Double value) {

        byteI_LEVEL = (int) (Math.floor(value * 1000));

    }

    public void setByteD_LEVEL(Double value) {

        byteD_LEVEL = (int) (Math.floor(value));

    }

    public void setByteRollPitchRate(Double value) {

        byteRollPitchRate = (int) (Math.floor(value * 100));

    }

    public void setByteYawRate(Double value) {

        byteYawRate = (int) (Math.floor(value * 100));

    }

    public void setByteRC_RATE(Double value) {
        if(AndMultiWiiConf.getActiveVersion() <= 3) {
            byteRC_RATE = (int) (Math.floor(value * 50));
        } else {
            byteRC_RATE = (int) (Math.floor(value * 100));
        }


    }

    public void setByteRC_EXPO(Double value) {

        byteRC_EXPO = (int) (Math.floor(value * 100));

    }

    public void setByteDynThrPID(Double value) {

        byteDynThrPID = (int) (Math.floor(value * 100));

    }

    public void setByteP_MAG(Double value) {
        byteP_MAG = (int) (Math.floor(value * 10));
    }

    //public void setIntPowerTrigger(int intPowerTrigger) {
    //    this.intPowerTrigger = intPowerTrigger;
    //}

    public void setByteP_POS(Double value) {
        byteP_POS = (int) (Math.floor(value * 100));
    }

    public void setByteI_POS(Double value) {
        byteI_POS = (int) (Math.floor(value * 100));
    }

    public void setByteP_POSR(Double value) {

        byteP_POSR = (int) (Math.floor(value * 10));

    }

    public void setByteI_POSR(Double value) {

        byteI_POSR = (int) (Math.floor(value * 100));

    }

    public void setByteD_POSR(Double value) {

        byteD_POSR = (int) (Math.floor(value * 1000));

    }

    public void setByteP_NAVR(Double value) {

        byteP_NAVR = (int) (Math.floor(value * 10));

    }

    public void setByteI_NAVR(Double value) {

        byteI_NAVR = (int) (Math.floor(value * 100));

    }

    public void setByteD_NAVR(Double value) {

        byteD_NAVR = (int) (Math.floor(value * 1000));

    }

    public void setByteTHR_MID(Double value) {

        byteTHR_MID = (int) (Math.floor(value * 100));

    }

    public void setByteTHR_EXPO(Double value) {

        byteTHR_EXPO = (int) (Math.floor(value * 100));

    }

    public void setByteP_ALARM(Double value) {

        byteP_ALARM = (int) (Math.floor(value));

    }

    /**
     * Set the state of the activation for the give BOX
     *
     * @param boxIndex
     * @param channelIndex
     * @param state
     */
    public void setBoxActivation(Integer boxIndex, Integer channelIndex, Integer positionIndex, boolean state) {
        if (boxIndex != null && channelIndex != null && positionIndex != null) {
            switch (channelIndex) {
                case 1: // AUX1
                    activationState[boxIndex] ^= positionIndex;
                    break;
                case 2: // AUX2
                    activationState[boxIndex] ^= positionIndex * 8;
                    break;
                case 3: // AUX3
                    activationState[boxIndex] ^= positionIndex * 64;
                    break;
                case 4: // AUX4
                    activationState[boxIndex] ^= positionIndex * 512;
                    break;
            }
            if(D) Log.d(TAG, "Set State is: " + activationState[boxIndex]);
        }
    }

    /**
     * Get the state of the activation for the give BOX
     *
     * @param boxIndex
     * @param channelIndex
     * @param positionIndex
     */
    public boolean getBoxActivation(Integer boxIndex, Integer channelIndex, Integer positionIndex) {
        if (boxIndex != null && channelIndex != null && positionIndex != null) {
            if(D) Log.d(TAG, "Get State of "
                     + boxIndex + "_"
                     + channelIndex + "_"
                     + positionIndex + "_"
                     + " : "  + activationState[boxIndex]);

            switch (channelIndex) {
                case 1: // AUX1
                    return ((activationState[boxIndex] & positionIndex) > 0 ? true : false);
                case 2: // AUX2
                    return ((activationState[boxIndex] & positionIndex * 8) > 0 ? true : false);
                case 3: // AUX3
                    return ((activationState[boxIndex] & positionIndex * 64) > 0 ? true : false);
                case 4: // AUX4
                    return ((activationState[boxIndex] & positionIndex * 512) > 0 ? true : false);
            }
        }
        return false;
    }

    public boolean setParamsData(int[] input) {

        try {
            if (D)
                Log.i(TAG, "Params size: " + input.length);
            parseData(input);
            return true;

        } catch (Exception e) {
            if (D)
                Log.e(TAG, "parseData(): Error parsing Parameters Error:" + e.getMessage());
                e.printStackTrace();
        }
        return false;
    }

    public void parseData(int[] paramsData) {
    	switch (AndMultiWiiConf.getActiveVersion() ) {
    	case MwcConstants.MWC_VERSION_17:
    	    if (D) Log.d(TAG, "Active version 1.7 parsing data");
    	    parseData_17(paramsData);
    	    break;
    	case MwcConstants.MWC_VERSION_18:
    	    if (D) Log.d(TAG, "Active version 1.8 parsing data");
    	    parseData_18(paramsData);
    	    break;
    	case MwcConstants.MWC_VERSION_19:
    	    if (D) Log.d(TAG, "Active version 1.9 parsing data");
    	    parseData_19(paramsData);
    	    break;
    	case MwcConstants.MWC_VERSION_20:
    	    if (D) Log.d(TAG, "Active version 2.0 parsing data");
    	    parseData_20(paramsData);
    	    break;
    	case MwcConstants.MWC_VERSION_21:
            if (D) Log.d(TAG, "Active version 2.1 parsing data");
            parseData_21(paramsData);
            break;
        case MwcConstants.MWC_VERSION_22:
            if (D) Log.d(TAG, "Active version 2.2 parsing data");
            parseData_22(paramsData);
            break;
    	}
    }

    private void parseData_17(int[] paramsData) {
        int i = 1;
    	if ((char) paramsData[0] == 'C' ) {

    	    byteP_ROLL = paramsData[i++];
    	    byteI_ROLL = paramsData[i++];
    	    byteD_ROLL = paramsData[i++];
    	    byteP_PITCH = paramsData[i++];
    	    byteI_PITCH = paramsData[i++];
    	    byteD_PITCH = paramsData[i++];
    	    byteP_YAW = paramsData[i++];
    	    byteI_YAW = paramsData[i++];
    	    byteD_YAW = paramsData[i++];
    	    byteP_LEVEL = paramsData[i++];
    	    byteI_LEVEL = paramsData[i++];
    	    byteRC_RATE = paramsData[i++];
    	    byteRC_EXPO = paramsData[i++];
    	    byteRollPitchRate = paramsData[i++];
    	    byteYawRate = paramsData[i++];
    	    byteDynThrPID = paramsData[i++];

    	    // Process switches
            for (int iter=0; iter < MwcConstants.MWC_17_BOX_LIST.size(); iter++) {
                activationState[iter] = paramsData[i++];
            }

    	    if(D) {

    		for(int iter =0; iter < paramsData.length; iter++) {
    		    Log.d(TAG, "parseData_new() : Pos[" + iter + "] -> "  + paramsData[iter]);
    		}
    		Log.d(TAG, "Parsing data succeded.");
    	    }

    	} else {
    	    // Log info as data parsing has failed.
    	    Log.i(TAG, "Parsing data failed.");
	}

    }

    private void parseData_18(int[] paramsData) {
        int i = 1;
	if ((char) paramsData[0] == 'W' ) {

	    byteP_ROLL = paramsData[i++];
	    byteI_ROLL = paramsData[i++];
	    byteD_ROLL = paramsData[i++];
	    byteP_PITCH = paramsData[i++];
	    byteI_PITCH = paramsData[i++];
	    byteD_PITCH = paramsData[i++];
	    byteP_YAW = paramsData[i++];
	    byteI_YAW = paramsData[i++];
	    byteD_YAW = paramsData[i++];
	    byteP_ALT = paramsData[i++];
	    byteI_ALT = paramsData[i++];
	    byteD_ALT = paramsData[i++];
	    byteP_VEL = paramsData[i++];
	    byteI_VEL = paramsData[i++];
	    byteD_VEL = paramsData[i++];
	    byteP_LEVEL = paramsData[i++];
	    byteI_LEVEL = paramsData[i++];
	    byteP_MAG= paramsData[i++];
	    byteRC_RATE = paramsData[i++];
	    byteRC_EXPO = paramsData[i++];
	    byteRollPitchRate = paramsData[i++];
	    byteYawRate = paramsData[i++];
	    byteDynThrPID = paramsData[i++];

	    // Process switches
        for (int iter=0; iter < MwcConstants.MWC_18_BOX_LIST.size(); iter++) {
            activationState[iter] = paramsData[i++];
        }

	    byteP_ALARM = paramsData[i++] ;

	    if(D) {
		for(int iter =0; iter < paramsData.length; iter++) {
		    Log.d(TAG, "parseData_new() : Pos[" + iter + "] -> "  + paramsData[iter]);
		}
		Log.d(TAG, "Parsing data succeded.");
	    }

	} else {
	    // Log info as data parsing has failed.
	    Log.i(TAG, "Parsing data failed.");
	}

    }

    private void parseData_19(int[] paramsData) {
        int i = 1;
	if ((char) paramsData[0] == 'W' ) {

	    byteP_ROLL = paramsData[i++] ;
	    byteI_ROLL = paramsData[i++] ;
	    byteD_ROLL = paramsData[i++] ;
	    byteP_PITCH = paramsData[i++] ;
	    byteI_PITCH = paramsData[i++] ;
	    byteD_PITCH = paramsData[i++];
	    byteP_YAW = paramsData[i++];
	    byteI_YAW = paramsData[i++];
	    byteD_YAW = paramsData[i++];
	    byteP_ALT = paramsData[i++];
	    byteI_ALT = paramsData[i++];
	    byteD_ALT = paramsData[i++];
	    byteP_VEL = paramsData[i++];
	    byteI_VEL = paramsData[i++];
	    byteD_VEL = paramsData[i++];
	    byteP_LEVEL = paramsData[i++];
	    byteI_LEVEL = paramsData[i++];
	    byteP_MAG= paramsData[i++];
	    byteRC_RATE = paramsData[i++];
	    byteRC_EXPO = paramsData[i++];
	    byteRollPitchRate = paramsData[i++];
	    byteYawRate = paramsData[i++];
	    byteDynThrPID = paramsData[i++];

	    // Process switches
        for (int iter=0; iter < MwcConstants.MWC_19_BOX_LIST.size(); iter++) {
            activationState[iter] = paramsData[i++];
        }

	    byteP_ALARM = paramsData[i++] ;

	    if(D) {
		for(int iter =0; iter < paramsData.length; iter++) {
		    Log.d(TAG, "parseData_19() : Pos[" + iter + "] -> "  + paramsData[iter]);
		}
		Log.d(TAG, "Parsing data succeded.");
	    }

	} else {
	    // Log info as data parsing has failed.
	    Log.i(TAG, "Parsing data failed.");
	}
    }

    private void parseData_20(int[] paramsData) {
        int i = 1;

    	if ((char) paramsData[0] == 'W' ) {

    	    byteP_ROLL = paramsData[i++] ;
    	    byteI_ROLL = paramsData[i++] ;
    	    byteD_ROLL = paramsData[i++] ;
    	    byteP_PITCH = paramsData[i++] ;
    	    byteI_PITCH = paramsData[i++] ;
    	    byteD_PITCH = paramsData[i++] ;
    	    byteP_YAW = paramsData[i++] ;
    	    byteI_YAW = paramsData[i++] ;
    	    byteD_YAW = paramsData[i++] ;
    	    byteP_ALT = paramsData[i++] ;
    	    byteI_ALT = paramsData[i++] ;
    	    byteD_ALT = paramsData[i++] ;
    	    byteP_VEL = paramsData[i++] ;
    	    byteI_VEL = paramsData[i++] ;
    	    byteD_VEL = paramsData[i++] ;
    	    byteP_GPS = paramsData[i++] ;
    	    byteI_GPS = paramsData[i++] ;
    	    byteD_GPS = paramsData[i++] ;
    	    byteP_LEVEL = paramsData[i++] ;
    	    byteI_LEVEL = paramsData[i++] ;
    	    byteD_LEVEL = paramsData[i++] ;
    	    byteP_MAG = paramsData[i++];
    	    // 2 Null for missing I & D Mag
    	    i++;
    	    i++;
    	    byteRC_RATE = paramsData[i++];
    	    byteRC_EXPO = paramsData[i++];
    	    byteRollPitchRate = paramsData[i++];
    	    byteYawRate = paramsData[i++];
    	    byteDynThrPID = paramsData[i++];

            for (int iter=0; iter < MwcConstants.MWC_20_BOX_LIST.size() * 2; iter++) {
                activationState[iter] = paramsData[i++];
            }

    	    byteP_ALARM = paramsData[i++] ;

    	    if(D) {
    		for(int iter =0; i < paramsData.length; i++) {
    		    Log.d(TAG, "parseData_20() : Pos[" + iter + "] -> "  + paramsData[iter]);
    		}
    		Log.d(TAG, "Parsing data succeded.");
    	    }

    	} else {
    	    // Log info as data parsing has failed.
    	    Log.i(TAG, "Parsing 2.0 data failed. Char: " + (char) paramsData[0]);
    	}

    }

    private void parseData_21(int[] paramsData) {

        if (paramsData[0] == Mwc2_1.MSP_SET_PID ) {
            int i = 1;
            byteP_ROLL = paramsData[i++] ;
            byteI_ROLL = paramsData[i++] ;
            byteD_ROLL = paramsData[i++] ;
            byteP_PITCH = paramsData[i++] ;
            byteI_PITCH = paramsData[i++] ;
            byteD_PITCH = paramsData[i++] ;
            byteP_YAW = paramsData[i++] ;
            byteI_YAW = paramsData[i++] ;
            byteD_YAW = paramsData[i++] ;
            byteP_ALT = paramsData[i++] ;
            byteI_ALT = paramsData[i++] ;
            byteD_ALT = paramsData[i++] ;
            byteP_POS = paramsData[i++] ;
            byteI_POS = paramsData[i++] ;
            i++;
            byteP_POSR = paramsData[i++] ;
            byteI_POSR = paramsData[i++];
            byteD_POSR =  paramsData[i++];
            byteP_NAVR = paramsData[i++] ;
            byteI_NAVR = paramsData[i++] ;
            byteD_NAVR = paramsData[i++] ;
            byteP_LEVEL = paramsData[i++] ;
            byteI_LEVEL = paramsData[i++] ;
            byteD_LEVEL = paramsData[i++] ;
            byteP_MAG = paramsData[i++] ;
            i++;
            i++; // 2 Null for missing I & D Mag
            byteRC_RATE = paramsData[i++] ;
            byteRC_EXPO = paramsData[i++] ;
            byteRollPitchRate = paramsData[i++] ;
            byteYawRate = paramsData[i++] ;
            byteDynThrPID = paramsData[i++] ;
            byteTHR_MID = paramsData[i++] ;
            byteTHR_EXPO = paramsData[i++] ;

            // Process switches
            // Level State
            for (int iter=0; iter < MwcConstants.MWC_21_BOX_LIST.size() * 2; iter++) {
                activationState[iter] = paramsData[i++];
            }

            byteP_ALARM = paramsData[i++] ;

            if(D) {
            for(int e =0; e < paramsData.length; e++) {
                Log.d(TAG, "parseData_21() : Pos[" + e + "] -> "  + paramsData[e]);
            }
            Log.d(TAG, "Parsing data succeded.");
            }

        } else {
            // Log info as data parsing has failed.
            Log.i(TAG, "Parsing 2.1 data failed. Char: " + paramsData[0]);
        }

        }

    private void parseData_22(int[] paramsData) {

        if (paramsData[0] == Mwc2_2.MSP_SET_PID ) {
            int i = 1;
            byteP_ROLL = paramsData[i++] ;
            byteI_ROLL = paramsData[i++] ;
            byteD_ROLL = paramsData[i++] ;
            byteP_PITCH = paramsData[i++] ;
            byteI_PITCH = paramsData[i++] ;
            byteD_PITCH = paramsData[i++] ;
            byteP_YAW = paramsData[i++] ;
            byteI_YAW = paramsData[i++] ;
            byteD_YAW = paramsData[i++] ;
            byteP_ALT = paramsData[i++] ;
            byteI_ALT = paramsData[i++] ;
            byteD_ALT = paramsData[i++] ;
            byteP_POS = paramsData[i++] ;
            byteI_POS = paramsData[i++] ;
            i++;
            byteP_POSR = paramsData[i++] ;
            byteI_POSR = paramsData[i++];
            byteD_POSR =  paramsData[i++];
            byteP_NAVR = paramsData[i++] ;
            byteI_NAVR = paramsData[i++] ;
            byteD_NAVR = paramsData[i++] ;
            byteP_LEVEL = paramsData[i++] ;
            byteI_LEVEL = paramsData[i++] ;
            byteD_LEVEL = paramsData[i++] ;
            byteP_MAG = paramsData[i++] ;
            i++;
            i++; // 2 Null for missing I & D Mag
            byteRC_RATE = paramsData[i++] ;
            byteRC_EXPO = paramsData[i++] ;
            byteRollPitchRate = paramsData[i++] ;
            byteYawRate = paramsData[i++] ;
            byteDynThrPID = paramsData[i++] ;
            byteTHR_MID = paramsData[i++] ;
            byteTHR_EXPO = paramsData[i++] ;

            // Process switches
            // Level State
            for (int iter=0; iter < MwcConstants.MWC_22_BOX_LIST.size() * 2; iter++) {
                activationState[iter] = paramsData[i++];
            }

            byteP_ALARM = paramsData[i++] ;

            if(D) {
                for(int e =0; e < paramsData.length; e++) {
                    Log.d(TAG, "parseData_22() : Pos[" + e + "] -> "  + paramsData[e]);
                }
                Log.d(TAG, "Parsing data succeded.");
            }

        } else {
            // Log info as data parsing has failed.
            Log.i(TAG, "Parsing 2.2 data failed. Char: " + paramsData[0]);
        }

    }


    public int[] getParamsData() {
	switch (AndMultiWiiConf.getActiveVersion() ) {
	case MwcConstants.MWC_VERSION_17:
	    if (D) Log.d(TAG, "Active version 1.7 getParamsData()");
	    return getParamsData_17();
	case MwcConstants.MWC_VERSION_18:
	    if (D) Log.d(TAG, "Active version 1.8 getParamsData()");
	    return getParamsData_18();
	case MwcConstants.MWC_VERSION_19:
	    if (D) Log.d(TAG, "Active version 1.9 getParamsData()");
	    return getParamsData_19();
	case MwcConstants.MWC_VERSION_20:
	    if (D) Log.d(TAG, "Active version 2.0 getParamsData()");
	    return getParamsData_20();
	}
	return null;
    }

    /**
     * Return the format string to pass to the BTService and send to the MWC board
     * to save as parameters
     * @return params as int[]
     */
    public int[] getParamsData_17() {

    	int[] paramsString = new int[32];
    	int i = 0;

    	paramsString[i++] = 'C'; // control char

    	paramsString[i++] = Math.round(byteP_ROLL);
    	paramsString[i++] = Math.round(byteI_ROLL);
    	paramsString[i++] = Math.round(byteD_ROLL);

    	paramsString[i++] = Math.round(byteP_PITCH);
    	paramsString[i++] = Math.round(byteI_PITCH);
    	paramsString[i++] = Math.round(byteD_PITCH);

    	paramsString[i++] = Math.round(byteP_YAW);
    	paramsString[i++] = Math.round(byteI_YAW);
    	paramsString[i++] = Math.round(byteD_YAW);

    	paramsString[i++] = Math.round(byteP_LEVEL);
    	paramsString[i++] = Math.round(byteI_LEVEL);

    	paramsString[i++] = Math.round(byteRC_RATE);
    	paramsString[i++] = Math.round(byteRC_EXPO);

    	paramsString[i++] = Math.round(byteRollPitchRate);
    	paramsString[i++] = Math.round(byteYawRate);
    	paramsString[i++] = Math.round(byteDynThrPID);

        // Process the Box
        for(int iter=0; iter < MwcConstants.MWC_17_BOX_LIST.size(); iter++) {
            paramsString[i++] = activationState[iter] & 63; // Get lsb 6bits
        }

    	return paramsString;

    }

    /**
     * Return the format string to pass to the BTService and send to the MWC board
     * to save as parameters
     * @return integer array containing the parameter bytes
     */
    public int[] getParamsData_18() {

    	int[] paramsString = new int[32];
    	int i = 0;

    	paramsString[i++] = 'W'; // control char

    	paramsString[i++] = Math.round(byteP_ROLL);
    	paramsString[i++] = Math.round(byteI_ROLL);
    	paramsString[i++] = Math.round(byteD_ROLL);

    	paramsString[i++] = Math.round(byteP_PITCH);
    	paramsString[i++] = Math.round(byteI_PITCH);
    	paramsString[i++] = Math.round(byteD_PITCH);

    	paramsString[i++] = Math.round(byteP_YAW);
    	paramsString[i++] = Math.round(byteI_YAW);
    	paramsString[i++] = Math.round(byteD_YAW);

    	paramsString[i++] = Math.round(byteP_ALT);
    	paramsString[i++] = Math.round(byteI_ALT);
    	paramsString[i++] = Math.round(byteD_ALT);

    	paramsString[i++] = Math.round(byteP_VEL);
    	paramsString[i++] = Math.round(byteI_VEL);
    	paramsString[i++] = Math.round(byteD_VEL);

    	paramsString[i++] = Math.round(byteP_LEVEL);
    	paramsString[i++] = Math.round(byteI_LEVEL);

    	paramsString[i++] = Math.round(byteP_MAG);

    	paramsString[i++] = Math.round(byteRC_RATE);
    	paramsString[i++] = Math.round(byteRC_EXPO);

    	paramsString[i++] = Math.round(byteRollPitchRate);
    	paramsString[i++] = Math.round(byteYawRate);
    	paramsString[i++] = Math.round(byteDynThrPID);

        // Process the Box
        for(int iter=0; iter < MwcConstants.MWC_18_BOX_LIST.size(); iter++) {
            paramsString[i++] = activationState[iter] & 63; // Get lsb 6bits
        }

    	paramsString[i++] = Math.round(byteP_ALARM);

    	if (D) {
    	    for (int iter = 0; iter < paramsString.length; iter++) {
    	      Log.d(TAG, "getParamdata() : Pos[" + iter + "] -> " + paramsString[iter]);
    	    }
    	}

    	return paramsString;

    }

    /**
     * Return the format string to pass to the BTService and send to the MWC board
     * to save as parameters
     * @return paramas as int[]
     */
    public int[] getParamsData_19() {

	int[] paramsString = new int[34];
	int i = 0;

	paramsString[i++] = 'W'; // control char

	paramsString[i++] = Math.round(byteP_ROLL);
	paramsString[i++] = Math.round(byteI_ROLL);
	paramsString[i++] = Math.round(byteD_ROLL);

	paramsString[i++] = Math.round(byteP_PITCH);
	paramsString[i++] = Math.round(byteI_PITCH);
	paramsString[i++] = Math.round(byteD_PITCH);

	paramsString[i++] = Math.round(byteP_YAW);
	paramsString[i++] = Math.round(byteI_YAW);
	paramsString[i++] = Math.round(byteD_YAW);

	paramsString[i++] = Math.round(byteP_ALT);
	paramsString[i++] = Math.round(byteI_ALT);
	paramsString[i++] = Math.round(byteD_ALT);

	paramsString[i++] = Math.round(byteP_VEL);
	paramsString[i++] = Math.round(byteI_VEL);
	paramsString[i++] = Math.round(byteD_VEL);

	paramsString[i++] = Math.round(byteP_LEVEL);
	paramsString[i++] = Math.round(byteI_LEVEL);

	paramsString[i++] = Math.round(byteP_MAG);

	paramsString[i++] = Math.round(byteRC_RATE);
	paramsString[i++] = Math.round(byteRC_EXPO);

	paramsString[i++] = Math.round(byteRollPitchRate);
	paramsString[i++] = Math.round(byteYawRate);
	paramsString[i++] = Math.round(byteDynThrPID);

	// Process the Box
    for(int iter=0; iter < MwcConstants.MWC_19_BOX_LIST.size(); iter++) {
        paramsString[i++] = activationState[iter] & 63; // Get lsb 6bits
    }

	paramsString[i++] = Math.round(byteP_ALARM);


	if (D) {
	    for (int iter = 0; iter < paramsString.length; iter++) {
	      Log.d(TAG, "getParamdata() : Pos[" + iter + "] -> " + paramsString[iter]);
	    }
	}

	return paramsString;
    }

    /**
     * Return the format string to pass to the BTService and send to the MWC board
     * to save as parameters
     * @return Integer array with values to write for v2.0
     */
    public int[] getParamsData_20() {

	int[] paramsString = new int[54];
	int i = 0;

	paramsString[i++] = 'W'; // control char

	paramsString[i++] = Math.round(byteP_ROLL);
	paramsString[i++] = Math.round(byteI_ROLL);
	paramsString[i++] = Math.round(byteD_ROLL);

	paramsString[i++] = Math.round(byteP_PITCH);
	paramsString[i++] = Math.round(byteI_PITCH);
	paramsString[i++] = Math.round(byteD_PITCH);

	paramsString[i++] = Math.round(byteP_YAW);
	paramsString[i++] = Math.round(byteI_YAW);
	paramsString[i++] = Math.round(byteD_YAW);

	paramsString[i++] = Math.round(byteP_ALT);
	paramsString[i++] = Math.round(byteI_ALT);
	paramsString[i++] = Math.round(byteD_ALT);

	paramsString[i++] = Math.round(byteP_VEL);
	paramsString[i++] = Math.round(byteI_VEL);
	paramsString[i++] = Math.round(byteD_VEL);

	paramsString[i++] = Math.round(byteP_GPS);
	paramsString[i++] = Math.round(byteI_GPS);
	paramsString[i++] = Math.round(byteD_GPS);

	paramsString[i++] = Math.round(byteP_LEVEL);
	paramsString[i++] = Math.round(byteI_LEVEL);
	paramsString[i++] = Math.round(byteD_LEVEL);

	paramsString[i++] = Math.round(byteP_MAG);
	paramsString[i++] = 0;
	paramsString[i++] = 0;

	paramsString[i++] = Math.round(byteRC_RATE);
	paramsString[i++] = Math.round(byteRC_EXPO);

	paramsString[i++] = Math.round(byteRollPitchRate);
	paramsString[i++] = Math.round(byteYawRate);
	paramsString[i++] = Math.round(byteDynThrPID);

	// Process the Box
    for(int iter=0; iter < MwcConstants.MWC_20_BOX_LIST.size(); iter++) {
        // Split in 2 bytes as per v2.0 method
        paramsString[i++] = activationState[iter] & 63; // Get lsb 6bits
        paramsString[i++] = activationState[iter] >> 6; // Get msb 6bits
    }

	paramsString[i++] = Math.round(byteP_ALARM);
	//paramsString[50] = Math.round(intPowerTrigger) >>8 &0xff;

	if (D) {
	    for (int iter = 0; iter < paramsString.length; iter++) {
		Log.d(TAG, "getParamdata() : Pos[" + iter + "] -> " + paramsString[iter]);
	    }
	}

	return paramsString;
    }

    public byte[] getParamstoArray(int mActiveMwcVersion){

      int[] paramsIntArray = this.getParamsData();
      byte[] paramsByteArray = new byte[paramsIntArray.length];

      for(int i =0; i < paramsIntArray.length; i++) {
        if (D) {
          Log.d(TAG, "getParamstoArray() : Pos[" + i + "] -> "  + paramsIntArray[i]);
        }
        paramsByteArray[i] = (byte) ((paramsIntArray[i]) & 0xFF);
      }

      if (mActiveMwcVersion == MwcConstants.MWC_VERSION_18
          || mActiveMwcVersion == MwcConstants.MWC_VERSION_19) {
        paramsByteArray[31] = (byte) ((paramsIntArray[30]) >> 8 &0xFF);
      }

      if (mActiveMwcVersion == MwcConstants.MWC_VERSION_20) {
        paramsByteArray[53] = (byte) ((paramsIntArray[52]) >> 8 &0xFF);
      }
      return paramsByteArray;
    }

    public void resetToDefaults(int mActiveMwcVersion) {
      if (D) { Log.d(TAG, "Reseting to defaults"); }
      parseData(MwcConstants.DEFAULT_VALUES[mActiveMwcVersion]);
    }

    /**
     * @param mActiveMwcVersion
     * @param msp
     * @return values as byte[]
     */
    public byte[] getValues(int mActiveMwcVersion, int msp) {
      if (mActiveMwcVersion > 3) {
        byte[] byteBuffer;
        int i = 0;

        switch (msp) {
            case Mwc2_1.MSP_SET_MISC:
                byteBuffer = new byte[2];
                int tempValue = Math.round(byteP_ALARM);
                byteBuffer[0] = ((byte) ((char) tempValue % 256));
                byteBuffer[1] = ((byte) ((char) tempValue / 256));
                return byteBuffer;

          case Mwc2_1.MSP_SET_RC_TUNING:
              byteBuffer = new byte[7];
              byteBuffer[i++] = ((byte) (char) Math.round(byteRC_RATE));
              byteBuffer[i++] = ((byte) (char) Math.round(byteRC_EXPO));
              byteBuffer[i++] = ((byte) (char) Math.round(byteRollPitchRate));
              byteBuffer[i++] = ((byte) (char) Math.round(byteYawRate));
              byteBuffer[i++] = ((byte) (char) Math.round(byteDynThrPID));
              byteBuffer[i++] = ((byte) (char) Math.round(byteTHR_MID));
              byteBuffer[i++] = ((byte) (char) Math.round(byteTHR_EXPO));

              if(D) Log.d(TAG, "MwcParamaters data: for RC_TUNING:" + byteBuffer.length);
              DataTools.getHex(byteBuffer);
              return byteBuffer;

          case Mwc2_1.MSP_SET_PID:
            byteBuffer = new byte[27];
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_ROLL));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_ROLL));
            byteBuffer[i++] = ((byte) (char) Math.round(byteD_ROLL));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_PITCH));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_PITCH));
            byteBuffer[i++] = ((byte) (char) Math.round(byteD_PITCH));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_YAW));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_YAW));
            byteBuffer[i++] = ((byte) (char) Math.round(byteD_YAW));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_ALT));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_ALT));
            byteBuffer[i++] = ((byte) (char) Math.round(byteD_ALT));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_POS));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_POS));
            byteBuffer[i++] = ((byte) (char) Math.round(0&0xFF));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_POSR));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_POSR));
            byteBuffer[i++] = ((byte) (char) Math.round(byteD_POSR));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_NAVR));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_NAVR));
            byteBuffer[i++] = ((byte) (char) Math.round(byteD_NAVR));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_LEVEL));
            byteBuffer[i++] = ((byte) (char) Math.round(byteI_LEVEL));
            byteBuffer[i++] = ((byte) (char) Math.round(byteD_LEVEL));
            byteBuffer[i++] = ((byte) (char) Math.round(byteP_MAG));
            byteBuffer[i++] = ((byte) (char) Math.round(0&0xFF));
            byteBuffer[i++] = ((byte) (char) Math.round(0&0xFF));
            return byteBuffer;

          case Mwc2_1.MSP_SET_BOX:
                  ByteBuffer tempBuffer = ByteBuffer.allocate(activationState.length * 2);
                  for(i=0; i < activationState.length; i++) {
                      tempBuffer.put((byte) ((char) activationState[i] % 256));
                      tempBuffer.put((byte) ((char) activationState[i] / 256));
                  }

                  if(D) Log.d(TAG, "MwcParamaters data: for BOX:");
                  DataTools.getHex(tempBuffer.array());
                  return tempBuffer.array();

          default:
            break;
        }
      }
      return null;
    }

    /**
     * @param valIndex
     * @param rcData
     */
    //public void setByte(int valIndex, Double rcData) {
        // TODO: adasd
    //}
    
}
