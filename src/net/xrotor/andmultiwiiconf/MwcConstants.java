package net.xrotor.andmultiwiiconf;

import java.util.HashMap;
import java.util.Map;

public class MwcConstants {

	// Set default base name for the screenshots
	public static final String SCREENSHOT_NAME = "AndMwcScreenshot_";

	// Set the path to the profiles folder
	public static final String PROFILES_PATH = "/amwc/";

	// Set the path to the screenshot folder
	public static final String SCREENSHOTS_PATH = "/amwc/";

	// Set the profiles file extension
	public static final String PROFILES_EXTENTION = ".mwc";

	// Set number of views
	public static final int VIEW_COUNT = 4;

	// List of the supported version numbers
	public static final int[] MWC_VERSIONS_LIST = {
		17, // v1.7
		18, // v1.8 patch2
		19, // v1.9
		20,  // v2.0
		21,  // v2.1
		22  // v2.2
	};
	// List of the names supported version numbers to use in the UI
	public static final String[] MWC_VERSIONS_LIST_NAME= {
		"1.7",
		"1.8p2",
		"1.9",
		"2.0",
		"2.1",
		"2.2"
	};

	// Mwc Version index mapping
	public static final int MWC_VERSION_17   = 0;
	public static final int MWC_VERSION_18   = 1;
	public static final int MWC_VERSION_19   = 2;
	public static final int MWC_VERSION_20   = 3;
	public static final int MWC_VERSION_21   = 4;
	public static final int MWC_VERSION_22   = 5;

	// Set the default Mwc Version index
	public static final int MWC_DEFAULT_VERSION = MWC_VERSION_22; // v.2.2

	// Mappings of the Serial command index
	public static final int SERIAL_COMMAND_READ_DATA = 0;
	public static final int SERIAL_COMMAND_WRITE_DATA = 1;
	public static final int SERIAL_COMMAND_CALIBRATE_ACC = 2;
	public static final int SERIAL_COMMAND_CALIBRATE_MAG = 3;
	public static final int SERIAL_COMMAND_OSD = 4;

	// Serial commands list for version v1.7
	public static String[] MWCCOMMANDS_LIST_17 = {
		"A",
		"C",
		"D",
		"",
		"O"
	};

	// Serial commands list for version v1.8
	public static String[] MWCCOMMANDS_LIST_18 = {
		"M",
		"W",
		"S",
		"E",
		"O"
	};

	// Serial commands list for version v1.9
	public static String[] MWCCOMMANDS_LIST_19 = {
		"M",
		"W",
		"S",
		"E",
		"O"
	};

	// Serial commands list for version v1.9
	public static String[] MWCCOMMANDS_LIST_20 = {
		"M",
		"W",
		"S",
		"E",
		"O"
	};

	// Serial frame size
	public static final int[] SERIAL_FRAME_SIZE = {
	    84,  // version 1.7
	    116, // version 1.8 patch2
	    125, // version 1.9
	    155, // version 2.0
	    256,  // version 2.1
	    256  // version 2.2
	};

    // Default values for Mwc software ( NOTE: * SWITCHES not included * )
    public static final int[][] DEFAULT_VALUES =  {
    	{ // Default values for version 1.7
	    67, 40, 30, 15, 40, 30, 15, 80, 0, 0, 140, 45, 45, 65, 0, 0, 0, 0,
		    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    	},
    	{ // Default values for version 1.8
	    87, 40, 30, 17, 40, 30, 17, 85, 0, 0, 47, 0, 0, 0, 0, 0, 90, 45,
		    40, 45, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        },
        { // Default values for version 1.9
	    87, 40, 30, 23, 40, 30, 23, 85, 0, 0, 47, 0, 0, 0, 0, 0, 90, 45,
		    40, 45, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    	},
    	{ // Default values for version 2.0
	    87, 40, 30, 23, 40, 30, 23, 85, 0, 0, 47, 0, 0, 0, 0, 0, 90, 45,
		    40, 45, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	// List of chars that represent received data per version
    public static final char[] DATA_IDENTIFICATION_CHAR = {
		'A', // v1.7
		'M', // v1.8
		'M', // v1.9
		'M'  // v2.0
	};

    /*
    public static final String[] MWC_20_FIELD_LIST = {
      "valRoll_P",     // ROLL
      "valRoll_I",
      "valRoll_D",
      "valPitch_P",    // PITCH
      "valPitch_I",
      "valPitch_D",
      "valYaw_P",      // YAW
      "valYaw_I",
      "valYaw_D",
      "valAlt_P",      // ALT
      "valAlt_I",
      "valAlt_D",
      "valVel_P",      // VEL
      "valVel_I",
      "valVel_D",
      "valGps_P",      //GPS
      "valGps_I",
      "valGps_D",
      "valRCRate",    // RC
      "valRCExpo",
      "valThrPIDAtt" // ThrPIDAtt
    };

    static final List<String> MWC_21_VALUES_LIST = Arrays.asList(
            "valRoll_P",     // ROLL
            "valRoll_I",
            "valRoll_D",
            "valPitch_P",    // PITCH
            "valPitch_I",
            "valPitch_D",
            "valYaw_P",      // YAW
            "valYaw_I",
            "valYaw_D",
            "valAlt_P",      // ALT
            "valAlt_I",
            "valAlt_D",
            "valPos_P",   // POS
            "valPos_I",
            "valPosR_P",   // POSR
            "valPosR_I",
            "valPosR_D",
            "valNav_P",      // NAV
            "valNav_I",
            "valNav_D",
            "valLevel_P",      //LEVEL
            "valLevel_I",
            "valLevel_D",
            "valMag_P",     // MAG
            "valThrRate",
            "valThrExpo",
            "valRCRate",    // RC
            "valRCExpo",
            "valRollPitch_Rate",
            "valYaw_Rate",
            "valThrPIDAtt", // ThrPIDAtt
            "valPAlarm"    //PALARM
          );

    public static final String[] MWC_21_FIELD_LIST = {
        "valRoll_P",     // ROLL
        "valRoll_I",
        "valRoll_D",
        "valPitch_P",    // PITCH
        "valPitch_I",
        "valPitch_D",
        "valYaw_P",      // YAW
        "valYaw_I",
        "valYaw_D",
        "valAlt_P",      // ALT
        "valAlt_I",
        "valAlt_D",
        "valPos_P",   // POS
        "valPos_I",
        "valPosR_P",   // POSR
        "valPosR_I",
        "valPosR_D",
        "valNav_P",      // NAV
        "valNav_I",
        "valNav_D",
        "valLevel_P",      //LEVEL
        "valLevel_I",
        "valLevel_D",
        "valMag_P",     // MAG
        "valThrRate",
        "valThrExpo",
        "valRCRate",    // RC
        "valRCExpo",
        "valRollPitch_Rate",
        "valYaw_Rate",
        "valThrPIDAtt", // ThrPIDAtt
        "valPAlarm"    //PALARM
      };
*/
    
    // Sensor Graph
    protected static final int MWC_SENSOR_SERIES_SIZE = 200;
    protected static final double MWC_SENSOR_SERIES_MAX_VALUE = 500;
    protected static final double MWC_SENSOR_SERIES_MIN_VALUE = -500;

    // Auto update delay time on millis
    protected static final int autoUpdateDelay = 40;

    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_17_BOX_LIST = new HashMap<String, Integer>() {{
        put("chkLevel", 0);
        put("chkBaro", 1);
        put("chkMag", 2);
        put("chkCamStab", 3);
        put("chkCamTrig", 4);
        put("chkArm", 5);
    }};
    
    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_18_BOX_LIST = new HashMap<String, Integer>() {{
        put("chkLevel", 0);
        put("chkBaro", 1);
        put("chkMag", 2);
        put("chkCamStab", 3);
        put("chkCamTrig", 4);
        put("chkArm", 5);
    }};
    
    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_19_BOX_LIST = new HashMap<String, Integer>() {{
        put("chkLevel", 0);
        put("chkBaro", 1);
        put("chkMag", 2);
        put("chkCamStab", 3);
        put("chkCamTrig", 4);
        put("chkArm", 5);
        put("chkGpsHome", 6);
        put("chkGpsHold",7);
    }};
    
    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_20_BOX_LIST = new HashMap<String, Integer>() {{
        put("chkLevel", 0);
        put("chkBaro", 1);
        put("chkMag", 2);
        put("chkCamStab", 3);
        put("chkCamTrig", 4);
        put("chkArm", 5);
        put("chkGpsHome", 6);
        put("chkGpsHold",7);
        put("chkPassThru", 8);
        put("chkHeadFree", 9);
        put("chkBeeper", 10);
    }};

    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_21_BOX_LIST = new HashMap<String, Integer>() {{
        put("chkLevel", 0);
        put("chkBaro", 1);
        put("chkMag", 2);
        put("chkCamStab", 3);
        put("chkCamTrig", 4);
        put("chkArm", 5);
        put("chkGpsHome", 6);
        put("chkGpsHold",7);
        put("chkPassThru", 8);
        put("chkHeadFree", 9);
        put("chkBeeper", 10);
        put("chkLedMax", 11);
        put("chkLLights", 12);
        put("chkHeadAdj", 13);
    }};

    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_22_BOX_LIST = new HashMap<String, Integer>() {{
        put("chkArm", 0);
        put("chkAngle", 1);
        put("chkHorizon", 2);
        put("chkBaro", 3);
        put("chkVario", 4);
        put("chkMag", 5);
        put("chkHeadFree", 6);
        put("chkHeadAdj", 7);        
        put("chkCamStab", 8);
        put("chkCamTrig", 9);
        put("chkGpsHome", 10);
        put("chkGpsHold", 11);
        put("chkPassThru", 12);
        put("chkBeeper", 13);
        put("chkLedMax", 14);
        put("chkLedLow", 15);
        put("chkLLights", 16);
        put("chkCalib", 17);
        put("chkGovernor", 18);
        put("chkOsdSwitch", 19);
    }};
    
    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_BOX_CHANNEL = new HashMap<String, Integer>() {{
        put("AUX1", 1);
        put("AUX2", 2);
        put("AUX3", 3);
        put("AUX4", 4);
    }};

    @SuppressWarnings("serial")
    public static final Map<String, Integer> MWC_BOX_POSITION = new HashMap<String, Integer>() {{
        put("low",  1);
        put("mid",  2);
        put("high", 4);
    }};
 
}
