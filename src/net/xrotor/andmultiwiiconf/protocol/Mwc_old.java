// Copyright 2012 Google Inc. All Rights Reserved.

package net.xrotor.andmultiwiiconf.protocol;

import net.xrotor.andmultiwiiconf.MwcCommands;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nando@xrotor.net (Fernando Morais)
 *
 */
public class Mwc_old  implements MwcProtocol{

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.Protocol#getVersion()
     */
    @Override
    public List<String> getSupportedVersion() {
        List<String> supportedVersions = new ArrayList<String>();
        supportedVersions.add ("1.7");
        supportedVersions.add ("1.8");
        supportedVersions.add ("1.9");
        return supportedVersions;
    }

    @Override
    public byte[] calibrateACC(int mActiveMwcVersion) {
        return MwcCommands.calibrateACC(mActiveMwcVersion);
    }

    @Override
    public byte[] calibrateMAG(int mActiveMwcVersion) {
        return MwcCommands.calibrateACC(mActiveMwcVersion);
    }

    @Override
    public byte[] readPARAMS(int mActiveMwcVersion) {
        return MwcCommands.readPARAMS(mActiveMwcVersion);
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.MwcProtocol#readRawsensor(int)
     */
    @Override
    public byte[] readSensorRaw(int mActiveMwcVersion) {
        return MwcCommands.readPARAMS(mActiveMwcVersion);
    }

    @Override
    public byte[] readChannels(int mActiveMwcVersion) {
        return null;
    }

    @Override
    public byte[] readState(int mActiveMwcVersion) {
        return null;
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#readGPS(int)
     */
    @Override
    public byte[] readGPS(int mActiveMwcVersion) {
        // TODO(fmorais): Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#resetCONF(int)
     */
    @Override
    public byte[] resetCONF(int mActiveMwcVersion) {
        // TODO(fmorais): Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see net.xrotor.andmultiwiiconf.protocol.MwcProtocol#setRCTUNING(int, byte[])
     */
    @Override
    public byte[] setRCTUNING(int mActiveMwcVersion, byte[] paramstoArray) {
        // TODO(fmorais): Auto-generated method stub
        return null;
    }

    @Override
    public byte[] setPID(int mActiveMwcVersion, byte[] payload) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public byte[] setBOX(int mActiveMwcVersion, byte[] values) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public byte[] eepromWRITE(int mActiveMwcVersion) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public byte[] setMISC(int mActiveMwcVersion, byte[] values) {
        // TODO Auto-generated method stub
        return null;
    }

}
