package net.xrotor.andmultiwiiconf;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Environment;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import java.io.FileOutputStream;
import java.util.Map;

public class MwcUIHelper {
    // Debugging
    private static final String TAG = "AndMultiWiiConf.MwCUIHelper";
    private static final boolean D = false;

    public static final int BUTTONS_PARAMS  = 0;
    public static final int BUTTONS_MONITOR = 1;

    static LinearLayout mLinearLayout;

    private static RCChannelBar barMotor1, barMotor2, barMotor3, barMotor4, barMotor5, barMotor6,
            barMotor7, barMotor8, barRoll, barPitch, barYaw, barThrottle, barAux1, barAux2, barAux3,
            barAux4;

    private static Button btnACC, btnBARO, btnMAG, btnGPS, btnSONAR;

    public MwcUIHelper(LinearLayout mLin) {
        mLinearLayout = mLin;
        // Motor 1
        barMotor1 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor1);
        // Motor 2
        barMotor2 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor2);
        // Motor 3
        barMotor3 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor3);
        // Motor 4
        barMotor4 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor4);
        // Motor 5
        barMotor5 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor5);
        // Motor 6
        barMotor6 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor6);
        // Motor 7
        barMotor7 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor7);
        // Motor 8
        barMotor8 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbMotor8);

        // RC Roll
        barRoll = (RCChannelBar)mLinearLayout.findViewById(R.id.pbRoll);
        // RC Pitch
        barPitch = (RCChannelBar)mLinearLayout.findViewById(R.id.pbPitch);
        // RC Yaw
        barYaw = (RCChannelBar)mLinearLayout.findViewById(R.id.pbYaw);
        // RC Throttle
        barThrottle = (RCChannelBar)mLinearLayout.findViewById(R.id.pbThrottle);
        // RC AUX1
        barAux1 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbAux1);
        // RC AUX2
        barAux2 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbAux2);
        // RC AUX3
        barAux3 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbAux3);
        // RC AUX4
        barAux4 = (RCChannelBar)mLinearLayout.findViewById(R.id.pbAux4);

        btnACC = (Button)mLinearLayout.findViewById(R.id.btnACC);
        btnBARO = (Button)mLinearLayout.findViewById(R.id.btnBARO);
        btnMAG = (Button)mLinearLayout.findViewById(R.id.btnMAG);
        btnGPS = (Button)mLinearLayout.findViewById(R.id.btnGPS);
        btnSONAR = (Button)mLinearLayout.findViewById(R.id.btnSONAR);
    }

    public void reloadLayoutToVersion(int mActiveMwcVersion) {

    	// Inflate the default layout on the first view (Params)
    	ViewGroup firstView = (ViewGroup) mLinearLayout.findViewById(R.id.MwcParameters_first);
    	//LayoutInflater.from(getBaseContext()).inflate(R.layout.params_values_17, firstView);
    	firstView.removeAllViews();
    	LayoutInflater.from(mLinearLayout.getContext()).inflate(
    		mLinearLayout.getResources().getIdentifier(
    			"params_values_" + MwcConstants.MWC_VERSIONS_LIST[mActiveMwcVersion],
    			"layout",
    			"net.xrotor.andmultiwiiconf"), firstView);
    	//firstView.invalidate(); // Force a re-draw

    	// Inflate the default layout on the second view (Switches)
    	ViewGroup secondView = (ViewGroup) mLinearLayout.findViewById(R.id.MwcParameters_second);
    	//LayoutInflater.from(getBaseContext()).inflate(R.layout.params_switches_17, secondView);
    	secondView.removeAllViews();
    	LayoutInflater.from(mLinearLayout.getContext()).inflate(
    		mLinearLayout.getResources().getIdentifier(
    			"params_switches_" + MwcConstants.MWC_VERSIONS_LIST[mActiveMwcVersion],
    			"layout",
    			"net.xrotor.andmultiwiiconf"), secondView);

        ViewGroup thirdView = (ViewGroup) mLinearLayout.findViewById(R.id.MwcParameters_third);
        thirdView.removeAllViews();

    	// If version is >=2.0 we load the second Switch view for AUX3 & 4
    	if (mActiveMwcVersion >= MwcConstants.MWC_VERSION_20) {
    	    LayoutInflater.from(mLinearLayout.getContext()).inflate(
    	        mLinearLayout.getResources().getIdentifier(
    	            "params_switches_" + MwcConstants.MWC_VERSIONS_LIST[mActiveMwcVersion] + "b",
    	            "layout",
    	            "net.xrotor.andmultiwiiconf"), thirdView);
    	} else {
    	       LayoutInflater.from(mLinearLayout.getContext()).inflate(
    	               mLinearLayout.getResources().getIdentifier(
    	                   "no_aux_support",
    	                   "layout",
    	                   "net.xrotor.andmultiwiiconf"), thirdView);
    	}

    	// Set the active Mwc Version on the statusbar text
    	TextView mStatusBarVersion = (TextView) mLinearLayout.findViewById(R.id.status_bar_version);
    	mStatusBarVersion.setText("Mwc: "+ MwcConstants.MWC_VERSIONS_LIST_NAME[mActiveMwcVersion]);

    }

    // Set the values boxes
    public boolean setChkBoxes(MwcParameters mMwcParameters, int mActiveMwcVersion) {

        Map<String, Integer> MWC_BOX_LIST = null;

        switch (mActiveMwcVersion) {
            case MwcConstants.MWC_VERSION_17:
                MWC_BOX_LIST = MwcConstants.MWC_17_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_18:
                MWC_BOX_LIST = MwcConstants.MWC_18_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_19:
                MWC_BOX_LIST = MwcConstants.MWC_19_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_20:
                MWC_BOX_LIST = MwcConstants.MWC_20_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_21:
                MWC_BOX_LIST = MwcConstants.MWC_21_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_22:
                MWC_BOX_LIST = mMwcParameters.boxNameMap;
                break;
            default:
                return false;
        }

        CheckBox chkBox = null;

        // Loop over all the CheckBoxes
        for (String box : MWC_BOX_LIST.keySet()) {
            // Loop over all The Channels Aux1 - 4
            for (String channel : MwcConstants.MWC_BOX_CHANNEL.keySet()) {
                // Loop over all the Positions low - mid - high
                for (String position : MwcConstants.MWC_BOX_POSITION.keySet()) {
                    String chkTag = box + "_" + channel + "_" + position;
                    //if (D) Log.d(TAG, "Fetching tag: " + chkTag);
                    chkBox = (CheckBox) mLinearLayout.findViewWithTag(chkTag);
                    if(chkBox != null) {
                        chkBox.setChecked(mMwcParameters.getBoxActivation(
                                MWC_BOX_LIST.get(box),
                                MwcConstants.MWC_BOX_CHANNEL.get(channel),
                                MwcConstants.MWC_BOX_POSITION.get(position)
                                ));
                        if (D) Log.d(TAG, "Set BOX: " + MWC_BOX_LIST.get(box)
                                + " Channel: " + MwcConstants.MWC_BOX_CHANNEL.get(channel)
                                + " Position: " + MwcConstants.MWC_BOX_POSITION.get(position));
                    }
                }
            }
        }
        return true;
    }

    // Get method to retrieve the current state of all the boxes
    public boolean getChkBoxes(MwcParameters mMwcParameters, int mActiveMwcVersion) {

        Map<String, Integer> MWC_BOX_LIST = null;

        switch (mActiveMwcVersion) {
            case MwcConstants.MWC_VERSION_17:
                MWC_BOX_LIST = MwcConstants.MWC_17_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_18:
                MWC_BOX_LIST = MwcConstants.MWC_18_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_19:
                MWC_BOX_LIST = MwcConstants.MWC_19_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_20:
                MWC_BOX_LIST = MwcConstants.MWC_20_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_21:
                MWC_BOX_LIST = MwcConstants.MWC_21_BOX_LIST;
                break;
            case MwcConstants.MWC_VERSION_22:
                MWC_BOX_LIST = mMwcParameters.boxNameMap;
                break;
            default:
                return false;
        }

        CheckBox chkBox = null;

        // Loop over all the CheckBoxes
        for (String box : MWC_BOX_LIST.keySet()) {
            // Loop over all The Channels Aux1 - 4
            for (String channel : MwcConstants.MWC_BOX_CHANNEL.keySet()) {
                // Loop over all the Positions low - mid - high
                for (String position : MwcConstants.MWC_BOX_POSITION.keySet()) {
                    String chkTag = box + "_" + channel + "_" + position;
                    //if (D) Log.d(TAG, "Fetching tag: " + chkTag);
                    chkBox = (CheckBox) mLinearLayout.findViewWithTag(chkTag);
                    if(chkBox != null) {
                        mMwcParameters.setBoxActivation(
                                MWC_BOX_LIST.get(box),
                                MwcConstants.MWC_BOX_CHANNEL.get(channel),
                                MwcConstants.MWC_BOX_POSITION.get(position),
                                chkBox.isChecked()
                                );
                        if (D) Log.d(TAG, "Get BOX: "
                                + MWC_BOX_LIST.get(box)
                                + " Channel: " + MwcConstants.MWC_BOX_CHANNEL.get(channel)
                                + " Position: " + MwcConstants.MWC_BOX_POSITION.get(position));
                    }
                }
            }
        }
        return true;
    }

    // Set the params in the UI
    public boolean setUIParams(MwcParameters mMwcParameters, int mActiveMwcVersion){

    	if (mLinearLayout == null) {
    	    Log.d(TAG, "The main layout is not defined");
    	    return false;
    	}

    	if (D) Log.d(TAG, "Setting base params and switches");
    	// Params Layout
    	// ROLL
    	RCEditText edtValRoll_P = (RCEditText) mLinearLayout.findViewById(R.id.valRoll_P);
    	edtValRoll_P.setRCData(mMwcParameters.getByteP_ROLL(), RCEditText.PRECISION_SINGLE);
    	RCEditText edtValRoll_I = (RCEditText) mLinearLayout.findViewById(R.id.valRoll_I);
    	edtValRoll_I.setRCData(mMwcParameters.getByteI_ROLL(), RCEditText.PRECISION_TRIPLE);
    	RCEditText edtValRoll_D = (RCEditText) mLinearLayout.findViewById(R.id.valRoll_D);
    	edtValRoll_D.setRCData(mMwcParameters.getByteD_ROLL(), RCEditText.PRECISION_NONE);

    	// PITCH
    	RCEditText edtValPitch_P = (RCEditText) mLinearLayout.findViewById(R.id.valPitch_P);
    	edtValPitch_P.setRCData(mMwcParameters.getByteP_PITCH(), RCEditText.PRECISION_SINGLE);
    	RCEditText edtValPitch_I = (RCEditText) mLinearLayout.findViewById(R.id.valPitch_I);
    	edtValPitch_I.setRCData(mMwcParameters.getByteI_PITCH(), RCEditText.PRECISION_TRIPLE);
    	RCEditText edtValPitch_D = (RCEditText) mLinearLayout.findViewById(R.id.valPitch_D);
    	edtValPitch_D.setRCData(mMwcParameters.getByteD_PITCH(), RCEditText.PRECISION_NONE);

    	// YAW
    	RCEditText edtValYaw_P = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_P);
    	edtValYaw_P.setRCData(mMwcParameters.getByteP_YAW(), RCEditText.PRECISION_SINGLE);
    	RCEditText edtValYaw_I = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_I);
    	edtValYaw_I.setRCData(mMwcParameters.getByteI_YAW(), RCEditText.PRECISION_TRIPLE);
    	RCEditText edtValYaw_D = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_D);
    	edtValYaw_D.setRCData(mMwcParameters.getByteD_YAW(), RCEditText.PRECISION_NONE);

    	// LEVEL
    	RCEditText edtValP_LEVEL = (RCEditText) mLinearLayout.findViewById(R.id.valLevel_P);
    	edtValP_LEVEL.setRCData(mMwcParameters.getByteP_LEVEL(), RCEditText.PRECISION_SINGLE);
    	RCEditText edtValI_LEVEL = (RCEditText) mLinearLayout.findViewById(R.id.valLevel_I);
    	edtValI_LEVEL.setRCData(mMwcParameters.getByteI_LEVEL(), RCEditText.PRECISION_TRIPLE);

    	// ROLL_PITCH_RATE
    	RCEditText edtValRollPitch_RATE = (RCEditText) mLinearLayout.findViewById(R.id.valRollPitch_RATE);
    	edtValRollPitch_RATE.setRCData(mMwcParameters.getRollPitchRate(), RCEditText.PRECISION_DOUBLE);

    	// YAW_RATE
    	RCEditText edtValYaw_RATE = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_Rate);
    	edtValYaw_RATE.setRCData(mMwcParameters.getYawRate(), RCEditText.PRECISION_DOUBLE);

    	// RC_RATE
    	RCEditText edtRCRate = (RCEditText) mLinearLayout.findViewById(R.id.valRCRate);
    	edtRCRate.setRCData(mMwcParameters.getRC_RATE(), RCEditText.PRECISION_DOUBLE);

    	// RC_EXPO
    	RCEditText edtRCExpo = (RCEditText) mLinearLayout.findViewById(R.id.valRCExpo);
    	edtRCExpo.setRCData(mMwcParameters.getRC_EXPO(), RCEditText.PRECISION_DOUBLE);

    	// THR_PID_ATT
    	RCEditText edtThrPIDAttenuation = (RCEditText) mLinearLayout.findViewById(R.id.valThrPIDAttenuation);
    	edtThrPIDAttenuation.setRCData(mMwcParameters.getDynThrPID(), RCEditText.PRECISION_DOUBLE);

    	// ---------------------------------------
    	if (mActiveMwcVersion >= MwcConstants.MWC_VERSION_18) {
    	    if (D) Log.d(TAG, "Setting params for ver 1.8+");

    	    // ALT
    	    RCEditText edtValP_ALT = (RCEditText) mLinearLayout.findViewById(R.id.valAlt_P);
    	    edtValP_ALT.setRCData(mMwcParameters.getByteP_ALT(), RCEditText.PRECISION_SINGLE);
    	    RCEditText edtValI_ALT = (RCEditText) mLinearLayout.findViewById(R.id.valAlt_I);
    	    edtValI_ALT.setRCData(mMwcParameters.getByteI_ALT(), RCEditText.PRECISION_TRIPLE);
    	    RCEditText edtValD_ALT = (RCEditText) mLinearLayout.findViewById(R.id.valAlt_D);
    	    edtValD_ALT.setRCData(mMwcParameters.getByteD_ALT(), RCEditText.PRECISION_NONE);

    	    // VEL
    	    // check first if version is 2.1+ as VEL no longuer exists...
    	    if (mActiveMwcVersion < MwcConstants.MWC_VERSION_21) {
    	        RCEditText edtValP_VEL = (RCEditText) mLinearLayout.findViewById(R.id.valVel_P);
    	        edtValP_VEL.setRCData(mMwcParameters.getByteP_VEL(), RCEditText.PRECISION_SINGLE);
    	        RCEditText edtValI_VEL = (RCEditText) mLinearLayout.findViewById(R.id.valVel_I);
    	        edtValI_VEL.setRCData(mMwcParameters.getByteI_VEL(), RCEditText.PRECISION_TRIPLE);
    	        RCEditText edtValD_VEL = (RCEditText) mLinearLayout.findViewById(R.id.valVel_D);
    	        edtValD_VEL.setRCData(mMwcParameters.getByteD_VEL(), RCEditText.PRECISION_NONE);
    	    }

    	    // MAG
    	    RCEditText edtValP_MAG = (RCEditText) mLinearLayout.findViewById(R.id.valMag_P);
    	    edtValP_MAG.setRCData(mMwcParameters.getByteP_MAG(), RCEditText.PRECISION_SINGLE);

    	}

    	// ---------------------------------------
    	if (mActiveMwcVersion >= MwcConstants.MWC_VERSION_20) {
    	    if (D) Log.d(TAG, "Setting params for ver 2.0+");
            // GPS
    	    // check version first and GPS no longer exists in 2.1+...
    	    if (mActiveMwcVersion < MwcConstants.MWC_VERSION_21) {
    	        RCEditText edtValP_GPS = (RCEditText) mLinearLayout.findViewById(R.id.valGps_P);
    	        edtValP_GPS.setRCData(mMwcParameters.getByteP_GPS(), RCEditText.PRECISION_SINGLE);
    	        RCEditText edtValI_GPS = (RCEditText) mLinearLayout.findViewById(R.id.valGps_I);
    	        edtValI_GPS.setRCData(mMwcParameters.getByteI_GPS(), RCEditText.PRECISION_TRIPLE);
    	        RCEditText edtValD_GPS = (RCEditText) mLinearLayout.findViewById(R.id.valGps_D);
    	        edtValD_GPS.setRCData(mMwcParameters.getByteD_GPS(), RCEditText.PRECISION_NONE);
    	    }

    	    // LEVEL_D
    	    RCEditText edtValD_LEVEL = (RCEditText) mLinearLayout.findViewById(R.id.valLevel_D);
    	    edtValD_LEVEL.setRCData(mMwcParameters.getByteD_LEVEL(), RCEditText.PRECISION_NONE);

    	}
  	// ---------------------------------------
        if (mActiveMwcVersion >= MwcConstants.MWC_VERSION_21) {
            if (D) Log.d(TAG, "Setting params for ver 2.1+");

            // POS
            RCEditText edtValP_POS = (RCEditText) mLinearLayout.findViewById(R.id.valPos_P);
            edtValP_POS.setRCData(mMwcParameters.getByteP_POS(), RCEditText.PRECISION_SINGLE);
            RCEditText edtValI_POS = (RCEditText) mLinearLayout.findViewById(R.id.valPos_I);
            edtValI_POS.setRCData(mMwcParameters.getByteI_POS(), RCEditText.PRECISION_DOUBLE);

            // POSR
            RCEditText edtValP_POSR = (RCEditText) mLinearLayout.findViewById(R.id.valPosR_P);
            edtValP_POSR.setRCData(mMwcParameters.getByteP_POSR(), RCEditText.PRECISION_SINGLE);
            RCEditText edtValI_POSR = (RCEditText) mLinearLayout.findViewById(R.id.valPosR_I);
            edtValI_POSR.setRCData(mMwcParameters.getByteI_POSR(), RCEditText.PRECISION_DOUBLE);
            RCEditText edtValD_POSR = (RCEditText) mLinearLayout.findViewById(R.id.valPosR_D);
            edtValD_POSR.setRCData(mMwcParameters.getByteD_POSR(), RCEditText.PRECISION_TRIPLE);

            // NAVR
            RCEditText edtValP_NAVR = (RCEditText) mLinearLayout.findViewById(R.id.valNavR_P);
            edtValP_NAVR.setRCData(mMwcParameters.getByteP_NAVR(), RCEditText.PRECISION_SINGLE);
            RCEditText edtValI_NAVR = (RCEditText) mLinearLayout.findViewById(R.id.valNavR_I);
            edtValI_NAVR.setRCData(mMwcParameters.getByteI_NAVR(), RCEditText.PRECISION_DOUBLE);
            RCEditText edtValD_NAVR = (RCEditText) mLinearLayout.findViewById(R.id.valNavR_D);
            edtValD_NAVR.setRCData(mMwcParameters.getByteD_NAVR(), RCEditText.PRECISION_TRIPLE);

            // THR MID
            RCEditText edtValTHR_MID = (RCEditText) mLinearLayout.findViewById(R.id.valTHRMid);
            edtValTHR_MID.setRCData(mMwcParameters.getByteThr_MID(), RCEditText.PRECISION_DOUBLE);

            // THR EXPO
            RCEditText edtValTHR_EXPO = (RCEditText) mLinearLayout.findViewById(R.id.valThrExpo);
            edtValTHR_EXPO.setRCData(mMwcParameters.getByteThr_EXPO(), RCEditText.PRECISION_DOUBLE);

            // PALARM
            RCEditText edtValPALARM = (RCEditText) mLinearLayout.findViewById(R.id.valPAlarm);
            edtValPALARM.setRCData(mMwcParameters.getByteP_ALARM(), RCEditText.PRECISION_NONE);

        }

        // Set the state to the Box's
    	setChkBoxes(mMwcParameters, mActiveMwcVersion);
        return true;

    }

    /**
     * Method to retrieve the BoxNames to populate the UI
     *
     * @param boxNames
     */
    public void updateBoxNames(String[] boxNames) {

        TableLayout tableLayout = (TableLayout) mLinearLayout.findViewById(R.id.ratesFullTable);
        LayoutInflater inflater = LayoutInflater.from(mLinearLayout.getContext());

        //String boxNameTest = "box_arm";
        String[] testBboxNames = new String[] {"box_arm", "box_baro"};

        for (String boxName : testBboxNames) {
            //TableRow tableRow = (TableRow) inflater.inflate(R.layout.box_arm, null, false);
            TableRow tableRow = (TableRow) inflater.inflate(
                    mLinearLayout.getResources().getIdentifier(boxName,
                            "view",
                            mLinearLayout.getContext().getPackageName()) ,
                            null,
                            false);
            tableLayout.addView(tableRow);
        }

    }

    protected void getUIParams(MwcParameters mMwcParameters, int mActiveMwcVersion) {

	// Go over all EditTexts
	/*
        ViewGroup mViewGroup = (ViewGroup) findViewById(R.layout.paramsFullTable);
        int viewcount = mViewGroup.getChildCount();

        for (int i=0; i < viewcount; i++) {
        	Log.i(TAG, "View id: " + mViewGroup.getChildAt(i) );
        }
	 */

	// ---------------------------------------
	try {
	    if (D) Log.d(TAG, "Getting base params.");

	    // ROLL
	    RCEditText edtValRoll_P = (RCEditText) mLinearLayout.findViewById(R.id.valRoll_P);
	    mMwcParameters.setByteP_ROLL( edtValRoll_P.getRCData() );
	    RCEditText edtValRoll_I = (RCEditText) mLinearLayout.findViewById(R.id.valRoll_I);
	    mMwcParameters.setByteI_ROLL( edtValRoll_I.getRCData() );
	    RCEditText edtValRoll_D = (RCEditText) mLinearLayout.findViewById(R.id.valRoll_D);
	    mMwcParameters.setByteD_ROLL( edtValRoll_D.getRCData() );

	    // PITCH
	    RCEditText edtValPitch_P = (RCEditText) mLinearLayout.findViewById(R.id.valPitch_P);
	    mMwcParameters.setByteP_PITCH( edtValPitch_P.getRCData() );
	    RCEditText edtValPitch_I = (RCEditText) mLinearLayout.findViewById(R.id.valPitch_I);
	    mMwcParameters.setByteI_PITCH( edtValPitch_I.getRCData() );
	    RCEditText edtValPitch_D = (RCEditText) mLinearLayout.findViewById(R.id.valPitch_D);
	    mMwcParameters.setByteD_PITCH( edtValPitch_D.getRCData() );

	    // YAW
	    RCEditText edtValYaw_P = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_P);
	    mMwcParameters.setByteP_YAW( edtValYaw_P.getRCData() );
	    RCEditText edtValYaw_I = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_I);
	    mMwcParameters.setByteI_YAW( edtValYaw_I.getRCData() );
	    RCEditText edtValYaw_D = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_D);
	    mMwcParameters.setByteD_YAW( edtValYaw_D.getRCData() );

	    // LEVEL
	    RCEditText edtValP_LEVEL = (RCEditText) mLinearLayout.findViewById(R.id.valLevel_P);
	    mMwcParameters.setByteP_LEVEL( edtValP_LEVEL.getRCData() );
	    RCEditText edtValI_LEVEL = (RCEditText) mLinearLayout.findViewById(R.id.valLevel_I);
	    mMwcParameters.setByteI_LEVEL( edtValI_LEVEL.getRCData() );

	    // EXTRA
	    RCEditText edtValRollPitch_RATE = (RCEditText) mLinearLayout.findViewById(R.id.valRollPitch_RATE);
	    mMwcParameters.setByteRollPitchRate( edtValRollPitch_RATE.getRCData() );
	    RCEditText edtValYaw_RATE = (RCEditText) mLinearLayout.findViewById(R.id.valYaw_Rate);
	    mMwcParameters.setByteYawRate( edtValYaw_RATE.getRCData() );
	    RCEditText edtRCRate = (RCEditText) mLinearLayout.findViewById(R.id.valRCRate);
	    mMwcParameters.setByteRC_RATE( edtRCRate.getRCData() );
	    RCEditText edtRCExpo = (RCEditText) mLinearLayout.findViewById(R.id.valRCExpo);
	    mMwcParameters.setByteRC_EXPO( edtRCExpo.getRCData() );
	    RCEditText edtDynThrPIDAttenuation = (RCEditText) mLinearLayout.findViewById(R.id.valThrPIDAttenuation);
	    mMwcParameters.setByteDynThrPID( edtDynThrPIDAttenuation.getRCData() );

	    // ---------------------------------------
	    if (mActiveMwcVersion >= MwcConstants.MWC_VERSION_18) {
	        if (D) Log.d(TAG, "Getting params for ver 1.8+");

		    // ALT
		    RCEditText edtValALT_P = (RCEditText) mLinearLayout.findViewById(R.id.valAlt_P);
		    mMwcParameters.setByteP_ALT( edtValALT_P.getRCData() );
		    RCEditText edtValALT_I = (RCEditText) mLinearLayout.findViewById(R.id.valAlt_I);
		    mMwcParameters.setByteI_ALT( edtValALT_I.getRCData() );
		    RCEditText edtValALT_D = (RCEditText) mLinearLayout.findViewById(R.id.valAlt_D);
		    mMwcParameters.setByteD_ALT( edtValALT_D.getRCData() );

		if (mActiveMwcVersion <= MwcConstants.MWC_VERSION_20) {
    		// VEL
    		RCEditText edtValVEL_P = (RCEditText) mLinearLayout.findViewById(R.id.valVel_P);
    		mMwcParameters.setByteP_VEL( edtValVEL_P.getRCData() );
    		RCEditText edtValVEL_I = (RCEditText) mLinearLayout.findViewById(R.id.valVel_I);
    		mMwcParameters.setByteI_VEL( edtValVEL_I.getRCData() );
    		RCEditText edtValVEL_D = (RCEditText) mLinearLayout.findViewById(R.id.valVel_D);
    		mMwcParameters.setByteD_VEL( edtValVEL_D.getRCData() );
    	}
		// MAG
		    RCEditText edtValP_MAG = (RCEditText) mLinearLayout.findViewById(R.id.valMag_P);
		    mMwcParameters.setByteP_MAG( edtValP_MAG.getRCData() );
	    }

	    if (mActiveMwcVersion == MwcConstants.MWC_VERSION_20) {
		if (D) Log.d(TAG, "Getting params for ver 2.0");

    		// GPS
    		RCEditText edtValP_GPS = (RCEditText) mLinearLayout.findViewById(R.id.valGps_P);
    		mMwcParameters.setByteP_GPS( edtValP_GPS.getRCData() );
    		RCEditText edtValI_GPS = (RCEditText) mLinearLayout.findViewById(R.id.valGps_I);
    		mMwcParameters.setByteI_GPS( edtValI_GPS.getRCData() );
    		RCEditText edtValD_GPS = (RCEditText) mLinearLayout.findViewById(R.id.valGps_D);
    		mMwcParameters.setByteD_GPS( edtValD_GPS.getRCData() );

    		RCEditText edtValD_LEVEL = (RCEditText) mLinearLayout.findViewById(R.id.valLevel_D);
    		mMwcParameters.setByteD_LEVEL( edtValD_LEVEL.getRCData() );
	    }

	    if (mActiveMwcVersion >= MwcConstants.MWC_VERSION_20) {
	        if (D) Log.d(TAG, "Getting params for ver 2.0+");

	       RCEditText edtValD_LEVEL = (RCEditText) mLinearLayout.findViewById(R.id.valLevel_D);
	       mMwcParameters.setByteD_LEVEL( edtValD_LEVEL.getRCData() );
	    }

	    if (mActiveMwcVersion >= MwcConstants.MWC_VERSION_21) {
	        if (D) Log.d(TAG, "Getting params for ver 2.0+");

            // POS
	        RCEditText edtValP_POS = (RCEditText) mLinearLayout.findViewById(R.id.valPos_P);
	        mMwcParameters.setByteP_POS( edtValP_POS.getRCData() );
	        RCEditText edtValI_POS = (RCEditText) mLinearLayout.findViewById(R.id.valPos_I);
	        mMwcParameters.setByteI_POS( edtValI_POS.getRCData() );

            // POSR
            RCEditText edtValP_POSR = (RCEditText) mLinearLayout.findViewById(R.id.valPosR_P);
            mMwcParameters.setByteP_POSR( edtValP_POSR.getRCData() );
            RCEditText edtValI_POSR = (RCEditText) mLinearLayout.findViewById(R.id.valPosR_I);
            mMwcParameters.setByteI_POSR( edtValI_POSR.getRCData() );
            RCEditText edtValD_POSR = (RCEditText) mLinearLayout.findViewById(R.id.valPosR_D);
            mMwcParameters.setByteD_POSR( edtValD_POSR.getRCData() );

            // NAVR
            RCEditText edtValP_NAVR = (RCEditText) mLinearLayout.findViewById(R.id.valNavR_P);
            mMwcParameters.setByteP_NAVR( edtValP_NAVR.getRCData() );
            RCEditText edtValI_NAVR = (RCEditText) mLinearLayout.findViewById(R.id.valNavR_I);
            mMwcParameters.setByteI_NAVR( edtValI_NAVR.getRCData() );
            RCEditText edtValD_NAVR = (RCEditText) mLinearLayout.findViewById(R.id.valNavR_D);
            mMwcParameters.setByteD_NAVR( edtValD_NAVR.getRCData() );

            // THR MID
            RCEditText edtValTHR_MID = (RCEditText) mLinearLayout.findViewById(R.id.valTHRMid);
            mMwcParameters.setByteTHR_MID( edtValTHR_MID.getRCData() );

            // THR EXPO
            RCEditText edtValTHR_EXPO = (RCEditText) mLinearLayout.findViewById(R.id.valThrExpo);
            mMwcParameters.setByteTHR_EXPO(edtValTHR_EXPO.getRCData());

            // APARAM
            RCEditText edtValP_ALARM = (RCEditText) mLinearLayout.findViewById(R.id.valPAlarm);
            mMwcParameters.setByteP_ALARM(edtValP_ALARM.getRCData());
	    }

	} catch (Exception e) {
	    Log.d(TAG, "Error: ", e);
	}
    }

    /**
     * Toggle state Enabled of elements in the UI
     *
     * @param btnState
     */
    protected void toggleButtonsState(Boolean btnState) {

	ViewFlipper mViewFlipper = (ViewFlipper) mLinearLayout.findViewById(R.id.viewFlipperMain);

	// Make sure we're not in the Monitor pages.
	if (mViewFlipper.getDisplayedChild() < 1) {
	    Button buttonRead = (Button) mLinearLayout.findViewById(R.id.btnParamsRead);
	    buttonRead.setEnabled(btnState); // Toggle current state

	    Button buttonWrite = (Button) mLinearLayout.findViewById(R.id.btnParamsWrite);
	    buttonWrite.setEnabled(btnState); // Toggle current state
	}
    }

    /**
     * Update the position of the Channels and Motors bars
     *
     * @param mActiveMwcVersion
     */
    public void updateChannelsBars(float[] channelData, int mActiveMwcVersion) {

    	// Motor 1
    	barMotor1.SetRCData((int) channelData[0]);
    	// Motor 2
    	barMotor2.SetRCData((int) channelData[1]);
    	// Motor 3
    	barMotor3.SetRCData((int) channelData[2]);
    	// Motor 4
    	barMotor4.SetRCData((int) channelData[3]);
    	// Motor 5
    	barMotor5.SetRCData((int) channelData[4]);
    	// Motor 6
    	barMotor6.SetRCData((int) channelData[5]);
    	// Motor 7
    	barMotor7.SetRCData((int) channelData[6]);
    	// Motor 8
    	barMotor8.SetRCData((int) channelData[7]);

    	// RC Roll
    	barRoll.SetRCData((int) channelData[8]);
    	// RC Pitch
    	barPitch.SetRCData((int) channelData[9]);
    	// RC Yaw
    	barYaw.SetRCData((int) channelData[10]);
    	// RC Throttle
    	barThrottle.SetRCData((int) channelData[11]);
    	//RC AUX1
    	barAux1.SetRCData((int) channelData[12]);
    	// RC AUX2
    	barAux2.SetRCData((int) channelData[13]);
    	//RC AUX3
    	barAux3.SetRCData((int) channelData[14]);
    	// RC AUX4
    	barAux4.SetRCData((int) channelData[15]);
    }

    public boolean saveScreenshot(View view){

    	Bitmap viewBitmap = Bitmap.createBitmap( view.getWidth(), view.getHeight
    		(), Bitmap.Config.ARGB_8888);

    	Canvas myCanvas = new Canvas( viewBitmap );

    	view.draw( myCanvas );

    	FileOutputStream myfileOutputStream = null;
    	String extStorageDirectory = Environment.getExternalStorageDirectory().toString();

    	try {

    	    myfileOutputStream = new FileOutputStream(extStorageDirectory + MwcConstants.SCREENSHOTS_PATH + MwcConstants.SCREENSHOT_NAME
    		    + System.currentTimeMillis() + ".png");

    	    if (myfileOutputStream != null) {
    		viewBitmap.compress(Bitmap.CompressFormat.PNG, 100, myfileOutputStream);

    		myfileOutputStream.close();
    	    }

    	} catch (Exception e) {
    	    Log.e("Saving screenshot", "Exception: " + e.toString());
    	    return false;
    	}
    	return true;
    }

    /**
     * @param buttonsMonitor
     */
    public static void loadButtons(int buttonsMonitor) {

      if (D) Log.d(TAG, "Loading buttons for view: " + buttonsMonitor);

      ViewGroup buttonsView = (ViewGroup) mLinearLayout.findViewById(R.id.statusbar_buttons);
      buttonsView.removeAllViews();

      switch (buttonsMonitor) {
        case BUTTONS_PARAMS:
          //Animation viewAnimation = AnimationUtils.makeInChildBottomAnimation(mLinearLayout.getContext());
          //viewAnimation.setDuration(300);

          LayoutInflater.from(mLinearLayout.getContext()).inflate(
              mLinearLayout.getResources().getIdentifier(
                  "params_buttons",
                  "layout",
                  "net.xrotor.andmultiwiiconf"), buttonsView);
          break;

        case BUTTONS_MONITOR:
          LayoutInflater.from(mLinearLayout.getContext()).inflate(
              mLinearLayout.getResources().getIdentifier(
                  "monitor_buttons",
                  "layout",
                  "net.xrotor.andmultiwiiconf"), buttonsView);
          break;
      }
      buttonsView.invalidate(); // Force a re-draw
    }

    public void updateSensorState(boolean[] sensorState) {

        // Sensor ACC
        if(sensorState[0] == true) {
            btnACC.setBackgroundResource(R.drawable.sensor_on);
        } else {
            btnACC.setBackgroundResource(R.drawable.sensor_off);
        }

        // Sensor BARO
        if(sensorState[1] == true) {
            btnBARO.setBackgroundResource(R.drawable.sensor_on);
        } else {
            btnBARO.setBackgroundResource(R.drawable.sensor_off);
        }

        // Sensor MAG
        if(sensorState[2] == true) {
            btnMAG.setBackgroundResource(R.drawable.sensor_on);
        } else {
            btnMAG.setBackgroundResource(R.drawable.sensor_off);
        }

        // Sensor GPS
        if(sensorState[3] == true) {
            btnGPS.setBackgroundResource(R.drawable.sensor_on);
        } else {
            btnGPS.setBackgroundResource(R.drawable.sensor_off);
        }

        // Sensor SONAR
        if(sensorState[4] == true) {
            btnSONAR.setBackgroundResource(R.drawable.sensor_on);
        } else {
            btnSONAR.setBackgroundResource(R.drawable.sensor_off);
        }
    }

    /**
     * Defines if the screen should be kept always on.
     * Setting the main view with the passed state
     * @netValue boolean representing the state
     */
    public static void setKeepScreenOn(boolean newValue) {
      mLinearLayout.setKeepScreenOn(newValue);
    }
    /*
	// Functions to handle screen orientation
	protected int getScreenOrientation() {
	      Display display = getWindowManager().getDefaultDisplay();
	      int orientation = display.getOrientation();

	      if (orientation == Configuration.ORIENTATION_UNDEFINED) {
	         orientation = getResources().getConfiguration().orientation;

	         if (orientation == Configuration.ORIENTATION_UNDEFINED) {
	            if (display.getWidth() == display.getHeight())
	               orientation = Configuration.ORIENTATION_SQUARE;
	            else if(display.getWidth() < display.getHeight())
	               orientation = Configuration.ORIENTATION_PORTRAIT;
	            else
	               orientation = Configuration.ORIENTATION_LANDSCAPE;
	            }
	         }
	      return orientation;
	   }
     */
/*
    public void customTitleBar(Activity activity, String left, String right) {
        if (right.length() > 20) right = right.substring(0, 20);
        // set up custom title
        if (activity.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE)) {
            activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
            activity.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
                    R.layout.actionbar);
            TextView titleTvLeft = (TextView) mLinearLayout.findViewById (R.id.titleTvLeft);
            TextView titleTvRight = (TextView) mLinearLayout.findViewById(R.id.titleTvRight);

            titleTvLeft.setText(left);
            titleTvRight.setText(right);

            ProgressBar titleProgressBar;
            titleProgressBar = (ProgressBar) mLinearLayout.findViewById(R.id.actionProgressBar);

            //hide the progress bar if it is not needed
            titleProgressBar.setVisibility(ProgressBar.GONE);
        }
    }
*/
    // Show ABOUT APP popup
    public void showAboutApp(Activity activity) {
        String message = "  Android MultiWiiCopter\n"
                + "    Configuration Tool\n" + " \n"
                + " Nando \"IceWind\" @2011\n" + " \n" + " Thanks to:\n"
                + " David \"Qrome\"\n"
                + " \n"
                + "GraphView library by jjoe64"
                + "\n"
                + " \n" + " visit us at:"
                + " www.xRotor.net\n";

        final SpannableString s = new SpannableString(message);
        Linkify.addLinks(s, Linkify.WEB_URLS);

        final AlertDialog d = new AlertDialog.Builder(activity)
        .setTitle(activity.getString(R.string.app_name) + " ver." + this.getVersion(activity))
        // .setCancelable(true)
        // .setIcon(android.R.drawable.ic_dialog_info)
        // .setPositiveButton(R.string.dialog_action_dismiss, null)
        .setMessage(s).show();

        // Make the textview clickable. Must be called after show()
        ((TextView) d.findViewById(android.R.id.message))
        .setMovementMethod(LinkMovementMethod.getInstance());
    }

    // Retrieves own Package version
    private String getVersion(Activity activity) {
        String version = "";
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(), PackageManager.GET_META_DATA);
            version = pInfo.versionName;
        } catch (NameNotFoundException e1) {
            Log.e(this.getClass().getSimpleName(), "Name not found", e1);
        }
        return version;
    }

    protected void dotViewChange(int displayedChild, int previousChild, boolean mMonitorState) {
        if (D)
            Log.i(TAG, "New displayed child: " + displayedChild + " previous child: "
                    + previousChild);

        if (previousChild == 3) {
            // Swap the buttons
            MwcUIHelper.loadButtons(MwcUIHelper.BUTTONS_PARAMS);
        }

        TextView leftDot = (TextView) mLinearLayout.findViewById(R.id.flipperDotLeft);
        TextView rightDot = (TextView) mLinearLayout.findViewById(R.id.flipperDotRight);

        switch (displayedChild) {
            case 0:
                leftDot.setTextAppearance(mLinearLayout.getContext(),
                        R.style.ViewFlipperDotsOn);
                rightDot.setTextAppearance(mLinearLayout.getContext(),
                        R.style.ViewFlipperDotsOff);
                break;
            case 1:
                leftDot.setTextAppearance(mLinearLayout.getContext(),
                        R.style.ViewFlipperDotsOff);
                rightDot.setTextAppearance(mLinearLayout.getContext(),
                        R.style.ViewFlipperDotsOn);
                break;
            case 3:
                // Load new status bar
                MwcUIHelper.loadButtons(MwcUIHelper.BUTTONS_MONITOR);
                // Set the state of the button from the mMonitorState flag var
                ToggleButton toggleMonitorButtonB = (ToggleButton) mLinearLayout.findViewById(R.id.tglMonitor);
                toggleMonitorButtonB.setChecked(mMonitorState);
                break;
            case 4:
                // Load new status bar
                MwcUIHelper.loadButtons(MwcUIHelper.BUTTONS_MONITOR);
                // Set the state of the button from the mMonitorState flag var
                ToggleButton toggleMonitorButtonC = (ToggleButton) mLinearLayout.findViewById(R.id.tglMonitor);
                toggleMonitorButtonC.setChecked(mMonitorState);
                break;
        }
    }

    public Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    public Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    public Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }

}
