package net.xrotor.andmultiwiiconf;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphView.LegendAlign;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;

/**
 * @author nando@xrotor.net (Fernando Morais)
 *
 */
public class MwcGraph {

  //private static final String TAG = "AndMultiWiiConf.Graph";
  //private static final boolean D = true;

  // Sensor Graph - default values
  private static int SENSOR_SERIES_SIZE = 50;
  private static double SENSOR_SERIES_MAX_VALUE = 500;
  private static double SENSOR_SERIES_MIN_VALUE = -500;
  private static GraphView graphView;
  //private static List<Double> seriesGyroX, seriesGyroY, seriesGyroZ, seriesAccX, seriesAccY, seriesAccZ;
  //private static Queue<Double> seriesGyroX, seriesGyroY, seriesGyroZ, seriesAccX, seriesAccY, seriesAccZ;
  //private static GraphViewData[] dataGX, dataGY, dataGZ, dataAX, dataAY, dataAZ;
  private volatile GraphViewSeries seriesGX, seriesGY, seriesGZ, seriesAX, seriesAY, seriesAZ;
  private Context mainContext;

  /**
   * MwcGraph constructor
   *
   * @param mainContext main application context
   * @param seriesSize set the amount of samples for each serie.
   * @param mwcSeriesMinValue define the minimun expected value per pair
   * @param mwcSeriesMaxValue define the maximum expected value per pair
   */
  public MwcGraph(Context mainContext, int seriesSize, double mwcSeriesMinValue, double mwcSeriesMaxValue) {
    this.mainContext = mainContext;
    MwcGraph.SENSOR_SERIES_SIZE = seriesSize;
    MwcGraph.SENSOR_SERIES_MIN_VALUE = mwcSeriesMinValue;
    MwcGraph.SENSOR_SERIES_MAX_VALUE = mwcSeriesMaxValue;
  }

  /**
   * Initializes the Sensor Graph View
   */
  public void initSensorGraph() {
      /*
      seriesGyroX = new CircularArrayList<Double>(SENSOR_SERIES_SIZE);
      seriesGyroY = new CircularArrayList<Double>(SENSOR_SERIES_SIZE);
      seriesGyroZ = new CircularArrayList<Double>(SENSOR_SERIES_SIZE);
      seriesAccX  = new CircularArrayList<Double>(SENSOR_SERIES_SIZE);
      seriesAccY  = new CircularArrayList<Double>(SENSOR_SERIES_SIZE);
      seriesAccZ  = new CircularArrayList<Double>(SENSOR_SERIES_SIZE);

      seriesGyroX = new PriorityQueue<Double>(SENSOR_SERIES_SIZE);
      seriesGyroY = new PriorityQueue<Double>(SENSOR_SERIES_SIZE);
      seriesGyroZ = new PriorityQueue<Double>(SENSOR_SERIES_SIZE);
      seriesAccX  = new PriorityQueue<Double>(SENSOR_SERIES_SIZE);
      seriesAccY  = new PriorityQueue<Double>(SENSOR_SERIES_SIZE);
      seriesAccZ  = new PriorityQueue<Double>(SENSOR_SERIES_SIZE);

      // Sensor Gyro
      dataGX = new GraphViewData[SENSOR_SERIES_SIZE];
      dataGY = new GraphViewData[SENSOR_SERIES_SIZE];
      dataGZ = new GraphViewData[SENSOR_SERIES_SIZE];

      // Sensor Acc
      dataAX = new GraphViewData[SENSOR_SERIES_SIZE];
      dataAY = new GraphViewData[SENSOR_SERIES_SIZE];
      dataAZ = new GraphViewData[SENSOR_SERIES_SIZE];
      */
      GraphViewData[] data = new GraphViewData[SENSOR_SERIES_SIZE];
      for (int i = 0; i < SENSOR_SERIES_SIZE; i++) {
          //data[i] = new GraphViewData(i, 0);
          data[i] = new GraphViewData(i, (SENSOR_SERIES_MIN_VALUE + (int)(Math.random() * ((SENSOR_SERIES_MAX_VALUE - SENSOR_SERIES_MIN_VALUE) + 1))));
          /*
          seriesGyroX.add((double) 10);
          seriesGyroY.add((double) 20);
          seriesGyroZ.add((double) 30);
          seriesAccX.add((double) 30);
          seriesAccY.add((double) 30);
          seriesAccZ.add((double) 30);
          */
      }

      // Sensor Gyro
      seriesGX = new GraphViewSeries("GyroX", new GraphViewSeriesStyle(Color.rgb(201, 51, 52), 3), data);
      seriesGY = new GraphViewSeries("GyroY", new GraphViewSeriesStyle(Color.rgb(249, 147, 39), 3), data);
      seriesGZ = new GraphViewSeries("GyroZ", new GraphViewSeriesStyle(Color.rgb(252, 255, 71), 3), data);
      // Sensor Acc
      seriesAX = new GraphViewSeries("AccX", new GraphViewSeriesStyle(Color.rgb(0, 253, 59), 3), data);
      seriesAY = new GraphViewSeries("AccY", new GraphViewSeriesStyle(Color.rgb(11, 251, 248), 3), data);
      seriesAZ = new GraphViewSeries("AccZ", new GraphViewSeriesStyle(Color.rgb(255, 254, 252), 3), data);

      graphView = new LineGraphView(
              mainContext // App Context
              , "Sensors Monitor" // Graph Header
              );

      // Add data
      graphView.addSeries(seriesGX);
      graphView.addSeries(seriesGY);
      graphView.addSeries(seriesGZ);

      graphView.addSeries(seriesAX);
      graphView.addSeries(seriesAY);
      graphView.addSeries(seriesAZ);

      // Set view port, start=2, size=10
      //graphView.setViewPort(2, 10);
      //graphView.setScalable(true);
      graphView.setManualYAxisBounds(SENSOR_SERIES_MAX_VALUE, SENSOR_SERIES_MIN_VALUE);
      // optional - legend
      graphView.setShowLegend(true);
      graphView.setLegendAlign(LegendAlign.BOTTOM);
      graphView.setHorizontalLabels(null);

  }

  /**
   * Update the values for the sensors
   *
   * @param sensors float[] containing the sensor data
   */
  public void updateSensorGraph(float[] sensors) {
    /*
    seriesGyroX.remove(0);
    seriesGyroY.remove(0);
    seriesGyroZ.remove(0);
    seriesAccX.remove(0);
    seriesAccY.remove(0);
    seriesAccZ.remove(0);

    seriesGyroX.remove();
    seriesGyroY.remove();
    seriesGyroZ.remove();
    seriesAccX.remove();
    seriesAccY.remove();
    seriesAccZ.remove();

    seriesGyroX.add((double) sensors[0]);
    seriesGyroY.add((double) sensors[1]);
    seriesGyroZ.add((double) sensors[2]);
    seriesAccX.add((double) sensors[3]);
    seriesAccY.add((double) sensors[4]);
    seriesAccZ.add((double) sensors[5]);

    for (int index = 0; index < SENSOR_SERIES_SIZE; index++) {

        //dataGX[index] = new GraphViewData(index, seriesGyroX.get(index));
        //dataGY[index] = new GraphViewData(index, seriesGyroY.get(index));
        //dataGZ[index] = new GraphViewData(index, seriesGyroZ.get(index));

        //dataAX[index] = new GraphViewData(index, seriesAccX.get(index));
        //dataAY[index] = new GraphViewData(index, seriesAccY.get(index));
        //dataAZ[index] = new GraphViewData(index, seriesAccZ.get(index));

        dataGX[index] = new GraphViewData(index, seriesGyroX.remove());
        dataGY[index] = new GraphViewData(index, seriesGyroY.remove());
        dataGZ[index] = new GraphViewData(index, seriesGyroZ.remove());

        dataAX[index] = new GraphViewData(index, seriesAccX.remove());
        dataAY[index] = new GraphViewData(index, seriesAccY.remove());
        dataAZ[index] = new GraphViewData(index, seriesAccZ.remove());
    }

    seriesGX = new GraphViewSeries("GyroX", new GraphViewSeriesStyle(Color.rgb(201, 51, 52), 3), dataGX);
    seriesGY = new GraphViewSeries("GyroY", new GraphViewSeriesStyle(Color.rgb(249, 147, 39), 3), dataGY);
    seriesGZ = new GraphViewSeries("GyroZ", new GraphViewSeriesStyle(Color.rgb(252, 255, 71), 3), dataGZ);

    seriesAX = new GraphViewSeries("AccX", new GraphViewSeriesStyle(Color.rgb(0, 253, 59), 3), dataAX);
    seriesAY = new GraphViewSeries("AccY", new GraphViewSeriesStyle(Color.rgb(11, 251, 248), 3), dataAY);
    seriesAZ = new GraphViewSeries("AccZ", new GraphViewSeriesStyle(Color.rgb(255, 254, 252), 3), dataAZ);

    graphView.removeSeries(5);
    graphView.removeSeries(4);
    graphView.removeSeries(3);
    graphView.removeSeries(2);
    graphView.removeSeries(1);
    graphView.removeSeries(0);

    graphView.addSeries(seriesGX);
    graphView.addSeries(seriesGY);
    graphView.addSeries(seriesGZ);

    graphView.addSeries(seriesAX);
    graphView.addSeries(seriesAY);
    graphView.addSeries(seriesAZ);
    */

    //if(D) Log.d(TAG, "Remove First");
    //if(D) Log.d(TAG, "Add new GX: " + sensors[0]);

      //seriesGX.removeFirst();
    seriesGX.appendData(new GraphViewData(SENSOR_SERIES_SIZE, sensors[0]), false, SENSOR_SERIES_SIZE);

    //seriesGY.removeFirst();
    seriesGY.appendData(new GraphViewData(SENSOR_SERIES_SIZE, sensors[1]), false, SENSOR_SERIES_SIZE);

    //seriesGZ.removeFirst();
    seriesGZ.appendData(new GraphViewData(SENSOR_SERIES_SIZE, sensors[2]), false, SENSOR_SERIES_SIZE);

    //seriesAX.removeFirst();
    seriesAX.appendData(new GraphViewData(SENSOR_SERIES_SIZE, sensors[3]), false, SENSOR_SERIES_SIZE);

    //seriesAY.removeFirst();
    seriesAY.appendData(new GraphViewData(SENSOR_SERIES_SIZE, sensors[4]), false,SENSOR_SERIES_SIZE);

    //seriesAZ.removeFirst();
    seriesAZ.appendData(new GraphViewData(SENSOR_SERIES_SIZE, sensors[5]), true, SENSOR_SERIES_SIZE);

    //Log.d(TAG, "Value at pos 199: " + seriesGX.getValueAtPos(199));
    //Log.d(TAG, "Value at pos 199: " + graphView.getSeries(0).getValueAtPos(199));

    // Forces the View update
    //graphView.invalidate();
    //graphView.redrawAll();
}

  /**
   * @return Return instance of the GraphView
   */
  public View getGraphView() {
    return MwcGraph.graphView;
  }

}
