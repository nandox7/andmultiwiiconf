package net.xrotor.andmultiwiiconf;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class ValueChangeDialog extends Dialog implements SeekBar.OnSeekBarChangeListener,
OnClickListener {

    private static final String TAG = "AndMultiWiiConf.ValueChangeDialog";
    private static final boolean D = false;

    public static final int PRECISION_NONE = 0;
    public static final int PRECISION_SINGLE = 1;
    public static final int PRECISION_DOUBLE = 2;
    public static final int PRECISION_TRIPLE = 3;

    // UI Elements
    private static SeekBar valueBar;
    private static TextView valueText;
    private static Button btnOk;
    private static Button btnCancel;
    private static Button btnDown;
    private static Button btnUp;

    private String value;
    private int type;
    private int minValue;
    private int maxValue;
    private String paramName;
    private int progressBarValue = 0;

    OnMyDialogResult mDialogResult; // the callback

    public ValueChangeDialog(Context context, RCEditText rcEditText, String paramName) {
        super(context);
        this.value = rcEditText.getText().toString();
        this.type = rcEditText.getsFormatType();
        this.minValue = rcEditText.getMinValue();
        this.maxValue = rcEditText.getMaxValue();
        this.paramName = paramName;
        this.progressBarValue = (int)(rcEditText.getRCData() * Math.pow(10, type)); // current position in %
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.valuechangedialog);
        setTitle("Value of " + paramName);
        //setTitle(paramName);

        valueBar = (SeekBar) findViewById(R.id.value_bar);
        valueText = (TextView) findViewById(R.id.red_text);

        btnOk = (Button) findViewById(R.id.ok);
        btnCancel = (Button) findViewById(R.id.cancel);
        btnDown = (Button) findViewById(R.id.btnDown);
        btnUp = (Button) findViewById(R.id.btnUp);

        valueBar.setMax(maxValue); // Set max value
        valueBar.setProgress(progressBarValue);

        valueText.setText(value);
        valueBar.setOnSeekBarChangeListener(this);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnDown.setOnClickListener(this);
        btnUp.setOnClickListener(this);

        setCancelable(false);

        if (D) {
            Log.d(TAG,
                    "New RCEditText - Type: " + type +
                    " minValue: " + minValue +
                    " maxValue: " + maxValue
                    );
        }

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (D) Log.i("Value", "New progress value: " + progress);
        Double newValue = (double) (progress);
        switch (this.type) {
            case PRECISION_NONE:
                if (D) Log.i("Value", "New value: " + newValue);
                valueText.setText(new DecimalFormat(
                        "#0",
                        new DecimalFormatSymbols(Locale.US)).format(newValue));
                break;
            case PRECISION_SINGLE:
                if (D) Log.i("Value", "New value: " + newValue / 10);
                valueText.setText(new DecimalFormat(
                        "#0.0",
                        new DecimalFormatSymbols(Locale.US)).format(newValue / 10));
                break;
            case PRECISION_DOUBLE:
                if (D) Log.i("Value", "New value: " + newValue / 100);
                valueText.setText(new DecimalFormat(
                        "#0.00",
                        new DecimalFormatSymbols(Locale.US)).format(newValue / 100));
                break;
            case PRECISION_TRIPLE:
                if (D) Log.i("Value", "New value: " + newValue / 1000);
                valueText.setText(new DecimalFormat(
                        "#0.000",
                        new DecimalFormatSymbols(Locale.US)).format(newValue / 1000));
                break;

            default:
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onClick(View v) {
        if (D) Log.i("Value", "onClick called");
        switch (v.getId()) {
            case R.id.ok:
                mDialogResult.finish(valueText.getText().toString());
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
            case R.id.btnDown:
                if (valueBar.getProgress() > minValue) {
                    valueBar.setProgress(valueBar.getProgress() - 1);
                }
                break;
            case R.id.btnUp:
                if (valueBar.getProgress() < maxValue) {
                    valueBar.setProgress(valueBar.getProgress() + 1);
                }
        }
    }

    public void setDialogResult(OnMyDialogResult onMyDialogResult) {
        mDialogResult = onMyDialogResult;
    }

    public interface OnMyDialogResult{
        void finish(String result);
     }
}
