package net.xrotor.andmultiwiiconf;

import android.util.Log;


public class MwcCommands {
	// Debugging
	private static final String TAG = "AndMultiWiiConf.MwcCommands";
	private static final boolean D = false;
	
	//TODO: Clean all this switch{} case code...
	public static byte[] calibrateACC(int mActiveMwcVersion) {
		byte[] command = null;
		switch (mActiveMwcVersion){
		case MwcConstants.MWC_VERSION_17:
			command = MwcConstants.MWCCOMMANDS_LIST_17[MwcConstants.SERIAL_COMMAND_CALIBRATE_ACC].getBytes();
			break;
		case MwcConstants.MWC_VERSION_18:
			command = MwcConstants.MWCCOMMANDS_LIST_18[MwcConstants.SERIAL_COMMAND_CALIBRATE_ACC].getBytes();
			break;
		case MwcConstants.MWC_VERSION_19:
			command = MwcConstants.MWCCOMMANDS_LIST_19[MwcConstants.SERIAL_COMMAND_CALIBRATE_ACC].getBytes();
			break;
		case MwcConstants.MWC_VERSION_20:
			command = MwcConstants.MWCCOMMANDS_LIST_20[MwcConstants.SERIAL_COMMAND_CALIBRATE_ACC].getBytes();
		}
	if (D) Log.d(TAG, "calibrateACC() command: " +new String(command));
	return command;
	}

	public static byte[] calibrateMAG(int mActiveMwcVersion) {
		byte[] command = null;
		switch (mActiveMwcVersion){
		case MwcConstants.MWC_VERSION_17:
			command = null;
			break;
		case MwcConstants.MWC_VERSION_18:
			command = MwcConstants.MWCCOMMANDS_LIST_18[MwcConstants.SERIAL_COMMAND_CALIBRATE_MAG].getBytes();
			break;
		case MwcConstants.MWC_VERSION_19:
			command = MwcConstants.MWCCOMMANDS_LIST_19[MwcConstants.SERIAL_COMMAND_CALIBRATE_MAG].getBytes();
			break;
		case MwcConstants.MWC_VERSION_20:
			command = MwcConstants.MWCCOMMANDS_LIST_20[MwcConstants.SERIAL_COMMAND_CALIBRATE_MAG].getBytes();
		}
	if (D) Log.d(TAG, "calibrateMAG() command: " + new String(command));
	return command;
	}

	public static byte[] readPARAMS(int mActiveMwcVersion) {
		byte[] command = null;
		switch (mActiveMwcVersion){
		case MwcConstants.MWC_VERSION_17:
			command = MwcConstants.MWCCOMMANDS_LIST_17[MwcConstants.SERIAL_COMMAND_READ_DATA].getBytes();
			break;
		case MwcConstants.MWC_VERSION_18:
			command = MwcConstants.MWCCOMMANDS_LIST_18[MwcConstants.SERIAL_COMMAND_READ_DATA].getBytes();
			break;
		case MwcConstants.MWC_VERSION_19:
			command = MwcConstants.MWCCOMMANDS_LIST_19[MwcConstants.SERIAL_COMMAND_READ_DATA].getBytes();
			break;
		case MwcConstants.MWC_VERSION_20:
			command = MwcConstants.MWCCOMMANDS_LIST_20[MwcConstants.SERIAL_COMMAND_READ_DATA].getBytes();
		}
	if (D) Log.d(TAG, "radPARAMSC() command: " + new String(command));
	return command;
	}
	
	/**
	 * 
	 * @param paramsData
	 * @param mActiveMwcVersion
	 * @return paramsData as byte[]
	 */
	//TODO: What exactly does this code do???
	//public static byte[] writePARAMS(byte[] paramsData, int mActiveMwcVersion) {
		/*
		byte[] command = null;
		switch (mActiveMwcVersion){
		case MwcConstants.MWC_VERSION_17:
			command = MwcConstants.MWCCOMMANDS_LIST_17[MwcConstants.SERIAL_COMMAND_WRITE_DATA].getBytes();
			break;
		case MwcConstants.MWC_VERSION_18:
			command = MwcConstants.MWCCOMMANDS_LIST_18[MwcConstants.SERIAL_COMMAND_WRITE_DATA].getBytes();
		}
		
		byte[] data = new byte[command.length + paramsData.length];

		for (int i = 0; i < data.length; ++i)
		{
		    data[i] = i < command.length ? command[i] : paramsData[i - command.length];
		}
	    if (D) Log.d(TAG, "writePARAMS() command: " + new String(command));
	        return data;
		*/
	//    return paramsData;
	//}
	
}