package net.xrotor.andmultiwiiconf;

public class Profile {
	
	private String name;
	private int version;
	
	Profile() {
		this.name = "";
		this.version = 0;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public void setVersion(int newVersion) {
		this.version = newVersion;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getVersion() {
		return this.version;
	}
	
}
