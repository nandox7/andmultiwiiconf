package net.xrotor.andmultiwiiconf;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

public class MwcProfiles {

    // Debugging
    private static final String TAG = "AndMultiWiiConf.MwcProfiles";
    private static final boolean D = false;

    // Class public vars
    public String profileName = "";
    public int profileVersion = 0;
    protected MwcParameters profileMwcParameters;

    MwcProfiles(MwcParameters mMwcParameters){
	this.profileMwcParameters = mMwcParameters;
    }

    /*
     * Save the Profile file 
     */
    public boolean loadProfile(String fileName){

	File file = new File(fileName);	
	FileInputStream myfileInputStream;

	try {

	    if (D) Log.d(TAG, "Trying to read file: " + file.getAbsolutePath());

	    myfileInputStream = new FileInputStream(file);
	    long length = file.length();

	    if (myfileInputStream != null) {

		if (length > Integer.MAX_VALUE) {
		    myfileInputStream.close();
		    throw new IOException("The file is too big");
		}

		// Create the byte array to hold the data
		byte[] readBytes = new byte[(int)length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < readBytes.length
			&& (numRead=myfileInputStream.read(readBytes, offset, readBytes.length-offset)) >= 0) {
		    offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < readBytes.length) {
	        // Close the input stream, all file contents are in the bytes variable
            myfileInputStream.close();
		    throw new IOException("The file was not completely read: " + file.getName());
		}

		// Close the input stream, all file contents are in the bytes variable
		myfileInputStream.close();

		if (D) Log.d(TAG, "profile read with success! ");

		//this.profileVersion = byteArrayToInt(readBytes[0]);
		parseProfile(readBytes);
	    }


	} catch (Exception e) {
	    Log.e(TAG, "loading profile error: " + e.toString());
	    return false;
	}

	return true;
    }

    public boolean saveProfile(String profileName, int[] dataStream, int mwcVersion){
	// Make sure the profile folder exists
	checkProfileFolder();

	FileOutputStream myfileOutputStream = null;
	String extStorageDirectory = Environment.getExternalStorageDirectory().toString();

	try {

	    myfileOutputStream = new FileOutputStream(extStorageDirectory + MwcConstants.PROFILES_PATH
		    + profileName + MwcConstants.PROFILES_EXTENTION);

	    if (myfileOutputStream != null) {

		// Write first the Mwc Version (Default: 17)
		myfileOutputStream.write(mwcVersion);

		// Write the PID values
		for(int i = 0; i < dataStream.length; i++) {
		    myfileOutputStream.write(dataStream[i]);
		    //myfileOutputStream.write(intToByteArray((byte) ((dataStream[i]) & 0xFF)));
		}
		myfileOutputStream.close();
	    }

	} catch (Exception e) {
	    Log.e(TAG, "saving profile" + "Exception: " + e.toString());
	    return false;
	}
	return true;
    }

    private boolean parseProfile(byte[] readBytes){

	// Parse the read data
	if (readBytes != null) {

	    this.profileVersion = readBytes[0]&0xff;
	    if (D) Log.d(TAG, "Version: " +this.profileVersion);

	    int[] data = new int[readBytes.length - 1];

	    for(int i = 0; i < readBytes.length - 1; i++) {
		data[i] = readBytes[i + 1]&0xff;
		//Log.e(TAG, "Field: " + data[i]);
	    }
	    // Set the received parameters values into the global MwcParameters instance
	    this.profileMwcParameters.setParamsData(data);

	} else {
	    // String is empty something is wrong...
	    return false;
	}
	return true;
    }

    private static void checkProfileFolder(){
	File dir = new File(Environment.getExternalStorageDirectory() + MwcConstants.PROFILES_PATH);
	if(!dir.exists() || !dir.isDirectory()) {
	    dir.mkdirs();
	}
    }

    public static ArrayList<Profile> findProfiles() {
        // Make sure the profile folder exists
        checkProfileFolder();
        File dir = new File(Environment.getExternalStorageDirectory() + MwcConstants.PROFILES_PATH);
        FilenameFilter filenameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith(MwcConstants.PROFILES_EXTENTION);
            }
        };

        String[] listFiles = dir.list(filenameFilter);

        if(listFiles != null) {
            if(D) Log.d(TAG, "Profile files found: " + listFiles.length);

            ArrayList<Profile> profiles = new ArrayList<Profile>();
            int i = 0;
            for(String file: listFiles){
                if(D) Log.d(TAG, "Profile number " + i + " for file: " + file);
                // Check if the profile version matches the active one if one ignore the file
                if (D) {Log.d(TAG, "File version: " + (readVersion(Environment.getExternalStorageDirectory() 
                        + MwcConstants.PROFILES_PATH + file) + " Active: " + MwcConstants.MWC_VERSIONS_LIST[AndMultiWiiConf.getActiveVersion()])); }
                        if (readVersion(Environment.getExternalStorageDirectory() 
                                + MwcConstants.PROFILES_PATH + file) == MwcConstants.MWC_VERSIONS_LIST[AndMultiWiiConf.getActiveVersion()]) {
                            profiles.add(new Profile());
                            profiles.get(i).setName(file);
                            profiles.get(i).setVersion(readVersion(Environment.getExternalStorageDirectory() 
                                    + MwcConstants.PROFILES_PATH + file));
                            i++;
                        }
            }
            return profiles;
        } else {
            return null;
        }
    }

    public static int readVersion(String file) {
	try {
	    FileInputStream myfileInputStream = new FileInputStream(file);
	    byte[] buffer = new byte[1];
	    try {
		myfileInputStream.read(buffer, 0, 1);
		myfileInputStream.close();
		return buffer[0]&0xff;
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
	return 0;
    }

    // Return the mwc version index for the given version number
    public static int getVersionIndex(int mwcVersionNumber) {
	for(int i =0; i < MwcConstants.MWC_VERSIONS_LIST.length; i++) {
	    if (MwcConstants.MWC_VERSIONS_LIST[i] == mwcVersionNumber) { 
		return i;
	    }
	}
	return 0;
    }

}
