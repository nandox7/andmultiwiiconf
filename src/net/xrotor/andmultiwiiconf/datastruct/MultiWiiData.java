package net.xrotor.andmultiwiiconf.datastruct;

import android.util.Log;

import net.xrotor.andmultiwiiconf.AndMultiWiiConf;
import net.xrotor.andmultiwiiconf.DataTools;
import net.xrotor.andmultiwiiconf.MwcConstants;

import java.nio.ByteBuffer;

/**
 * [[ MultiWii Data Structure ]]
 *
 * This structure is valid up to 1.8.
 *
 */
public class MultiWiiData {

    // Debugging
    protected static final String TAG = "AndMultiWiiConf.MultiWiiData";
    protected static final boolean D = true;

    int multiType;  // 1 for tricopter, 2 for quad+, 3 for quadX

    int version;

    float gx,gy,gz = 0;
    float ax,ay,az = 0;
    float baro = 0;
    float mag = 0;
    float angx,angy = 0;
    float r;
    int init_com = 0;
    int graph_on = 0;

    float[] motorArray = new float[8];
    float[] servoArray = new float[8];

    float rcThrottle = 0,
      rcRoll     = 0,
      rcPitch    = 0,
      rcYaw      = 0,
      rcAUX1     = 0,
      rcAUX2     = 0,
      rcAUX3     = 0,
      rcAUX4     = 0;

    int nunchukPresent,
        i2cAccPresent,
        i2cBaroPresent,
        i2cMagnetoPresent,
        evelMode;

    int byteP_ROLL,
        byteI_ROLL,
        byteD_ROLL,
        byteP_PITCH,
        byteI_PITCH,
        byteD_PITCH,
        byteP_YAW,
        byteI_YAW,
        byteD_YAW,
        byteP_LEVEL,
        byteI_LEVEL,
        byteD_LEVEL,
        byteRC_RATE,
        byteRC_EXPO,
        byteACC_STRENGTH,
        byteRollPitchRate,
        byteYawRate,
        byteDynThrPID;

    int[] activationState = new int[40];

    boolean[] sensorsPresent = new boolean[] {false, false, false, false, false};
    boolean[] sensorsState = new boolean[] {false, false, false, false, false};

    int cycleTime;

    // New vars for version 1.8
    int magx,
        magy,
        magz;

/*
    int mot6,
        mot7,
        mot8;
*/

    int byteP_ALT,
        byteI_ALT,
        byteD_ALT,
        byteP_VEL,
        byteI_VEL,
        byteD_VEL,
        byteP_MAG;

    int pMeterSum,
        intPowerTrigger,
        bytevbat;

    int debug1,
        debug2,
        debug3,
        debug4;
    // -------------------------

    // Temp Byte Data Buffer for incoming data
    ByteBuffer mInByteBuffer = ByteBuffer.allocate(180);

    // Byte array with that to process
    public byte[] dataBuffer = new byte[ MwcConstants.SERIAL_FRAME_SIZE[ AndMultiWiiConf.getActiveVersion() ] ];

    // Buffer frame pointer
    int bufPointer;

    // **
    // Buffer processing methods
    // -------------------------
    protected final int read32() {
        return (dataBuffer[bufPointer++]&0xff) + ((dataBuffer[bufPointer++]&0xff)<<8) + ((dataBuffer[bufPointer++]&0xff)<<16) + ((dataBuffer[bufPointer++]&0xff)<<24);
    }

    protected final int read16() {
        return (dataBuffer[bufPointer++]&0xff) + (dataBuffer[bufPointer++]<<8);
    }

    protected final int read8() {
        return dataBuffer[bufPointer++]&0xff;
    }
    // -------------------------


    public boolean processData() {

        switch (AndMultiWiiConf.getActiveVersion() ) {
        case MwcConstants.MWC_VERSION_17:
            if (D) Log.d(TAG, "Active version 1.7 processing data");
            if ( processData_17() ) return true;
            if (D) Log.d(TAG, "Data processing failed.");
            break;
        case MwcConstants.MWC_VERSION_18:
            if (D) Log.d(TAG, "Active version 1.8 processing data");
            if ( processData_18() ) return true;
            if (D) Log.d(TAG, "Data processing failed.");
            break;
        }
        return false;
    }

    // Old process data for ver 1.7
    public boolean processData_17() {

    int present = 0;
    int mode = 0;

    if (D) {
        Log.d(TAG, "Processing data for v1.7" );
        Log.d(TAG, "Data String size:" + dataBuffer.length );
        Log.d(TAG, "Last byte: " + ((char)(dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_17] - 1]&0xff)));
        Log.d(TAG, DataTools.getHex(dataBuffer) );
    }

    if ( ((char) (dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_17] - 1]&0xff)) == 'A' ) {

        if (D) Log.d(TAG, "Processing data for v1.7..");

        //bufPointer = 0; // only when using the fake MWC service
        bufPointer = 1; //starting with 1 to ingnore pos=0 that is the A

        ax = read16();
        ay = read16();
        az = read16();

        gx = read16();
        gy = read16();
        gz = read16();

        baro = read16();
        mag = read16();

        int lenght = servoArray.length;
        for (int i = 0; i < lenght - 4; i++) {
            servoArray[i] = read16();
        }

        lenght = motorArray.length;
        for (int i = 0; i < lenght - 2; i++) {
            motorArray[i] = read16();
        }

        rcRoll = read16();
        rcPitch = read16();
        rcYaw = read16();
        rcThrottle = read16();

        rcAUX1 = read16();
        rcAUX2 = read16();
        rcAUX3 = read16();
        rcAUX4 = read16(); //52

        present = read8();
        mode = read8();
        cycleTime = read16();

        angx = read16();
        angy = read16();

        multiType = read8(); //61

        byteP_ROLL = read8();
        byteI_ROLL = read8();
        byteD_ROLL = read8();

        byteP_PITCH = read8();
        byteI_PITCH = read8();
        byteD_PITCH = read8();

        byteP_YAW = read8();
        byteI_YAW = read8();
        byteD_YAW = read8(); // 70


        byteP_LEVEL = read8();
        byteI_LEVEL = read8();

        byteRC_RATE = read8();
        byteRC_EXPO = read8();

        byteRollPitchRate = read8();
        byteYawRate = read8();
        byteDynThrPID = read8();

        activationState[0] = read8(); // Level
        activationState[1] = read8(); // Baro
        activationState[2] = read8(); // Mag
        activationState[3] = read8(); // CamStab
        activationState[4] = read8(); // CamTrig

        // Processing present - Sensors Presence
        if ((present&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // NK
        if ((present&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // ACC
        if ((present&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // BARO
        if ((present&8) >0) sensorsState[3] = true; else sensorsState[3] = false ; // MAG

        // Processing Mode - Sensors State
        if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
        if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
        if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG

        return true;
    }

    return false;
    }

    // New process data for ver 1.8
    public boolean processData_18() {

    int present = 0;
    int mode = 0;

    if (D) {
        Log.d(TAG, "Processing data for v1.8" );
        Log.d(TAG, "Data String size:" + dataBuffer.length );
        Log.d(TAG, "Last byte: " + ((char)(dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_18] - 1]&0xff)));
        Log.d(TAG, DataTools.getHex(dataBuffer) );
    }

    if ( ((char) (dataBuffer[ MwcConstants.SERIAL_FRAME_SIZE[MwcConstants.MWC_VERSION_18] - 1]&0xff)) == 'M' ) {

        if (D) Log.d(TAG, "Processing data for v1.8...");

        //bufPointer = 0; // only when using the fake MWC service
        bufPointer = 1; //starting with 1 to ignore pos=0 that is the M

        version = read8();

        ax = read16();
        ay = read16();
        az = read16();

        gx = read16();
        gy = read16();
        gz = read16();

        magx = read16();
        magy = read16();
        magz = read16(); //19

        baro = read16();
        mag = read16();

        for (int i = 0; i < servoArray.length - 4; i++) {
            servoArray[i] = read16();
        }

        for (int i = 0; i < motorArray.length; i++) {
            motorArray[i] = read16();
        }

        rcRoll = read16();
        rcPitch = read16();
        rcYaw = read16();
        rcThrottle = read16();

        rcAUX1 = read16();
        rcAUX2 = read16();
        rcAUX3 = read16();
        rcAUX4 = read16(); //63

        present = read8();
        mode = read8();
        cycleTime = read16();

        angx = read16();
        angy = read16();

        multiType = read8(); //72

        byteP_ROLL = read8();
        byteI_ROLL = read8();
        byteD_ROLL = read8();

        byteP_PITCH = read8();
        byteI_PITCH = read8();
        byteD_PITCH = read8();

        byteP_YAW = read8();
        byteI_YAW = read8();
        byteD_YAW = read8(); // 70

        byteP_ALT = read8();
        byteI_ALT = read8();
        byteD_ALT = read8();

        byteP_VEL = read8();
        byteI_VEL = read8();
        byteD_VEL = read8(); // 70

        byteP_LEVEL = read8();
        byteI_LEVEL = read8(); //89
        byteP_MAG = read8();

        byteRC_RATE = read8();
        byteRC_EXPO = read8();

        byteRollPitchRate = read8();
        byteYawRate = read8();
        byteDynThrPID = read8(); //95

        activationState[0] = read8(); // Level
        activationState[1] = read8(); // Baro
        activationState[2] = read8(); // Mag
        activationState[3] = read8(); // CamStab
        activationState[4] = read8(); // CamTrig
        activationState[5] = read8(); // Arm

        pMeterSum = read8();
        intPowerTrigger = read16();
        bytevbat = read8();

        debug1 = read16();
        debug2 = read16();
        debug3 = read16();
        debug4 = read16();

        // Process multi-mode bytes
        if ((present&1) >0) nunchukPresent = 1; else  nunchukPresent = 0;
        if ((present&2) >0) i2cAccPresent = 1; else  i2cAccPresent = 0;
        if ((present&4) >0) i2cBaroPresent = 1; else  i2cBaroPresent = 0;
        if ((present&8) >0) i2cMagnetoPresent = 1; else  i2cMagnetoPresent = 0;

        // Processing Mode - Sensors State
        if ((mode&1) >0) sensorsState[0] = true; else sensorsState[0] = false ; // ACC
        if ((mode&2) >0) sensorsState[1] = true; else sensorsState[1] = false ; // BARO
        if ((mode&4) >0) sensorsState[2] = true; else sensorsState[2] = false ; // MAG

        return true;
    }

    return false;
    }



    public int[] getParameters(int mwcVersion){
        int[] paramsString = null;
        int iterA = 0;

        switch(mwcVersion){
        case MwcConstants.MWC_VERSION_17:
            paramsString = new int[32];

            paramsString[iterA++] = 'C'; // control char
            paramsString[iterA++] = byteP_ROLL;
            paramsString[iterA++] = byteI_ROLL;
            paramsString[iterA++] = byteD_ROLL;
            paramsString[iterA++] = byteP_PITCH;
            paramsString[iterA++] = byteI_PITCH;
            paramsString[iterA++] = byteD_PITCH;
            paramsString[iterA++] = byteP_YAW;
            paramsString[iterA++] = byteI_YAW;
            paramsString[iterA++] = byteD_YAW;
            paramsString[iterA++] = byteP_LEVEL;
            paramsString[iterA++] = byteI_LEVEL;
            paramsString[iterA++] = byteRC_RATE;
            paramsString[iterA++] = byteRC_EXPO;
            paramsString[iterA++] = byteRollPitchRate;
            paramsString[iterA++] = byteYawRate;
            paramsString[iterA++] = byteDynThrPID;

            for(int iterB = 0; iterB <= 5; iterB++) {
                paramsString[iterA++] = activationState[iterB];
            }
            break;

        case MwcConstants.MWC_VERSION_18:
            paramsString = new int[32];

            paramsString[iterA++] = 'W'; // control char
            paramsString[iterA++] = byteP_ROLL;
            paramsString[iterA++] = byteI_ROLL;
            paramsString[iterA++] = byteD_ROLL;
            paramsString[iterA++] = byteP_PITCH;
            paramsString[iterA++] = byteI_PITCH;
            paramsString[iterA++] = byteD_PITCH;
            paramsString[iterA++] = byteP_YAW;
            paramsString[iterA++] = byteI_YAW;
            paramsString[iterA++] = byteD_YAW;
            paramsString[iterA++] = byteP_ALT;
            paramsString[iterA++] = byteI_ALT;
            paramsString[iterA++] = byteD_ALT;
            paramsString[iterA++] = byteP_VEL;
            paramsString[iterA++] = byteI_VEL;
            paramsString[iterA++] = byteD_VEL;
            paramsString[iterA++] = byteP_LEVEL;
            paramsString[iterA++] = byteI_LEVEL;
            paramsString[iterA++] = byteP_MAG;
            paramsString[iterA++] = byteRC_RATE;
            paramsString[iterA++] = byteRC_EXPO;
            paramsString[iterA++] = byteRollPitchRate;
            paramsString[iterA++] = byteYawRate;
            paramsString[iterA++] = byteDynThrPID;

            for(int iterB = 0; iterB < 6; iterB++) {
                paramsString[iterA++] = activationState[iterB];
            }

            paramsString[30] = intPowerTrigger;
            break;
        }
        return paramsString;
    }

    /**
     * Return the values readings for the sensors
     * @param mwcVersion int representing the Mwc current version
     * @return float[] containing the sensor readings
     */
    public float[] getSensors(int mwcVersion) {
        float[] sensorStream = new float[6];

        // Gyro readings
        sensorStream[0] = gx;
        sensorStream[1] = gy;
        sensorStream[2] = gz;

        // Acc readings
        sensorStream[3] = ax;
        sensorStream[4] = ay;
        sensorStream[5] = az;

        if (D) {
            for (int i = 0; i < sensorStream.length; i++) {
            Log.d(TAG, "getSensors() : Pos[" + i + "] -> "
                + sensorStream[i]);
            }
        }
        return sensorStream;
    }

    /**
     * Return the values readings for the sensors
     * @param mwcVersion int representing the Mwc current version
     * @return float[] containing the sensor readings
     */
    public boolean[] getSensorState(int mwcVersion) {
        return sensorsState;
    }

    /*public boolean setData(byte[] buffer) {
        try {
            if (D) Log.i(TAG, "Received buffer: " + DataTools.getHex(buffer));

            this.dataBuffer = buffer;
            if (processData()) {
                return true;
            }
        } catch (Exception e) {
            if (D) { Log.e(TAG, "Exception eccurred: " + e.toString());}
        }
        return false;
    }*/

    /**
     * 
     * @return dataBuffer
     */
    public byte[] getData() {
        return dataBuffer;
    }

    public int getLenght() {
        return dataBuffer.length;
    }

    @Override
    public void finalize() {
      if (D) Log.d(TAG, "Destroying class MultiWiiData");
    }

}